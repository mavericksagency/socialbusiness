<?php get_header(); ?>
<main class="main">
	<div class="main-grid">
		<div class="left-column">
            <?php get_sidebar(); ?>
        </div>
		<div class="double-column">
			<div class="clear-block">
				<img class="clear-image" src="<?php bloginfo('template_directory'); ?>/img/404.svg">
				<h2 class="clear-title">Помилка 404</h2>
				<span class="clear-subtitle">сторінку не знайдено</span>
				<span class="clear-text">Не хвилюйтеся. Поверніться на попередню сторінку...</span>
			</div>
		</div>
	</div>
</main>
<?php get_footer(); ?>