<?php 
get_header(); ?>
	<main class="main post-page">
		<div class="main-grid">
			<div class="left-column">
				<?php get_sidebar(); ?>
			</div>
			<div class="double-column">
				<?php get_template_part('partials/post/post_modal'); ?>
			</div>
		</div>
	</main>
<?php get_footer();