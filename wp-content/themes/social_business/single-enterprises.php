<?php 
    global $bookmarks;
    get_header(); 
    the_post();
    $main               =   get_field('main');
    $contacts           =   get_field('contacts');
    $workers            =   get_field('workers');
    $add_info           =   get_field('add_info');
    $geo_position       =   get_field('geo_position');
    $sbInUAPost         =   get_field('sb_in_ua', $post->ID);
?>
<main class="main">
    <div class="main-grid">
    <div class="left-column">
        <?php get_sidebar(); ?>
    </div>
    <div class="double-column">
        <div class="panel-main-lg page-main-panel <?= 'class-'.$post->post_type; ?>">
            <div class="item-main">
                <?php 
                    $logo   = get_the_post_thumbnail_url($post->ID, 'full');
                ?>
                <div class="item-image <?php if(!$logo){echo 'inner-svg';}?> " <?php if($logo){ echo 'style="background-image: url('.$logo.')";'; } ?>>
                    <?php if(!$logo){ ?>
                    <svg class="icon svg-fill">
                        <use xlink:href="#buildings">
                    </svg>
                    <?php } ?>
                </div>
                <div class="item-info">
                    <h1 class="item-title">
                        <?php the_title(); ?>
                        <?php 
                            if($sbInUAPost){
                                echo '<span class="sb-mark">SBinUA</span>';
                            }
                        ?>
                    </h1>
                    <span class="item-desc"><?= get_type_resourse($post); ?></span>
                </div>
            </div>
            <div class="panel-main-buttons">
                <a href="#" class="btn-main-panel btn btn-share share-button" data-url="<?php the_permalink(); ?>">
                    <svg class="icon">
                        <use xlink:href="#share">
                    </svg>
                </a>
                <button class="btn-main-panel btn btn-mark add_bookmark <?php if(in_array($post->ID, $bookmarks, FALSE)){ echo 'added'; } ?>" data-id="<?= $post->ID; ?>">
                    <svg class="icon">
                        <use xlink:href="#star">
                    </svg>
                    <span class="text">До закладок</span>
                </button>
            </div>
        </div>
        <div class="main-grid user-grid">
            <div class="center-column">
                <div class="info-source shadow-block">
                    <h3 class="info-title">Контакти</h3>
                    <ul class="info-list">
                        <?php 
                        if($contacts['address']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#map-mark-stroke">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Адреса</span>
                                <?php 
                                    $geo_pos = get_field('geo_position');
                                    if($geo_pos){
                                        echo '<a target="_blank" href="http://maps.google.com/?q='.$geo_pos.'" class="info-link">'.$contacts['address'].'</a>';
                                    }else{
                                        echo '<span class="text">'.$contacts['address'].'</span>';
                                    }
                                ?>
                                
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($contacts['email'] && !$contacts['hide_email']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#mail">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">E-mail:</span>
                                <a class="info-link" href="mailto:<?= $contacts['email']; ?>"><?= $contacts['email']; ?></a>
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($contacts['phone'] && !$contacts['hide_phone']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#telephone">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Телефон:</span>
                                <a class="info-link" href="tel:<?= $contacts['phone']; ?>"><?= $contacts['phone']; ?></a>
                            </div>
                        </li>
                        <?php } ?>
                        <?php 
                        //website
                        if($contacts['website'] && !$contacts['hide_website']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#www">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Веб-сайт:</span>
                                <a class="info-link"  href="<?= $contacts['website']; ?>" target="_blank"><?= $contacts['website']; ?></a>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                    <div class="more">
                        <button class="more-text btn-transparent">
                            <span class="text active">Детальна інформація</span>
                            <span class="text">Приховати детальну інформацію</span>
                        </button>
                    </div>
                    <div class="more-panel">
                        <h3 class="info-title">Детальна інформація</h3>
                        <ul class="info-list">
                            <?php 
                            //service
                            $serviceTerms   = get_the_terms($post->ID, 'service');
                            if($serviceTerms){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#business-sphere">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Сфера бізнесу:</span>
                                    <span class="text"><?= getTermsName($serviceTerms); ?></span>
                                </div>
                            </li>
                            <?php } ?>
                            <?php 
                            //type
                            $getTerms   = get_the_terms($post->ID, 'type_enterprises'); 
                            if($getTerms){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#book">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Тип соціального підприємства:</span>
                                    <span class="text"><?= getTermsName($getTerms); ?></span>
                                </div>
                            </li>
                            <?php } ?>
                            <?php 
                            //beneficiaries
                            $getTerms   = get_the_terms($post->ID, 'beneficiaries');
                            if($getTerms){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#aim-work">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Заради кого працюєте:</span>
                                    <span class="text"><?= getTermsName($getTerms); ?></span>
                                </div>
                            </li>
                            <?php } ?>
                            </ul>
                            <ul  class="info-list">
                            <?php 
                            //date_of_foundation
                            if($main['date_of_foundation']){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#start-date">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Дата заснування:</span>
                                    <span class="text"><?= $main['date_of_foundation']; ?></span>
                                </div>
                            </li>
                            <?php } ?>
                            <?php 
                                //service
                                if($main['form_of_activity']){                    
                                    $values     = get_field_object('field_5bbb54649e6ce');
                            ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#organization-form">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Організаційна форма:</span>
                                    <span class="text"><?= $values['choices'][$main['form_of_activity']]; ?></span>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <div class="info-article">
                            <?php 
                                $info   = get_field('add_info', $post->ID);
                                if($info){
                                    if($info['about_enterprise']){
                                        ?>
                                        <span class="info-title">
                                            Про підприємство
                                        </span>
                                        <div class="info-article-text">
                                            <p>
                                                <?= $info['about_enterprise']; ?>
                                            </p>
                                        </div>
                                        <?php
                                    }
                                    if($info['services']){
                                    ?>
                                    <span class="info-title">
                                        Послуги
                                    </span>
                                    <div class="info-article-text">
                                        <p>
                                            <?= $info['services']; ?>
                                        </p>
                                    </div>
                                    <?php
                                    }
                                    if($info['mission_enterprise']){
                                        ?>
                                        <span class="info-title">
                                        Місія підприємства
                                        </span>
                                        <div class="info-article-text">
                                            <p>
                                                <?= $info['mission_enterprise']; ?>
                                            </p>
                                        </div>
                                        <?php
                                    }
                                    if($info['story_creation']){
                                        ?>
                                        <span class="info-title">
                                            Історія створення
                                        </span>
                                        <div class="info-article-text">
                                            <p>
                                                <?= $info['story_creation']; ?>
                                            </p>
                                        </div>
                                        <?php
                                    }
                                    if($info['history_of_success']){
                                        ?>
                                        <span class="info-title">
                                            Історія успіху
                                        </span>
                                        <div class="info-article-text">
                                            <p>
                                                <?= $info['history_of_success']; ?>
                                            </p>
                                        </div>
                                        <?php
                                    }
                                    if($info['real_companys_bus']){
                                        ?>
                                        <span class="info-title">
                                            Реалізація місії підприємства
                                        </span>
                                        <div class="info-article-text">
                                            <p>
                                                <?= $info['real_companys_bus']; ?>
                                            </p>
                                        </div>
                                        <?php
                                    }
                                    if($info['profit_fin_ind']){
                                        ?>
                                        <span class="info-title">
                                            Прибуток та фінансові показники
                                        </span>
                                        <div class="info-article-text">
                                            <p>
                                                <?= $info['profit_fin_ind']; ?>
                                            </p>
                                        </div>
                                        <?php
                                    }
                                    if($info['impact_activity']){
                                        ?>
                                        <span class="info-title">
                                            Вплив діяльності
                                        </span>
                                        <div class="info-article-text">
                                            <p>
                                                <?= $info['impact_activity']; ?>
                                            </p>
                                        </div>
                                        <?php
                                    }
                                }
                            ?>
                            
                        </div>
                    </div>
                </div>
                <div class="wall shadow-block">
                    <div class="wall-header">
                        <?php 
                            $args = array(
                                'post_type'         => array('news', 'event', 'opportunities', 'knowledge_base'),
                                'posts_per_page'    => 10,
                                'order'             => 'DESC',
                                'meta_query'        => array(
                                    'public'        => array(
                                        'key'       => 'public',
                                        'value'     => $post->post_type.'_'.$post->ID,
                                        'compare'   => 'LIKE',
                                    )
                                )
                            );
                            $query_all              = new WP_Query($args);
                            get_template_part('partials/wall-result');
                        ?>
                </div>

            </div>

            <div class="right-column">
                <div class="block siblings-block">
                    <h2 class="block-title">
                        Редактори
                    </h2>
                    <?php 
                        $admins     = get_field('page_administrators');
                        $admins     = explode(',', $admins);
                        foreach($admins as $user_admin){
                            $user_admin_id  = explode('_', $user_admin)[1];
                            if($user_admin_id){
                                get_template_part('partials/author/in-post-short');
                            }
                        }
                    ?>
                </div>
                <button class="btn-block share-button" data-url="<?php the_permalink(); ?>">
                    <svg class="icon">
                        <use xlink:href="#share">
                    </svg>
                    <span class="text">
                        Поширити сторінку
                    </span>
                </button>
                <?php if(in_array('user_'.$user_id, $admins)){ ?>
                <a href="<?= get_the_permalink(53) ?>?enterprises&post_id=<?= $post->ID; ?>" class="btn-block edit-pen">
                    <svg class="icon">
                        <use xlink:href="#pen">
                    </svg>
                    <span class="text">
                        Редагувати
                    </span>
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>