<?php 
	global $user_id, $title_page, $post;
?>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta http-equiv="X-UA-Compatible" content="IE=edge">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=no">
	<link rel="apple-touch-icon" sizes="57x57" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-57x57.png">
	<link rel="apple-touch-icon" sizes="60x60" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-60x60.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="76x76" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-76x76.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-114x114.png">
	<link rel="apple-touch-icon" sizes="120x120" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-120x120.png">
	<link rel="apple-touch-icon" sizes="144x144" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-144x144.png">
	<link rel="apple-touch-icon" sizes="152x152" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-152x152.png">
	<link rel="apple-touch-icon" sizes="180x180" href="<?= get_template_directory_uri() ?>/img/favicon/apple-icon-180x180.png">
	<link rel="icon" type="image/png" sizes="192x192" href="<?= get_template_directory_uri() ?>/img/favicon/android-icon-192x192.png">
	<link rel="icon" type="image/png" sizes="32x32" href="<?= get_template_directory_uri() ?>/img/favicon/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="96x96" href="<?= get_template_directory_uri() ?>/img/favicon/favicon-96x96.png">
	<link rel="icon" type="image/png" sizes="16x16" href="<?= get_template_directory_uri() ?>/img/favicon/favicon-16x16.png">
	<link rel="manifest" href="<?= get_template_directory_uri() ?>/img/favicon/manifest.json">
	<meta name="msapplication-TileColor" content="#ffffff">
	<meta name="msapplication-TileImage" content="<?= get_template_directory_uri() ?>/img/favicon/ms-icon-144x144.png">
	<meta name="theme-color" content="#ffffff">

	<meta name="twitter:title" content="<?php the_title(); ?>" />
    <meta name="twitter:description" content="<?php the_excerpt(); ?>" />
    <meta name="twitter:image:src" content="<?= get_the_post_thumbnail_url($post->ID) ?>" />
    <meta property="og:title" content="<?php the_title(); ?>" />
    <meta property="og:image" content="<?= get_the_post_thumbnail_url(); ?>" />
    <meta property="og:description" content="<?php the_excerpt(); ?>" />

	<?php wp_head(); ?>
	<!-- Google Tag Manager -->
	<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
	new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
	j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
	'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
	})(window,document,'script','dataLayer','GTM-M2L5LKS');</script>
	<!-- End Google Tag Manager -->
	<!-- Google Tag Manager (noscript) -->
	<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-M2L5LKS"
	height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
	<!-- End Google Tag Manager (noscript) -->
</head>
<body>
<?php //include('inc/dev-menu.php');?>
<div class="svgmap display-none">
	<?php include('inc/svgmap.php'); ?>
</div>
<div class="shadow"></div>
<header class="header">
	<button class="menu-icon btn-transparent">
		<svg class="icon">
			<use xlink:href="#menu">
		</svg>
	</button>
	<a href="<?= get_home_url(); ?>" class="logo">
		<svg class="logo-icon">
			<use xlink:href="#logo">
		</svg>
	</a>
	<nav class="top-menu">
		<button class="mob-page-title">
			<span class="text">
				<?php 
					if($title_page){
						echo $title_page;
					}else{
						the_title();
					}
				?>
			</span>
			<svg class="icon">
				<use xlink:href="#down-arrow">
			</svg>
		</button>
		<?php
			wp_nav_menu(array(
				'theme_location'  	=> 'primary',
				'container'			=> false,
				'fallback_cb'		=> false,
				'items_wrap'      	=> '<ul class="top-menu-list">%3$s</ul>',
				'depth'				=> 2,
				'walker' 			=> new submenuWrap
			));
		?>
	</nav>
	<a href="#" class="top-search">	
		<svg class="icon">
			<use xlink:href="#search">
		</svg>
	</a>
	<?php if(!is_user_logged_in()){ ?>
	<div class="buttons-enter">
		<a href="#" class="btn-raised btn-enter">
			Вхід
		</a>
		<a href="#" class="btn btn-register">
			Реєстрація
		</a>
	</div>
	<?php }else{ 
		//get avatar author
		$avatar = get_field('avatar', 'user_'.$user_id);
		?>
		<a class="top-notifications" href="<?= get_the_permalink(486); ?>">
			<div class="top-notification-icon">
				<svg class="icon">
					<use xlink:href="#star">
				</svg>
				<!-- <span class="notification-number">
					7+
				</span> -->
			</div>
		</a>
	<div class="top-user">
		<div class="user-short-info">
			<div class="user-image blue bg-color" <?php if($avatar){ echo 'style="background-image: url('.$avatar['url'].')"'; } ?>>
				<?php 
					if(!$avatar){
				?>
				<svg class="icon svg-fill">
					<use xlink:href="#person">
				</svg>
				<?php 
					}
				?>
			</div>
			<div class="user-title">
				<span class="name"><?= get_the_author_meta('first_name', $user_id).' '.get_the_author_meta('last_name', $user_id) ?></span>
				<span class="company"><?= get_field('career', 'user_'.$user_id)['work_place']; ?></span>
			</div>
			<span class="user-control">
				<svg class="icon">
					<use xlink:href="#down-arrow">
				</svg>
			</span>
		</div>
		<nav class="user-menu">
			<div class="user-menu-inner">
				<div class="user-menu-inner-settings">
					<ul class="user-main-settings user-menu-section">
						<li>
							<a href="<?= get_author_posts_url($user_id); ?>">
								<svg class="icon">
									<use xlink:href="#person-circle">
								</svg>
								<span class="text">
									Профіль
								</span>
							</a>
						</li>
						<li>
							<a href="<?= get_the_permalink(486); ?>">
								<svg class="icon">
									<use xlink:href="#settings">
								</svg>
								<span class="text">
									Мої закладки
								</span>
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(658); ?>">
								<svg class="icon">
									<use xlink:href="#settings">
								</svg>
								<span class="text">
									Змінити пароль
								</span>
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(34); ?>">
								<svg class="icon">
									<use xlink:href="#settings">
								</svg>
								<span class="text">
									Редагувати профіль
								</span>
							</a>	
						</li>
					</ul>
					<ul class="user-menu-list user-menu-section">				
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=enterprises">
								Мої підприємства
							</a>	
						<li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=event">
								Мої події
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=knowledge_base">
								Моя база знань
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=resources">
								Мої ресурси
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=opportunities">
								Мої вакансії
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=news">
								Мої новини
							</a>	
						</li>
						<li>
							<a href="/online_cons/?my_question">
								Мої питання
							</a>	
						</li>
					</ul>
					<a href="#" id="logout" class="user-exit user-menu-section">
						<svg class="icon">
							<use xlink:href="#exit">
						</svg>
						<span class="text">
							Вихід з профілю
						</span>
					</a>
				</div>
			</div>
		</nav>
	</div>
	<?php } ?>
	<div class="mob-menu-create">
		<div class="menu-create">
			<button class="btn-close">
				<svg class="icon">
					<use xlink:href="#close"> 
				</svg>
			</button>
			<button class="btn-create__menu">
				<svg class="icon">
					<use xlink:href="#plus">
				</svg>
				<span class="create-text">Створити</span>
			</button>
			<div class="menu-create-inner">	
				<ul class="menu-create-list">
					<?php $createPage = get_the_permalink(53); ?>
					<li><a href="<?= $createPage; ?>?enterprises">Підприємство</a></li>
					<li><a href="<?= $createPage; ?>?event">Подію</a></li>
					<li><a href="<?= $createPage; ?>?vacancy">Вакансію</a></li>
					<li><a href="<?= $createPage; ?>?news">Новину</a></li>
					<li><a href="<?= get_the_permalink(632); ?>">Ресурс</a></li>			
					<li><a href="<?= $createPage; ?>?knowledge_base">Базу знань</a></li>
				</ul>
			</div>
		</div>
	</div>
</header>
<div class="mob-sidemenu">
	<div class="mob-sidemenu-header">
		<a href="#" class="logo">
			<svg class="logo-icon">
				<use xlink:href="#logo">
			</svg>
		</a>
		<button class="btn-close">
			<svg class="icon">
				<use xlink:href="#close">
			</svg>
		</button>
	</div>
	<div class="mob-sidemenu-inner">
		<?php if(is_user_logged_in()){ ?>
		<div class="top-user">
			<div class="user-short-info">
				<div class="user-image blue bg-color">
					<svg class="icon svg-fill">
						<use xlink:href="#person">
					</svg>
				</div>
				<div class="user-title">
					<span class="name"><?= get_the_author_meta('first_name', $user_id).' '.get_the_author_meta('last_name', $user_id) ?></span>
					<span class="company"><?= get_field('career', 'user_'.$user_id)['work_place']; ?></span>
				</div>
				<span class="user-control">
					<svg class="icon">
						<use xlink:href="#down-arrow">
					</svg>
				</span>
			</div>
			<nav class="user-menu">
				<div class="user-menu-inner">
				<div class="user-menu-inner-settings">
					<ul class="user-main-settings user-menu-section">
						<li>
							<a href="<?= get_author_posts_url($user_id); ?>">
								<svg class="icon">
									<use xlink:href="#person-circle">
								</svg>
								<span class="text">
									Профіль
								</span>
							</a>
						</li>
						<li>
							<a href="<?= get_the_permalink(486); ?>">
								<svg class="icon">
									<use xlink:href="#settings">
								</svg>
								<span class="text">
									Мої закладки
								</span>
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(658); ?>">
								<svg class="icon">
									<use xlink:href="#settings">
								</svg>
								<span class="text">
									Змінити пароль
								</span>
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(34); ?>">
								<svg class="icon">
									<use xlink:href="#settings">
								</svg>
								<span class="text">
									Редагувати профіль
								</span>
							</a>	
						</li>
					</ul>
					<ul class="user-menu-list user-menu-section">				
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=enterprises">
								Мої підприємства
							</a>	
						<li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=event">
								Мої події
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=knowledge_base">
								Моя база знань
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=resources">
								Мої ресурси
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=opportunities">
								Мої вакансії
							</a>	
						</li>
						<li>
							<a href="<?= get_the_permalink(546); ?>?post=news">
								Мої новини
							</a>	
						</li>
						<li>
							<a href="/online_cons/?my_question">
								Мої питання
							</a>	
						</li>
					</ul>
					<a href="#" id="logout" class="user-exit user-menu-section">
						<svg class="icon">
							<use xlink:href="#exit">
						</svg>
						<span class="text">
							Вихід з профілю
						</span>
					</a>
				</div>
				</div>
			</nav>
		</div>
		<?php }else{ ?>
		<div class="buttons-enter">
			<a href="#" class="btn-raised btn-enter">
				Вхід
			</a>
			<a href="#" class="btn btn-register">
				Реєстрація
			</a>
		</div>
		<?php } ?>
		<?php include('inc/sidemenu.php'); ?>
	</div>
</div>