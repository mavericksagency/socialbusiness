//
$('.url_register').on('click', function(e){
    e.preventDefault();
    openModal($('#sign-up'));
});

$('.login_url').on('click', function(e){
    e.preventDefault();
    openModal($('#sign-in'));
});

//modal
$('.openLogin').on('click', function(e){
    e.preventDefault();

    closeModal($('#sign-up'));

    setTimeout(function(){
        openModal($('#sign-in'));
    }, 200);
    
});

//modal
$('.openRegister').on('click', function(e){
    e.preventDefault();

    closeModal($('#sign-in'));
    setTimeout(function(){
        openModal($('#sign-up'));
    }, 200);
});

//modal
$('.pass-forgot').on('click', function(e){
    e.preventDefault();

    closeModal($('#sign-in'));
    setTimeout(function(){
        openModal($('#reset-pass-modal'));
    }, 200);
});

$('body').on('click', '.share-button',function(){       
    const el = document.createElement('textarea');
    el.value = $(this).attr('data-url');
    el.setAttribute('readonly', '');
    el.style.position = 'absolute';
    el.style.left = '-9999px';
    document.body.appendChild(el);
    el.select();
    document.execCommand('copy');
    document.body.removeChild(el);

    notification('success', 'Успішно', 'Посилання скопійовано.');
  });

/**
 * custom surialize
 */
function formatFormData($form, data) {
    var formValues = $form.serializeArray();
    for (var f = 0; f < formValues.length; f++) {
        //   if(formValues[f].value === "") {
        // 	  return false;
        //   }
        data[formValues[f].name] = formValues[f].value;
    }
    return data;
}

function formatFormDataArr($form, data){
    var arr = $form.serializeArray();

    arr.forEach(function (el) {
        if (!data[el.name]) {
            data[el.name] = [];
        }
        data[el.name].push(+el.value);
    });

    return data;
}

/*
 *	change status of message and save settings in cabinet
 */
function loadDoneButton(form) {
    setTimeout(function () {
        $(form).find('.submit').addClass('done');
    }, 2000);
    setTimeout(function () {
        $(form).find('.submit').removeClass('load');
        $(form).find('.submit').removeClass('done');
    }, 4000);
}
/**
 * animate move to
 */
$(".navscroll").click(function (e) {
    e.preventDefault();
    var elementClick = $(this).attr("data-move-to");
    var destination = $(elementClick).offset().top;
    jQuery("html:not(:animated),body:not(:animated)").animate({
        scrollTop: destination
    }, 900);
});
$(document).ready(function () {

    //datapicker
    $(".datepicker").datepicker({
        'dateFormat' : 'dd.mm.yy',
        yearRange: "-50:+0",
        changeMonth: true,
        changeYear: true
    });

    //active sidebar
    $('.side-menu__main li a').each(function(){
        var url = $(this).attr('href');
        if(url == location.pathname){
            $(this).addClass('active');
        }
    })

    /**
     * remove user from admin/main field
     */

    jQuery('.people-block').on('click', '.btn-delete', function(){
        var that        = jQuery(this).parent(); 
        var id          = jQuery(this).attr('data-remove');
        var input       = that.parent().attr('data-input');
        var input_load  = that.parent().attr('data-input-load');

        that.slideUp();
        setTimeout(function(){
            that.remove();
        }, 1000);

        var value       = jQuery(input).val();
        var arr         = value.split(',');
        console.log(arr);
        var newArr      = arr.filter(function(el){
            return el != id;
        });
        arr             = newArr.join(',');
        console.log(arr);
        jQuery(input).val(arr);

        var countPeople = jQuery(input_load).attr('data-corrent');
        jQuery(input_load).attr('data-corrent', countPeople - 1).prop('disabled', false);
    });

    /**
     * load user input search user
     */
    jQuery('.load_persone').on('keydown', function(e){
        var keyCode = e.keyCode || e.which;
        if (keyCode == 13){ 
            console.log('stop submit!');
            return false;
        }
    })

    $('.search-user').on('click', function(){
        var input       = jQuery(this).attr('data-input');
        var that        = jQuery(input);
        var val         = that.val();
        var block       = that.attr('data-block');
        var $input      = jQuery(that.attr('data-input'));
        var currentVal  = +that.attr('data-corrent');  
        var maxValue    = +that.attr('data-max');

        var data = {
            'action': 'load_user',
            'email' : val
        };

        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                if(data['error']){
                    notification('error', 'Помилка', 'Не вдалося знайти користувача з даною email адресою.');
                }
                jQuery(block).append(data['data']);
                var val = $input.val();
                if(val){
                    var arr = val.split(',');
                }else{
                    var arr = [];
                }
                
                arr.push('user_'+data['id_user']);
                val = arr.join(',');

                $input.val(val);

                that.val('');
                currentVal++;
                that.attr('data-corrent', currentVal); 
                if(currentVal == maxValue){
                    that.prop('disabled', true);
                }

                console.log(data);
            },
            error: function (err) {
                console.log(err);
            }
        });
    })

    /**
     * load image ajax
     */
    jQuery('#photo_load').on('change', function () {
        var file = $(this)[0].files[0];
        if (file.size > 500000) {
            notification('error', 'Помилка завантаження', 'Вказаний вами файл перевищує розмір в 500кб, оберіть файл відповідного розміру.');
            return false;
        }
        $('.field-upload-photo').addClass('loading');
        var fd = new FormData();
        fd.append("action", 'load_image');
        fd.append("image", file);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: fd,
            type: 'POST',
            processData: false,
            contentType: false,
            success: function (data) {
                //console.log(data);
                if (data) {
                    $('#photo .photo-icon .icon').css('opacity', '0');
                    $('#photo .photo-icon').css({
                        'background-image': 'url(' + data.url + ')'
                    });
                    $('#id_photo').val(data.id);
                    $('.field-upload-photo').removeClass('loading');
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * remove load attached ajax
     */
    $('#files-list').on('click', '.btn-delete', function(){
        $(this).parent().slideUp();
        setTimeout(function(){
            $('#files-list').html('');
        });
        $('#file_attached').val('');
    });

    /**
     * load file ajax
     */
    jQuery('#load_file').on('change', function () {
        var file = $(this)[0].files[0];
        if (file.size > 20000000) {
            notification('error', 'Помилка завантаження', 'Вказаний вами файл перевищує розмір в 20мб, оберіть файл відповідного розміру.');
            return false;
        }
        var fd = new FormData();
        fd.append("action", 'load_image');
        fd.append("style", 'attached_file');
        fd.append("image", file);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: fd,
            type: 'POST',
            processData: false,
            contentType: false,
            success: function (data) {
                console.log(data);
                if(data) {
                    $('#file_attached').val(data['id']);
                    $('.files-list').append(data['data']);
                    $('#load_file').prop('disabled', true);
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * add Advice question
     */
    $('#add_advice_question').on('submit', function (e) {           
        e.preventDefault();
        $(this).find('.submit').addClass('load');
        var data = {
            'action': 'add_advice_question',
            'user_id': api_settings.user_id
        };
        data = formatFormData($(this), data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                loadDoneButton('#add_advice_question');
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * update event
     */
    $('#ajax_event').on('submit', function (e) {           
        e.preventDefault();
        $(this).find('.submit').addClass('load');
        var data = {
            'action': 'add_event',
            'user_id': api_settings.user_id
        };
        data = formatFormData($(this), data);
        data['service'] = getTermsId('#service input:checked');
        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                loadDoneButton('#ajax_event');
                if(data['post_id']){
                    $('#ajax_event').append('<input type="hidden" name="post_id" value="'+data['post_id']+'">');
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * update news
     */
    $('#ajax_news').on('submit', function (e) {           
        e.preventDefault();
        $(this).find('.submit').addClass('load');
        var data = {
            'action'    : 'add_news',
            'user_id'   : api_settings.user_id,
            'authors'   : getAllAuthors()
        };
        data = formatFormData($(this), data);
        data['service'] = getTermsId('#service input:checked');
        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                loadDoneButton('#ajax_news');
                if(data['post_id']){
                    $('#ajax_news').append('<input type="hidden" name="post_id" value="'+data['post_id']+'">');
                }
                
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * update vacancy
     */
    $('#ajax_vacancy').on('submit', function (e) {           
        e.preventDefault();
        $(this).find('.submit').addClass('load');
        var data = {
            'action': 'add_vacancy',
            'user_id': api_settings.user_id
        };
        data = formatFormData($(this), data);
        data['service'] = getTermsId('#service input:checked');
        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                loadDoneButton('#ajax_vacancy');
                if(data['post_id']){
                    $('#ajax_vacancy').append('<input type="hidden" name="post_id" value="'+data['post_id']+'">');
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * load fields knowledgebase
     */

    $('#type_detect_know').on('change', function(){
        var type = $(this).val();
        console.log(type);

        var data = {
            'action'    : 'load_fields_knowledgebase',
            'type'      : type       
        };

        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                $('#content-field-area').html(data);
                setTimeout(function(){
                    selectStylize('.custom-fields');
                }, 500);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });


    //add/update knowledgebase

    $('#ajax_knowledgebase').on('submit', function(e){
        e.preventDefault();
        $(this).find('.submit').addClass('load');
        var data = {
            'action'    : 'add_knowledgebase',
            'user_id'   : api_settings.user_id,
            'authors'   : getAllAuthors()
        };
        data = formatFormData($(this), data);
        data['service'] = getTermsId('#service input:checked');
        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                loadDoneButton('#ajax_knowledgebase');
                if(data['post_id']){
                    $('#ajax_knowledgebase').append('<input type="hidden" name="post_id" value="'+data['post_id']+'">');
                }
                
                setTimeout(function(){
                    //location.reload();
                }, 3000);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * create/update
    */

    $('#ajax_resurse').on('submit', function(e){
        e.preventDefault();

        $(this).find('.submit').addClass('load');
        var data = {
            'action': 'ajax_resurse',
            'user_id': api_settings.user_id
        };

        data = formatFormData($(this), data);
        data['service'] = getTermsId('#service input:checked');

        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                loadDoneButton('#ajax_resurse');
                if(data['post_id']){
                    $('#ajax_resurse').append('<input type="hidden" name="post_id" value="'+data['post_id']+'">');
                }
                console.log(data);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * add comment consultation
     */
    $('#add_comment').on('submit', function (e) {           
        e.preventDefault();
        $(this).find('.submit').addClass('load');
        var data = {
            'action': 'add_comment',
            'user_id': api_settings.user_id
        };
        data = formatFormData($(this), data);

        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                loadDoneButton('#add_comment');
                setTimeout(function(){
                    //location.reload();
                }, 3000);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /**
     * add comment consultation
     */
    $('#question-done').on('click', function (e) {           
        e.preventDefault();
        questionSolve(true);
        var data = {
            'action'    : 'done_question',
            'post_id'   : $(this).attr('data-question'),
        };

        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                questionSolve(false);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    /*
    * enterprise ajax
    */
    $('#ajax_enterprise').on('submit', function (e) {           
        e.preventDefault();
        $(this).find('.submit').addClass('load');
        var data = {
            'action': 'update_enterprise',
            'user_id': api_settings.user_id
        };
        data = formatFormData($(this), data);
        data['service'] = getTermsId('#service input:checked');
        data['type_enterprises'] = getTermsId('#type_enterprises input:checked');
        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                loadDoneButton('#ajax_enterprise');
                if(data['post_id']){
                    $('#ajax_enterprise').append('<input type="hidden" name="post_id" value="'+data['post_id']+'">');
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    //add/remove bookmark
    $('.add_bookmark').on('click', function(e){
        e.preventDefault();

        that = this;

        if($(this).hasClass('added')){
            var event = 'remove_bookmark';
        }else{
            var event = 'add_bookmark'
        }

        var data = {
            'action'    : 'update_bookmark',
            'user_id'   : api_settings.user_id,
            'event'     : event,
            'ID'        : $(this).attr('data-id')
        };
        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                
                if (data.add) {
                    $(that).addClass('added');
                    notification('success', 'Успішно', data.add);
                } else {
                    $(that).removeClass('added');
                    notification('success', 'Успішно', data.remove);
                }
            },
            error: function (err) {
                console.log(err);
            }
        });
    })

    /*
    * account settings
    */
    $('#user_info').on('submit', function (e) {
        e.preventDefault();
        $(this).find('.submit').addClass('load');
        var data = {
            'action': 'update_user_info',
            'user_id': api_settings.user_id
        };
        data = formatFormData($(this), data);
        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                loadDoneButton('#user_info', true);
                console.log(data);
            },
            error: function (err) {
                console.log(err);
            }
        });
    });

    //filter
    $('#search_form').on('submit', function(e){
        e.preventDefault();
        $('#content').html('');
        current_page = 1;
        $('#load').slideDown();

        filterActivate();
    });
    $('#filter input, #filter selector').on('change', function(e){
        e.preventDefault();
        $('#content').html('');
        current_page = 1;
        $('#load').slideDown();

        filterActivate();
    });

    //load more filter
    $('#load_more_filter').on('click', function(){
        $('#load').slideDown();

        filterActivate();
    });

    //activate map
    if(jQuery('#map_canvas')[0]){
        initialize();
    }

    //додати поля
    $('#add_author').on('click', function(){
        $('#fields_get .content').append(
            `
            <fieldset class="fieldset-columns">
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <input type="text" class="input" placeholder="ПІБ">
                    <input type="text" class="input" placeholder="Поcилання на автора">
                    <span class="valid-notification"></span>
                </label>
            </fieldset>
            `
        );
    });



    /*************************************************************************************************************************************************
     * login/register/logout
     */

     //change password

    $('#change_password').on('submit', function(e){
        e.preventDefault();

        if($('#new_password').val() == $('#repeat_new_password').val()){
            $('#new_password, #repeat_new_password').css('border-color','');
        }else{
            $('#new_password, #repeat_new_password').css('border-color','red');
            notification('error', 'Помилка', 'Паролі не співпадають.');
            return false;
        }

        $(this).find('.submit').addClass('load');
        var data = {
            'action'        : 'change_password' 
        };
        data = formatFormData($(this), data);
        console.log(data);

        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                if(data['done']){
                    notification('success', 'Успішно', 'Пароль успішно змінено.');
                }else{
                    notification('error', 'Помилка', 'Поточний пароль вказано невірно.');
                }
                loadDoneButton('#change_password', true);
                console.log(data);
            },
            error: function (err) {
                console.log(err);
                notification('error', 'Помилка', 'Поточний пароль вказано невірно.');
            }
        });
    })


    //register
    $('#sign-form').on('submit', function (e) {
        e.preventDefault();
        var data = {
            'action'        : 'loginUser' 
        };
        data = formatFormData($(this), data);
        console.log(data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                if (data.done) {
                    notification('success', 'Успішно', 'Авторизація пройшла успішно');
                    closeModal($('#sign-in'));
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                } else {
                    notification('error', 'Помилка', 'Вказаний вами Email або пароль не вірні');
                }               
            },
            error: function (err) {
                notification('error', 'Помилка', 'Вказаний вами Email або пароль не вірні');
                console.log(err);
                //openInfoWindow('error', 'Error', 'Wrong data');
            }
        });
    });

    /**
     * register
     */
    $('#register-form').on('submit', function (e) {
        e.preventDefault();
        var data = {
            'action': 'register_user'
        };
        data = formatFormData($(this), data);

        if($('#password').val() == $('#password_repeat').val()){
            $('#password, #password_repeat').css('border-color','');
        }else{
            $('#password, #password_repeat').css('border-color','red');
            notification('error', 'Помилка', 'Паролі не співпадають.');
            return false;
        }

        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                if (data.complete) {
                    notification('success', 'Успішно', 'Реєстрація пройшла успішно');
                    closeModal($('#sign-up'));
                    setTimeout(function () {
                        location.reload();
                    }, 2000);
                } else {
                    if(data['error'] == 'fields'){
                        notification('error', 'Помилка', 'Не заповненні всі необхідні поля.');
                    }else{
                        notification('error', 'Помилка', 'З даним Email адрасою вже зареєстрований користувач');
                    }
                    
                }
            },
            error: function (err) {
                notification('error', 'Помилка', 'З даним Email адрасою вже зареєстрований користувач');
                console.log(err);
            }
        });
    });
    $('#logout').on('click', function (e) {
        e.preventDefault();
        var data = {
            'action': 'logout'
        };
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function () {
                location.href = location.origin;
            },
            error: function (err) {
                location.reload();
            }
        });
    });

    /**
     * reset password
     */
    $('#reset-password').on('submit', function (e) {
        e.preventDefault();
        var data = {
            'action': 'reset_pass'
        };
        data = formatFormData($(this), data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                if (data.complete) {
                    $('#input-login').val(data['login']);
                    notification('success', 'Успішно', 'Вам на електронну адресу відправлено лист з ключом для відновлення пароля.');
                    closeModal($('#reset-pass-modal'));
                    setTimeout(function () {
                        openModal($('#check-key-modal'));
                    }, 2000);
                } else {
                    notification('error', 'Помилка', 'Даного користувача не знайдено в системі');
                }
            },
            error: function (err) {
                notification('error', 'Помилка', 'Даного користувача не знайдено в системі');
                console.log(err);
            }
        });
    });

    /**
     * check key reset
     */
    $('#check-key').on('submit', function (e) {
        e.preventDefault();
        var data = {
            'action': 'reset_pass_key'
        };
        data = formatFormData($(this), data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                if (data.complete) {
                    $('#user_id').val(data['user_id']);
                    notification('success', 'Успішно', 'Ключ вказано вірно, тепер можете встановити новий пароль.');
                    closeModal($('#check-key-modal'));
                    setTimeout(function () {
                        openModal($('#new-password-modal'));
                    }, 500);
                } else {
                    notification('error', 'Помилка', 'Ключ не вірний або не дійсний.');
                }
            },
            error: function (err) {
                notification('error', 'Помилка', 'Ключ не вірний або не дійсний.');
                console.log(err);
            }
        });
    });

    /**
     * set new password
     */
    $('#new-password').on('submit', function (e) {
        e.preventDefault();
        var data = {
            'action': 'reset_pass_new'
        };
        data = formatFormData($(this), data);
        jQuery.ajax({
            url: api_settings.ajax_url,
            data: data,
            type: 'POST',
            success: function (data) {
                console.log(data);
                notification('success', 'Успішно', 'Пароль змінено тепер ви можете увійти в систему.');
                closeModal($('#new-password-modal'));
            },
            error: function (err) {
                notification('error', 'Помилка', 'При зміні пароля травилася помилка.');
                console.log(err);
            }
        });
    });


    //google map enterprice
    if($('#map_enterprises')[0]){
        filterActivate();
    }
})

function filterActivate(){
    var data = {
        'action'        : 'filter',
        'post_type'     : post_type,
        'page'          : current_page  
    };
    data = formatFormDataArr($('#filter'), data);
    data = formatFormData($('#search_form'), data);

    console.log(data);

    jQuery.ajax({
        url: api_settings.ajax_url,
        data: data,
        type: 'POST',
        success: function (data){
            console.log(data);
            if(data.no_result){
                notification('error', 'Помилка', 'За вашим запитом записів не знайдено!');
                //$('#content').append('<h1>За вашим запитом записів не знайдено!</h1>');
                $('.load-more-sec').slideUp();
            }else{
                if(data.posts){
                    initMapEnterprices(data.posts);
                }else{
                    $('#content').append(data.data);
                    max_pages   = data.pages;
                    if(max_pages <= current_page){
                        $('.load-more-sec').slideUp();
                    }
                    current_page++;
                }
            }
            $('#load').slideUp();
        },
        error: function (err) {
            console.log(err);
        }
    });
}


var that;

//get all author
function getAllAuthors(){
    var values = [];
    $('#fields_get fieldset').each(function(el){
        console.log(el);
        var valEl = [];
        var $inputs = $(this).find('input');
        console.log($inputs);
        $inputs.each(function(elInput){
            valEl.push($(this).val());
        })
        values.push(valEl);
    });
    return values;
}

//map
function initialize() {
    geocoder = new google.maps.Geocoder();
    var latlng = new google.maps.LatLng(50.427920, 30.513639);
    var myOptions = {
      zoom: 6,
      center: latlng,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }
    var map = new google.maps.Map(document.getElementById("map_canvas"), myOptions);
    var markers = [];

    // Create the search box and link it to the UI element.
    var input = document.getElementById('pac-input');
    var searchBox = new google.maps.places.SearchBox(input);
    //map.controls[google.maps.ControlPosition.TOP_LEFT].push(input);

    // Bias the SearchBox results towards current map's viewport.
    map.addListener('bounds_changed', function() {
      searchBox.setBounds(map.getBounds());
    });

    var markers = [];
        // Listen for the event fired when the user selects a prediction and retrieve
        // more details for that place.
    searchBox.addListener('places_changed', function() {
        var places = searchBox.getPlaces();

        if (places.length == 0) {
            return;
        }

        var componentMap = {
            country: 'country',
            locality: 'locality',
            administrative_area_level_1 : 'administrative_area_level_1',
        }

        var placeInfo = searchBox.getPlaces()[0].address_components;

        placeInfo.forEach(function(el){
            $city = 'locality';
            $city = 'country';
            if(el.types.includes('locality')){
                $('#city').val(el.long_name);
            }else if(el.types.includes('country')){
                $('#country').val(el.long_name);
            }
        });

        // Clear out the old markers.
        markers.forEach(function(marker) {
        marker.setMap(null);
        });
        markers = [];

        // For each place, get the icon, name and location.
        var bounds = new google.maps.LatLngBounds();
        places.forEach(function(place) {
        if (!place.geometry) {
            console.log("Returned place contains no geometry");
            return;
        }
        var icon = {
            size: new google.maps.Size(71, 71),
            origin: new google.maps.Point(0, 0),
            anchor: new google.maps.Point(17, 34),
            scaledSize: new google.maps.Size(25, 25)
        };

        // Create a marker for each place.
        console.log(searchBox.getPlaces()[0].address_components);
        jQuery('#geo_position').val(place.geometry.location.lat()+','+place.geometry.location.lng());
        markers.push(new google.maps.Marker({
            map: map,
            title: place.name,
            position: place.geometry.location
        }));

        if (place.geometry.viewport) {
            // Only geocodes have viewport.
            bounds.union(place.geometry.viewport);
        } else {
            bounds.extend(place.geometry.location);
        }
        });
        map.fitBounds(bounds);
    });

    google.maps.event.addListener(map, 'click', function(event){   

        console.log(event);
        var lat = event.latLng.lat();
        var lng = event.latLng.lng();
        jQuery('#geo_position').val(lat+','+lng);

        setMapOnAll(null);
        markers = [];

        addMarker({lat: lat, lng: lng});
        setMapOnAll(map);  
    });

    if(typeof coords != 'undefined'){
        var coordsArr = coords.split(',');
        console.log(coordsArr);
        addMarker({lat: +coordsArr[0], lng: +coordsArr[1]});
        setMapOnAll(map);
    }

    function setMapOnAll(map) {
        for (var i = 0; i < markers.length; i++) {
            markers[i].setMap(map);
        }
    }

    function addMarker(location) {
        var marker = new google.maps.Marker({
            position: location,
            map: map,
            title: ''
        });
        markers.push(marker);
        console.log(location);
    }
}

/*
* animation of solve question
*/
function questionSolve(status){

    if(status){
        $('.btn-another').addClass('display-none');
        $('body').find('.message-button').addClass('question-solved');
        $('body').find('.submit').addClass('load');
    }else{
        setTimeout(function() {
            $('.communication-block').find('.btn-solve').addClass('done');
        }, 1000);
    }
}

//get all terms_id
function getTermsId(selector){
    var terms = [];
    $(selector).each(function(){
        terms.push(+$(this).val());
    });
    return terms;
}


  /*
  * google map 
  */

function initMapEnterprices(data) {
    let first;
    if(data){
        if(data.length > 0){
            first = {lat: +data[0].latitude, lng: +data[0].longitude};
        }else{
            first = {lat: 20.345750, lng: 6.193636};
        }                    
    }else{
        first = {lat: 49.492566, lng: 30.874410 };
    }
    
    let bounds  = new google.maps.LatLngBounds();
    
    let map = new google.maps.Map(document.getElementById('map_enterprises'), {
      zoom: 6,      
      center: first,
      disableDefaultUI: true,
      zoomControl: true,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    }); 

    map.setOptions({ minZoom: 2, maxZoom: 18 });

    if(data){

        var locations = [];
        var markers = [];
        var labels = 'ABCDEFGHIJKLMNOPQRSTUVWXYZ';

        data.forEach(function(el){    
            
            if(el.geo){
                var coords = el.geo.split(',');
              var marker = new google.maps.Marker({
                  position: {lat: +coords[0], lng: +coords[1]},
                  center: {lat: +coords[0] , lng: +coords[1]},
                  //icon: api_settings.template + '/dist/assets/images/pin.png',
                  map: map
              });

              markers.push(marker); 


              (function (marker, info) {

                  var contentString =
                  '<div id="content">' +
                  '<div id="siteNotice" class="content-map">' +
                  '<h3><b>' + info.title + '</b></h3>' +
                  '<p>' + info.info.address + '</p>' +
                  '<p><a href="'+info.url+'">сторінка підприємства</a></p>' +
                  '<p><a target="_blank" href="https://www.google.com.ua/maps/place/'+el.geo+'">Відкрити на карті</a></p>' +
                  '</div>' +
                  '</div>';

                  var infowindow = new google.maps.InfoWindow({
                      content: contentString
                  });

                  google.maps.event.addListener(marker, 'click', function() {
                      jQuery('.gm-style-iw').next().click();
                      infowindow.open(map, marker);
                  });
                  
                  let loc = new google.maps.LatLng(marker.position.lat(), marker.position.lng());
                  bounds.extend(loc);

                  google.maps.event.addListener(infowindow, 'domready', function() {

                      var iwOuter = jQuery('.gm-style-iw');

                      var iwBackground = iwOuter.prev();

                  });


              })(marker, el);    
          }

        });

        var clusterStyles = [
            {
                textColor: 'white',
                url: api_settings.template + '/img/m1.png',
                height: 52,
                width: 52,
                anchor:[52,52],
            },
            {
                textColor: 'white',
                url: api_settings.template + '/img/m1.png',
                height: 52,
                width: 52,
                anchor:[52,52],
            },
            {
                textColor: 'white',
                url: api_settings.template + '/img/m1.png',
                height: 52,
                width: 52,
                anchor:[52,52],
            }
        ];

        var mcOptions = {                   
            gridSize: 50,
            styles: clusterStyles,
            maxZoom: 9
        };
        
        map.fitBounds(bounds);
        map.panToBounds(bounds); 

        var markerCluster = new MarkerClusterer(map, markers, mcOptions);

    }
  }