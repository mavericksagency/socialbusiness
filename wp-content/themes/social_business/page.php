<?php get_header(); 
    the_post();
?>
	<main class="main post-page">
		<div class="main-grid">
			<div class="left-column">
				<?php get_sidebar(); ?>
			</div>
			<div class="double-column">
            <div class="post-modal">
                <div class="modal-inner">
                    <div class="center-column">
                        <div class="post-main">
                            <div class="post-article">
                                <h1 class="post-title">
                                    <?php the_title(); ?>
                                </h1>
                                <div class="content">
                                    <?php the_content(); ?>
                                </div>
                            </div>
                        </div>
                    </div> 
                </div>
            </div>
			</div>
		</div>
	</main>
<?php get_footer();