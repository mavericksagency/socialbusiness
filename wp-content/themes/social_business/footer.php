<footer class="footer">
	<div class="footer-row">
		<div class="wrap">
			<div class="footer-logos">
				<a href="/" class="logo">
					<svg class="icon">
						<use xlink:href="#logo">
					</svg>
				</a>
				<a href="/" class="euro-logo">
					<img class="icon" src="<?php bloginfo('template_directory'); ?>/img/icons/euro-logo.svg" alt="">
				</a>
			</div>
			<nav class="footer-menu">
				<?php
					wp_nav_menu(array(
						'theme_location'  	=> 'footer-menu',
						'container'			=> false,
						'fallback_cb'		=> false,
						'items_wrap'      => '<ul class="footer-menu-list">%3$s</ul>',
						'depth'				=> 1
					));
				?>
			</nav>
		</div>
	</div>
	<div class="footer-copyright">
		<div class="wrap">
			<div class="copyright">
				<span class="copy">
					&copy; Social Business In UA 2018. Всі права захищені
				</span>
				<div class="signature">
					<span class="signature-title">
						Designed and developed by
					</span>
					<a href="https://mavericks.com.ua" target="_blank" class="signature-link">
						<img src="<?php bloginfo('template_directory'); ?>/img/mavericks.png" alt="">
					</a>
				</div>
			</div>
			<div class="footer-links">
				<?php 
					$social_network = get_field('social_network', 'option');
					foreach($social_network as $key=>$soc){
						if(!$soc){
							continue;
						}
						?>
						<a href="<?= $soc; ?>" target="_blank" class="social-link <?= $key; ?>">
							<svg class="icon">
								<use xlink:href="#<?= $key; ?>">
							</svg>
						</a>
						<?php
					}
				?>
			</div>
		</div>
	</div>
</footer>
<?php include('inc/notification.php'); ?>
<?php include('inc/sign_up.php'); ?>
<?php include('inc/sign_in.php'); ?>
<?php include('inc/reset.php'); ?>
<?php include('inc/search.php'); ?>
<?php //include('inc/share_window.php'); ?>
<?php wp_footer(); ?>
<?php 
	if(isset($_GET['key_reset']) && isset($_GET['login'])){
		?>
		<script>
			setTimeout(function(){
				console.log('Run reset!');
				$('#check-key').submit();
			}, 1000);
			
		</script>
		<?php
	}
?>
<?php 
	global $type_event_get, $type_knowledge_base_field, $type_resourse_field, $inUa;
	if(!isset($_GET['my_question'])){
		if((($type_resourse_field || $type_knowledge_base_field || $type_event_get || $inUa) && count($_GET) > 0) && (is_archive() || is_page_template('page_templates/collective.php'))){
			
			?>
			<script>
			//alert('map');
				if($('#filter')[0]){
					current_page = 1;
					$('#content').html('');
					filterActivate();
				}
			</script>
			<?php
		}
	}
?>
</body>
</html>