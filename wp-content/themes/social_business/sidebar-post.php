<?php  
    global $post;
?>
<div class="right-column">
    <div class="more-posts">
        <h3 class="more-posts-header-title">
            Може бути цікаво
        </h3>
        <div class="more-posts-slider">
            <?php 
                $term_ids   = get_terms_ids($post->ID);
                $args 		= array(
                    'post_type'			=> $post->post_type,
                    'posts_per_page'	=> 5,
                    'post_status'   	=> 'publish',
                    'order'				=> 'DESC',
                    'post__not_in'		=> array($post->ID)
                    //terms choise variation 
                ); 
                if($post->post_type == 'event'){
                    $today              = date("d.m.Y");
                    $args['meta_key']   =  'custom_date';
                    $args['orderby']      =  'meta_value';
                    $args['order']		= 'DESC';
                    $args['meta_query'] = array(
                        array(
                            'key'       => 'custom_date',
                            'compare'   => '>=',
                            'value'     => $today,
                            'type'      => 'DATE'
                        )
                    );
                }
                $query 		= new WP_Query($args);
                if($query->posts){
                    foreach($query->posts as $post_pop){
                        $url 	= get_the_permalink($post_pop->ID);
                        $title	= get_the_title($post_pop->ID);
                        $image  = get_the_post_thumbnail_url($post_pop->ID, 'news-image');
                        
                        ?>
                        <div class="more-post-item">
                            <a href="<?= $url; ?>" class="more-post-image <?php if(!$image){echo 'inner-svg '; echo $post_pop->post_type;} ?>">
                                <?php 
                                    if($image){
                                        ?>
                                        <img class="img" src="<?= $image; ?>" alt="<?= $title; ?>">
                                        <?php
                                    }else{
                                        $iconPublish   = getIconPostType($post_pop->ID, $post_pop->post_type);
                                        ?>
                                        <svg class="icon svg-fill">
                                            <use xlink:href="#<?= $iconPublish; ?>"></use>
                                        </svg>
                                        <?php
                                    }
                                ?>
                                
                            </a>
                            <div class="more-post-info">
                                <a href="<?= $url; ?>" class="more-post-title">
                                    <?= $title ?>
                                </a>
                                <div class="more-post-type">
                                    <!-- <span class="type-icon">
                                        <svg class="icon">
                                            <use xlink:href="#star">
                                        </svg>
                                    </span> -->
                                    <!-- <span class="type-text">
                                        Блог
                                    </span> -->
                                </div>
                            </div>
                        </div>
                        <?php
                    }
                }
            ?>
        </div>
    </div>
    <?php if($post->post_type != 'online_cons'){ ?>
        <?php if(adminCheck($post->ID)){ ?>
            <a href="<?= get_the_permalink(53) ?>?<?= $post->post_type.'&post_id='.$post->ID; ?>" class="btn-block edit-pen">
                <svg class="icon">
                    <use xlink:href="#pen">
                </svg>
                <span class="text">
                    Редагувати
                </span>
            </a>
        <?php 
        } 
    }
    ?>
</div>