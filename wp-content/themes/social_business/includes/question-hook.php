<?php

add_action('save_post_online_cons', 'publish_consultation', 55, 3);
function publish_consultation($post_id, $post, $update){

    if($update){

        $consultant = get_field('consultant', $post_id);

        notificationConNewQuestion($consultant['user_email'], $post_id);
    }
    
}