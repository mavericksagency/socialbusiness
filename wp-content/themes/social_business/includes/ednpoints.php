<?php

$authorField = 'field_5bbca03d156f3';
$authors     = 'field_5bc8556018c0c';
$public      = 'field_5bc8557718c0d';

//ajax upload image
function uploadimage()
{

    $attachment_id = media_handle_upload('image', 0);

    $result = array(
        'url'   => wp_get_attachment_url($attachment_id),
        'id'    => $attachment_id,
        'title' => get_the_title($attachment_id)
    );

    if ($_POST['style'] == 'attached_file') {
        global $file_ID, $file_title;
        $file_ID        = $attachment_id;
        $file_title     = get_the_title($attachment_id);
        ob_start();

        get_template_part('partials/file/file_attached');

        $data = ob_get_clean();
    }
    $result['data'] = $data;

    wp_send_json($result);
}

add_action('wp_ajax_load_image', 'uploadimage');


//ajax load user
function loadUser()
{

    global $userPost;

    $userPost = get_user_by('email', sanitize_text_field($_POST['email']));

    if ($userPost == false) {
        $result['error'] = true;
        wp_send_json($result);
    }

    $result['id_user']  = $userPost->ID;

    ob_start();

    get_template_part('partials/user/small');

    $data = ob_get_clean();

    $result['data']     = $data;

    wp_send_json($result);
}

add_action('wp_ajax_load_user', 'loadUser');

/**
 * add question cunsultation question
 */
function addComment()
{

    $post_id    = (int) sanitize_text_field($_POST['post_id']);
    $user_id    = (int) sanitize_text_field($_POST['user_id']);

    $commentdata = array(
        'comment_post_ID'      => $post_id,
        'comment_content'      => sanitize_text_field($_POST['comment']),
        'comment_author_email' => get_the_author_meta('email', $_POST['user_id']),
        'comment_author'       => get_the_author_meta('first_name', $_POST['user_id']) . ' ' . get_the_author_meta('last_name', $_POST['user_id']),
        'user_id'              => $user_id,
    );

    $comment_id = wp_insert_comment($commentdata);

    update_field('field_5bf6e2b982a84', $_POST['file_id'], 'comment_' . $comment_id);

    $consultant = get_field('consultant', $post_id);

    if ($user_id == $consultant['ID']) {
        //send notification user who create this question
        $author_id     = get_field('id_author', $post_id);
        notificationtoUser(get_the_author_meta('email', $author_id), $post_id);
    } else {
        //or consultant
        notificationtoConsultant($consultant['user_email'], $post_id);
    }

    wp_send_json($commentdata);
}

add_action('wp_ajax_add_comment', 'addComment');

/**
 * add question done
 */
function questionDone()
{

    $result = update_field('field_5bbf4f18baa5c', 1, sanitize_text_field($_POST['post_id']));

    wp_send_json($result);
}

add_action('wp_ajax_done_question', 'questionDone');

/**
 * create/update enterprise
 */
function create_advice_question()
{
    global $authorField;

    $post_data = array(
        'post_title'    => sanitize_text_field($_POST['title']),
        'post_content'  => $_POST['content'],
        'post_type'     => 'online_cons',
        'post_status'   => 'draft',
        'post_author'   => 1,
        'post_excerpt'  => '',
    );

    $post_id = wp_insert_post($post_data, true);

    //check error
    if (is_wp_error($post_id)) {
        $result['error'] = $post_id->get_error_message();
        wp_die();
    }

    //tax
    wp_set_post_terms($post_id, $_POST['service'], 'service', false);
    wp_set_post_terms($post_id, (int) $_POST['topics'], 'topics', false);

    //set file
    update_field('field_5bbf1f8a7b401', sanitize_text_field($_POST['file']), $post_id);


    //author
    update_field('field_5bf5792700828', sanitize_text_field($_POST['user_id']), $post_id);

    createNewQuestion($post_id);

    wp_send_json($_POST);
}

add_action('wp_ajax_add_advice_question', 'create_advice_question');

/**
 * create/update event
 */
function updateEvent()
{
    global $authorField, $public, $current_user;

    $response['inside'] = true;

    if ($_POST['post_id']) {

        $post_data = array(
            'ID'            => sanitize_text_field($_POST['post_id']),
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content']
        );
        wp_update_post($post_data);

        $post_id = sanitize_text_field($_POST['post_id']);
    } else {
        $post_data = array(
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content'],
            'post_type'     => 'event',
            'post_status'   => $current_user->roles[0] == 'user_vis' ? 'draft' : 'publish',
            'post_author'   => 1,
            'post_excerpt'  => '',
        );

        $post_id = wp_insert_post($post_data, true);

        //check error
        if (is_wp_error($post_id)) {
            $result['error'] = $post_id->get_error_message();
            wp_die();
        }
        $response['post_id'] = $post_id;
    }



    $fields         = array(
        'main_info'     => 'field_5bc09dc016b90',
        'place'         => 'field_5bbf93faec150',
        'date'          => 'field_5bbf93ccec14d'
    );

    $data                   = array(
        'date'              => sanitize_text_field($_POST['date']),
        'main_info'         => array(
            'time_start'    => sanitize_text_field($_POST['time_start']),
            'time_end'      => sanitize_text_field($_POST['time_end']),
            'end_register'  => sanitize_text_field($_POST['end_register']),
            'url_register'  => sanitize_text_field($_POST['url_register']),
            'price'         => sanitize_text_field($_POST['price']),
            'description'   => sanitize_text_field($_POST['description']),
            'category'      => sanitize_text_field($_POST['category']),
            'contact'       => sanitize_text_field($_POST['contact']),
        ),
        'place'             => array(
            'address'       => sanitize_text_field($_POST['place']),
            'coordinates'   => sanitize_text_field($_POST['geo_position']),
        )
    );

    //image
    set_post_thumbnail($post_id, sanitize_text_field($_POST['id_photo']));

    //tax
    wp_set_post_terms($post_id, (int) $_POST['type_event'], 'type_event', false);

    //date
    update_field($fields['date'], $data['date'], $post_id);
    //main_info
    update_field($fields['main_info'], $data['main_info'], $post_id);

    //place
    update_field($fields['place'], $data['place'], $post_id);
    //public
    update_field($public, sanitize_text_field($_POST['public']), $post_id);

    //tax
    wp_set_post_terms($post_id, (int) $_POST['topics'], 'topics', false);
    wp_set_post_terms($post_id, $_POST['service'], 'service', false);
    wp_set_post_terms($post_id, $_POST['type_event'], 'type_event', false);
    wp_set_post_terms($post_id, (int) $_POST['geo'], 'geo', false);


    wp_send_json($response);
}

add_action('wp_ajax_add_event', 'updateEvent');

/**
 * create/update news
 */
function updateNews()
{
    global $authorField, $authors, $public, $current_user;

    $response['inside'] = true;

    if ($_POST['post_id']) {

        $post_data = array(
            'ID'            => sanitize_text_field($_POST['post_id']),
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content']
        );
        wp_update_post($post_data);

        $post_id = sanitize_text_field($_POST['post_id']);
    } else {

        $post_data = array(
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content'],
            'post_type'     => 'news',
            'post_status'   => $current_user->roles[0] == 'user_vis' ? 'draft' : 'publish',
            'post_author'   => 1,
            'post_excerpt'  => '',
        );

        $post_id = wp_insert_post($post_data, true);

        //check error
        if (is_wp_error($post_id)) {
            $result['error'] = $post_id->get_error_message();
            wp_die();
        }

        $response['post_id'] = $post_id;
    }

    //image
    set_post_thumbnail($post_id, sanitize_text_field($_POST['id_photo']));

    //description
    update_field('field_5bc5c679df691', sanitize_text_field($_POST['description']), $post_id);

    //tax
    wp_set_post_terms($post_id, (int) $_POST['topics'], 'topics', false);
    wp_set_post_terms($post_id, $_POST['service'], 'service', false);
    wp_set_post_terms($post_id, (int) $_POST['geo'], 'geo', false);

    //public
    update_field($public, sanitize_text_field($_POST['public']), $post_id);
    //authors
    update_field($authors, json_encode($_POST['authors'], JSON_UNESCAPED_UNICODE), $post_id);

    wp_send_json($response);
}

add_action('wp_ajax_add_news', 'updateNews');

/**
 * update Knowledgebase
 */
function updateKnowledgebase()
{
    global $authors, $public, $current_user;

    $response['inside'] = true;

    if ($_POST['post_id']) {

        $post_data = array(
            'ID'            => sanitize_text_field($_POST['post_id']),
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content']
        );
        wp_update_post($post_data);

        $post_id = sanitize_text_field($_POST['post_id']);
    } else {

        $post_data = array(
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content'],
            'post_type'     => 'knowledge_base',
            'post_status'   => $current_user->roles[0] == 'user_vis' ? 'draft' : 'publish',
            'post_author'   => 1,
            'post_excerpt'  => '',
        );

        $post_id = wp_insert_post($post_data, true);

        //check error
        if (is_wp_error($post_id)) {
            $result['error'] = $post_id->get_error_message();
            wp_send_json($result);
            wp_die();
        }
        $response['post_id'] = $post_id;
    }

    $fields         = array(
        'type'          => 'field_5bc5dad95b031',
        'description'   => 'field_5bc5ffa258880',
        'url_book'      => 'field_5bc5ff5c5887d',
        'url_video'     => 'field_5bc5ff8f5887f',
        'url_material'  => 'field_5bc6e9aa6823b',
        'url_course'    => 'field_5be3f159099bf',
        'url_manual'    => 'field_5bf560406f620'
    );

    //image
    set_post_thumbnail($post_id, sanitize_text_field($_POST['id_photo']));

    //type
    update_field($fields['type'], sanitize_text_field($_POST['type']), $post_id);
    //description
    update_field($fields['description'], sanitize_text_field($_POST['description']), $post_id);
    //url_book
    update_field($fields['url_book'], sanitize_text_field($_POST['url_book']), $post_id);
    //url_material
    update_field($fields['url_material'], sanitize_text_field($_POST['url_material']), $post_id);
    //url_course
    update_field($fields['url_course'], sanitize_text_field($_POST['url_course']), $post_id);
    //url_video
    update_field($fields['url_video'], sanitize_text_field($_POST['url_video']), $post_id);
    //url_manual
    update_field($fields['url_manual'], sanitize_text_field($_POST['url_manual']), $post_id);
    //public
    update_field($public, sanitize_text_field($_POST['public']), $post_id);
    //authors
    update_field($authors, json_encode($_POST['authors'], JSON_UNESCAPED_UNICODE), $post_id);


    //author
    update_field($authorField, sanitize_text_field($_POST['user_id']), $post_id);

    //tax
    wp_set_post_terms($post_id, (int) $_POST['topics'], 'topics', false);
    wp_set_post_terms($post_id, $_POST['service'], 'service', false);
    wp_set_post_terms($post_id, (int) $_POST['geo'], 'geo', false);
    wp_set_post_terms($post_id, (int) $_POST['language'], 'language', false);


    wp_send_json($_POST);
}

add_action('wp_ajax_add_knowledgebase', 'updateKnowledgebase');

/**
 * update Resources
 */
function updateResources()
{

    global $current_user;

    $response['inside'] = true;

    if ($_POST['post_id']) {

        $post_data = array(
            'ID'            => sanitize_text_field($_POST['post_id']),
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content']
        );
        wp_update_post($post_data);

        $post_id = sanitize_text_field($_POST['post_id']);
    } else {
        $post_data = array(
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content'],
            'post_type'     => 'resources',
            'post_status'   => $current_user->roles[0] == 'user_vis' ? 'draft' : 'publish',
            'post_author'   => 1,
            'post_excerpt'  => '',
        );

        $post_id = wp_insert_post($post_data, true);

        //check error
        if (is_wp_error($post_id)) {
            $result['error'] = $post_id->get_error_message();
            wp_send_json($result);
            wp_die();
        }
        $response['post_id'] = $post_id;
    }

    $fields                     = array(
        'type'                  => 'field_5b74070a20a68',
        'city'                  => 'field_5bc701c591d1d',
        'phone'                 => 'field_5bc701e091d1e',
        'email'                 => 'field_5bc701e891d1f',
        'url_soc'               => 'field_5bc701ee91d20',
        'education'             => 'field_5bc701f591d21',
        'keys'                  => 'field_5bc701fa91d22',
        'org_before'            => 'field_5bc7020591d23',
        'course_information'    => 'field_5bc70a9272d79',
        'faculty'               => 'field_5bc70abc72d7a',
        'address'               => 'field_5bc70af072d7b',
        'contact_person'        => 'field_5bc70c12fa2e9',
        'support'               => 'field_5bc72fab7ee66',
        'page_administrators'   => 'field_5bbb561b9e6de',
        'about'                 => 'field_5bea9f8718426'
    );

    //image
    set_post_thumbnail($post_id, sanitize_text_field($_POST['id_photo']));

    //type
    update_field($fields['type'], sanitize_text_field($_POST['type']), $post_id);
    //city
    update_field($fields['city'], sanitize_text_field($_POST['city']), $post_id);
    //phone
    update_field($fields['phone'], sanitize_text_field($_POST['phone']), $post_id);
    //email
    update_field($fields['email'], sanitize_text_field($_POST['email']), $post_id);
    //url_soc
    update_field($fields['url_soc'], sanitize_text_field($_POST['url_soc']), $post_id);
    //education
    update_field($fields['education'], sanitize_text_field($_POST['education']), $post_id);
    //keys
    update_field($fields['keys'], sanitize_text_field($_POST['keys']), $post_id);
    //org_before
    update_field($fields['org_before'], sanitize_text_field($_POST['org_before']), $post_id);
    //course_information
    update_field($fields['course_information'], sanitize_text_field($_POST['course_information']), $post_id);
    //faculty
    update_field($fields['faculty'], sanitize_text_field($_POST['faculty']), $post_id);
    //address
    update_field($fields['address'], sanitize_text_field($_POST['address']), $post_id);
    //contact_person
    update_field($fields['contact_person'], array('person' => sanitize_text_field($_POST['person']), 'position' => sanitize_text_field($_POST['position'])), $post_id);
    //support
    update_field($fields['support'], sanitize_text_field($_POST['support']), $post_id);
    //about
    update_field($fields['about'], sanitize_text_field($_POST['about']), $post_id);

    //author
    update_field($authorField, sanitize_text_field($_POST['user_id']), $post_id);

    //contacts
    update_field($fields['page_administrators'], sanitize_text_field($_POST['page_administrators']), $post_id);

    //tax
    wp_set_post_terms($post_id, (int) $_POST['topics'], 'topics', false);
    wp_set_post_terms($post_id, $_POST['service'], 'service', false);
    wp_set_post_terms($post_id, (int) $_POST['geo'], 'geo', false);

    wp_send_json($response);
}

add_action('wp_ajax_ajax_resurse', 'updateResources');

/**
 * load fields
 */
function loadFields()
{

    ob_start();

    get_template_part('partials/edit/knowledgebase/' . $_POST['type']);

    $data = ob_get_clean();

    wp_send_json($data);
}

add_action('wp_ajax_load_fields_knowledgebase', 'loadFields');

/**
 * create/update vacancy
 */
function addVacancy()
{
    global $authorField, $public, $authors;

    $response['inside'] = true;

    if ($_POST['post_id']) {

        $post_data = array(
            'ID'            => sanitize_text_field($_POST['post_id']),
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content'],
        );

        wp_update_post($post_data);

        $post_id = sanitize_text_field($_POST['post_id']);
    } else {
        $post_data = array(
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => $_POST['content'],
            'post_type'     => 'opportunities',
            'post_status'   => 'draft',
            'post_author'   => 1,
            'post_excerpt'  => '',
        );

        $post_id = wp_insert_post($post_data, true);

        //check error
        if (is_wp_error($post_id)) {
            $result['error'] = $post_id->get_error_message();
            wp_send_json($result);
            wp_die();
        }

        $response['post_id'] = $post_id;
    }

    //image
    set_post_thumbnail($post_id, sanitize_text_field($_POST['id_photo']));

    //description
    update_field('field_5bc5cc4d2149e', sanitize_text_field($_POST['description']), $post_id);
    //salary
    update_field('field_5bc5cc3d2149d', sanitize_text_field($_POST['salary']), $post_id);
    //from
    update_field('field_5bc5cc972149f', sanitize_text_field($_POST['public']), $post_id);
    //public
    update_field($public, sanitize_text_field($_POST['public']), $post_id);
    //authors
    update_field($authors, json_encode($_POST['authors'], JSON_UNESCAPED_UNICODE), $post_id);

    //tax
    wp_set_post_terms($post_id, (int) $_POST['topics'], 'topics', false);
    wp_set_post_terms($post_id, $_POST['service'], 'service', false);
    wp_set_post_terms($post_id, (int) $_POST['geo'], 'geo', false);

    wp_send_json($response);
}

add_action('wp_ajax_add_vacancy', 'addVacancy');

/**
 * remove post
 */
function removePost()
{
    global $user_id;

    $response['post']           = get_post((int) $_POST['id']);

    if ($_GET['id'] && !adminCheck($_GET['post_id'])) {
        wp_die();
    }

    if ($response['post']->post_type == 'enterprises' || $response['post']->post_type == 'resources') {
        $args = array(
            'post_type'         => array('news', 'event', 'opportunities', 'knowledge_base'),
            'posts_per_page'    => -1,
            'order'             => 'DESC',
            'meta_query'        => array(
                'public'        => array(
                    'key'       => 'public',
                    'value'     => $response['post']->post_type . '_' . $response['post']->ID,
                    'compare'   => 'LIKE',
                )
            )
        );
        $query_all              = new WP_Query($args);
        $response['posts'] = $query_all->posts;
        if ($query_all->posts) {
            foreach ($query_all->posts as $item) {
                $response['foreach'][] = $item;
                wp_trash_post($item->ID);
            }
        }
    }

    wp_trash_post($response['post']->ID);

    wp_send_json($response);
}

add_action('wp_ajax_remove_post', 'removePost');

/**
 * create/update enterprise
 */
function update_enterprise()
{

    global $current_user;

    if (!$_POST['post_id']) {

        $post_data = array(
            'post_title'    => sanitize_text_field($_POST['title']),
            'post_content'  => ' ',
            'post_type'     => 'enterprises',
            'post_status'   => $current_user->roles[0] == 'user_vis' ? 'draft' : 'publish',
            'post_author'   => 1,
            'post_excerpt'  => '',
        );

        $post_id = wp_insert_post($post_data, true);

        //check error
        if (is_wp_error($post_id)) {
            echo $post_id->get_error_message();
            wp_die();
        }

        $response['post_id'] = $post_id;
    } else {
        $post_data = array(
            'ID'            => sanitize_text_field($_POST['post_id']),
            'post_title'    => sanitize_text_field($_POST['title'])
        );
        wp_update_post($post_data);

        $response['update'] = true;
        $post_id = sanitize_text_field($_POST['post_id']);
    }



    $field = array(
        'main'                  => 'field_5bbb54a39e6d0',
        'contacts'              => 'field_5bbb55229e6d3',
        'workers'               => 'field_5bbb55d59e6dc',
        'add_info'              => 'field_5bbb56699e6e0',
        'author'                => 'field_5bbca03d156f3',
        'geo_position'          => 'field_5bbde23f13b51',
        'page_administrators'   => 'field_5bbb561b9e6de'
    );

    $data = array(
        'main'                      => array(
            'form_of_activity'      => sanitize_text_field($_POST['form_of_activity']),
            //'commercial_activity'   => sanitize_text_field($_POST['commercial_activity']),
            'date_of_foundation'    => sanitize_text_field($_POST['date_of_foundation'])
        ),
        'contacts'                  => array(
            'country'               => sanitize_text_field($_POST['country']),
            'address'               => sanitize_text_field($_POST['address']),
            'city'                  => sanitize_text_field($_POST['city']),
            'phone'                 => sanitize_text_field($_POST['phone']),
            'hide_phone'            => $_POST['hide_phone'] != 1 ? 0 : 1,
            'email'                 => sanitize_text_field($_POST['email']),
            'hide_email'            => $_POST['hide_email'] != 1 ? 0 : 1,
            'website'               => sanitize_text_field($_POST['website']),
            'hide_website'          => $_POST['hide_website'] != 1 ? 0 : 1,
        ),
        'workers'                   => array(
            'manager_enterprise'    => sanitize_text_field($_POST['manager_enterprise']),
            //'site_administrators'   => sanitize_text_field($_POST['site_administrators']),
            'number_of_employees'   => sanitize_text_field($_POST['number_of_employees'])
        ),
        'add_info'                  => array(
            'services'              => sanitize_text_field($_POST['services']),
            'mission_enterprise'    => sanitize_text_field($_POST['mission_enterprise']),
            'story_creation'        => sanitize_text_field($_POST['story_creation']),
            'history_of_success'    => sanitize_text_field($_POST['history_of_success']),
            'real_companys_bus'     => sanitize_text_field($_POST['real_companys_bus']),
            'profit_fin_ind'        => sanitize_text_field($_POST['profit_fin_ind']),
            'impact_activity'       => sanitize_text_field($_POST['impact_activity']),
            'about_enterprise'      => sanitize_text_field($_POST['about_enterprise'])
        )
    );

    //tax
    wp_set_post_terms($post_id, $_POST['service'], 'service', false);
    wp_set_post_terms($post_id, (int) $_POST['geo'], 'geo', false);
    wp_set_post_terms($post_id, $_POST['type_enterprises'], 'type_enterprises', false);
    wp_set_post_terms($post_id, $_POST['beneficiaries'], 'beneficiaries', false);

    //image
    set_post_thumbnail($post_id, sanitize_text_field($_POST['id_photo']));

    //author
    update_field($field['author'], sanitize_text_field($_POST['user_id']), $post_id);

    //main
    update_field($field['main'], $data['main'], $post_id);
    //contacts
    update_field($field['contacts'], $data['contacts'], $post_id);
    //contacts
    update_field($field['page_administrators'], sanitize_text_field($_POST['page_administrators']), $post_id);
    //workers
    update_field($field['workers'], $data['workers'], $post_id);
    //other information
    update_field($field['add_info'], $data['add_info'], $post_id);
    //geo position
    update_field($field['geo_position'], sanitize_text_field($_POST['geo_position']), $post_id);

    wp_send_json($response);
}

add_action('wp_ajax_update_enterprise', 'update_enterprise');

/**
 * create/update enterprise
 */
function updateBookmark()
{
    global $bookmarks, $user_id;

    if ($bookmarks) {
        if ($_POST['event'] == 'remove_bookmark') {
            $position = array_search($_POST['ID'], $bookmarks);
            unset($bookmarks[$position]);
        } else {
            $bookmarks[] = $_POST['ID'];
        }
        //update value in DB
        $str = implode(',', $bookmarks);
        update_field('field_5bd32bb6bfaa9', $str, 'user_' . $user_id);
    } else {
        $bookmarks = $_POST['ID'];
        update_field('field_5bd32bb6bfaa9', $bookmarks, 'user_' . $user_id);
    }

    if ($_POST['event'] == 'remove_bookmark') {
        $response['remove']     = 'Даний запис успішно видалено із закладок.';
    } else {
        $response['add']        = 'Даний запис успішно додано до закладок.';
    }

    wp_send_json($response);
}

add_action('wp_ajax_update_bookmark', 'updateBookmark');

/**
 * filter
 */
function filter()
{
    global $bookmarks, $user_id, $post;

    $response = array();

    $args  = array(
        'post_type'         => $_POST['post_type'],
        'posts_per_page'    => $_POST['map'][0] == 1 ? -1 : 10,
        'order'             => 'DESC',
        'post_status'       => 'publish',
        'paged'             => $_POST['map'][0] == 1 ? 1 : $_POST['page'],
        'tax_query'         => array(
            'relation'      => 'AND'
        )
    );

    //search
    if ($_POST['search']) {
        $args['s'] = $_POST['search'];
    }
    //page
    if ($_POST['page']) {
        $args['paged'] = $_POST['page'];
    }

    //template
    if (count($_POST['post_type']) >= 2) {
        $template = 'post-short';
    } else if (in_array('enterprises', $_POST['post_type']) || in_array('resources', $_POST['post_type'])) {
        $template = 'post-enterprises';
    } else if ($_POST['post_type'][0] == 'online_cons') {
        $template = 'question-short';
    } else {
        $template = 'post-short';
    }

    if ($_POST['type_knowledge_base'] || $_POST['socbusinua'] || $_POST['type_resourse']) {
        $args['meta_query'] = array(
            'relation' => 'OR'
        );


        if ($_POST['type_knowledge_base']) {
            $args['meta_query']['type_knowledge_base'] = array(
                'key'           => 'type',
                'value'         => $_POST['type_knowledge_base'],
                'compare'       => 'IN'
            );
        }

        if ($_POST['type_resourse']) {
            $args['meta_query']['type_resourse'] = array(
                'key'           => 'type',
                'value'         => $_POST['type_resourse'],
                'compare'       => 'IN'
            );
        }

        if ($_POST['type_event_ids']) {
            $args['meta_query']['custom_date'] = array(
                'key'           => 'custom_date',
                'compare'       => 'EXISTS'
            );
        }

        if ($_POST['socbusinua']) {
            $args['meta_query']['socbusinua'] = array(
                'key'           => 'sb_in_ua',
                'value'         => 1,
                'compare'       => '='
            );
        }
    }

    //tax
    foreach ($_POST as $key => $items) {
        $key = str_replace('_ids', '', $key);
        if (
            $key == 'post_type' ||
            $key == 'action' ||
            $key == 'search' ||
            $key == 'page' ||
            $key == 'type_knowledge_base' ||
            $key == 'socbusinua' ||
            $key == 'map' ||
            $key == 'type_resourse'
        ) {
            continue;
        }
        $args['tax_query'][$key] = array(
            'taxonomy' => $key,
            'field'    => 'term_id',
            'terms'    => $items,
            'operator ' => 'IN'
        );
    }

    $query                  = new WP_Query($args);
    $response['pages']      = $query->max_num_pages;
    if ($query->posts) {

        if ($_POST['map'][0] == 1) {
            $response = array(
                'posts' => array()
            );
            foreach ($query->posts as $post) {
                $response['posts'][] = array(
                    'title'     => get_the_title($post->ID),
                    'url'       => get_the_permalink($post->ID),
                    'geo'       => get_field('geo_position', $post->ID),
                    'info'      => get_field('contacts', $post->ID)
                );
            }
        } else {
            ob_start();
            foreach ($query->posts as $post) {
                get_template_part('partials/post/' . $template);
            }
            $response = array(
                'pages'     => $query->max_num_pages,
                'data'      => ob_get_clean(),
                'post_count' => $query->post_count,
                'query'     => $query->posts,
                'args'     => $args
            );
        }
    } else {
        ob_start();
        get_template_part('partials/no-result');
        $response['no_result'] = ob_get_clean();
        $response['args'] = $args;
    }



    wp_send_json($response);
}

add_action('wp_ajax_filter', 'filter');
add_action('wp_ajax_nopriv_filter', 'filter');



/**
 * update profile info
 */
function profile_info_update()
{

    $user_id                = 'user_' . $_POST['user_id'];
    $fistName               = sanitize_text_field($_POST['first_name']);
    $last_name              = sanitize_text_field($_POST['last_name']);

    $field = array(
        'avatar'            => 'field_5bbb531d3c537',
        'hide_email'        => 'field_5bbc92f1d722a',
        'birthday'          => 'field_5bba3637d8443',
        'contacts'          => 'field_5bba3661d8444',
        'education'         => 'field_5bba390ec1fd1',
        'career'            => 'field_5bba3972c1fd5',
        'social_network'    => 'field_5bbdcee4df170',
        'description'       => 'field_5bba39c0c1fd8'
    );

    $data = array(
        'contacts'          => array(
            'country'       => sanitize_text_field($_POST['contact_country']),
            'city'          => sanitize_text_field($_POST['contact_city']),
            'phone'         => sanitize_text_field($_POST['contact_phone']),
            'hide_phone'    => $_POST['hide_phone'] != 1 ? 0 : 1,
            'website'       => sanitize_text_field($_POST['contact_website']),
            'hide_website'  => $_POST['hide_website'] != 1 ? 0 : 1,
            'hide_email'    => $_POST['hide_email'] != 1 ? 0 : 1,
        ),
        'education'         => array(
            'university'    => sanitize_text_field($_POST['university']),
            'faculty'       => sanitize_text_field($_POST['faculty']),
            'study_div'     => sanitize_text_field($_POST['study_div'])
        ),
        'career'            => array(
            'work_place'    => sanitize_text_field($_POST['work_place']),
            'position'      => sanitize_text_field($_POST['position'])
        ),
        'social_network'    => array(
            'facebook'      => sanitize_text_field($_POST['facebook']),
            'instagram'     => sanitize_text_field($_POST['instagram']),
            'twitter'       => sanitize_text_field($_POST['twitter']),
            'linkedin'      => sanitize_text_field($_POST['linkedin']),
            'youtube'       => sanitize_text_field($_POST['youtube']),
        )
    );

    //avatar
    //update_field('key_1234567890123', $attachment_id, $post_id);

    //update first/last name
    update_user_meta($_POST['user_id'], 'first_name', $fistName);
    update_user_meta($_POST['user_id'], 'last_name', $last_name);
    update_user_meta($_POST['user_id'], 'display_name', $fistName . ' ' . $last_name);

    //avatar
    update_field($field['avatar'], sanitize_text_field($_POST['id_photo']), $user_id);

    //birthdat
    update_field($field['birthday'], sanitize_text_field($_POST['day_birth']), $user_id);
    //contacts
    update_field($field['contacts'], $data['contacts'], $user_id);
    //education
    update_field($field['education'], $data['education'], $user_id);
    //career
    update_field($field['career'], $data['career'], $user_id);
    //social network
    update_field($field['social_network'], $data['social_network'], $user_id);

    //descrition
    update_field($field['description'], sanitize_text_field($_POST['description']), $user_id);

    $result = $data;

    wp_send_json($result);
}

add_action('wp_ajax_update_user_info', 'profile_info_update');


/******************************************************************************************************************************
 * user login/register
 */

/**
 * Login user
 */

function loginUser()
{

    $auth = wp_authenticate(sanitize_text_field($_POST['login']), sanitize_text_field($_POST['password']));

    if (is_wp_error($auth)) {

        $result['error'] = $auth->get_error_message();
        wp_send_json($result);
    } else {
        nocache_headers();
        wp_clear_auth_cookie();
        wp_set_auth_cookie($auth->ID);
        $result['done'] = true;
    }

    wp_send_json($result);
}

add_action('wp_ajax_loginUser', 'loginUser');
add_action('wp_ajax_nopriv_loginUser', 'loginUser');

/**
 * Register new user
 */
function registerUser()
{

    if (!($_POST['first_name'] && $_POST['last_name'] && $_POST['email'] && $_POST['password'] && $_POST['repeat_password'])) {
        $result['error'] = 'fields';
        wp_send_json($result);
    }

    if ($_POST['password'] != $_POST['repeat_password']) {
        $result['error'] = $user_id->get_error_message();
        wp_send_json($result);
    }

    $first_name = sanitize_text_field($_POST['first_name']);
    $last_name  = sanitize_text_field($_POST['last_name']);
    $email      = sanitize_text_field($_POST['email']);


    $userdata = array(
        'user_pass'       => sanitize_text_field($_POST['password']),
        'user_login'      => $email,
        'user_email'      => $email,
        'description'     => '',
        'display_name'    => $first_name + ' ' + $last_name,
        'nickname'        => $email,
        'first_name'      => $first_name,
        'last_name'       => $last_name,
        'role'            => 'user_vis',
    );

    $result['data'] = $userdata;

    $user_id = wp_insert_user($userdata);

    if (is_wp_error($user_id)) {
        $result['error'] = $user_id->get_error_message();
        wp_send_json($result);
    } else {
        $result['complete'] = $user_id;
    }

    wp_clear_auth_cookie();
    wp_set_auth_cookie($user_id);

    wp_send_json($result);
}

add_action('wp_ajax_register_user', 'registerUser');
add_action('wp_ajax_nopriv_register_user', 'registerUser');

/**
 * logout user
 */
function logOutUser()
{

    wp_logout();
}

add_action('wp_ajax_logout', 'logOutUser');

/**
 * logout user
 */
function replacePassword()
{
    global $current_user;

    if (wp_check_password($_POST['current_password'], $current_user->data->user_pass)) {
        $response['done'] = true;
        wp_set_password($_POST['new_password'], $current_user->ID);
    } else {
        $response['error'] = $user_id->get_error_message();
    }

    wp_send_json($response);
}

add_action('wp_ajax_change_password', 'replacePassword');

/*
 *
 * reset password
 *
 *  */

function checkUser()
{

    $user = get_user_by('email', $_POST['email']);

    if ($user) {
        $result['complete'] = true;
        $result['user'] = true;
        $result['login'] = $user->user_login;
        $key = get_password_reset_key($user);
        //$result['key'] = $key;

        $subject = "Відновлення пароля";


        $message = '<p>Привіт ' . $user->first_name . ',<br/>'
            . 'Ви здійснили запит для відновлення пароля. <br/>'
            . 'Ключ: ' . $key . '<br/>'
            . 'Посилання дня відновлення: ' . $_SERVER['HTTP_HOST'] . '?key_reset=' . $key . '&login=' . $user->user_login . '<br/>'
            . 'Дане посилання і ключ працюватимуть тільки 24 години.<br/>'
            . 'Дякую</p>';

        $mailto = $_POST['email'];
        //$mailto = 'bodya.flesh@gmail.com';

        $headers  = 'From: ' . get_bloginfo('name') . ' <no-reply@' . $_SERVER['HTTP_HOST'] . '>' . "\r\n" .
            'Reply-To: ' . get_bloginfo('name') . ' <no-reply@' . $_SERVER['HTTP_HOST'] . '>' . "\r\n" . "Content-type: text/html; charset=utf-8 \r\n";


        if (wp_mail($mailto, $subject, $message, $headers)) {
            $result['email_status'] = true;
        }
    } else {
        $result['user'] = false;
    }

    wp_send_json($result);
}

add_action('wp_ajax_reset_pass', 'checkUser');
add_action('wp_ajax_nopriv_reset_pass', 'checkUser');

/**
 * check new key to reset password
 */
function checkKey()
{

    $user_login = $_POST['login'];
    $key = $_POST['key'];

    $is_ok = check_password_reset_key($key, $user_login);

    if (is_wp_error($is_ok)) {
        $result['error'] = $is_ok->get_error_message();
    } else {
        $result['complete'] = true;
        $user = get_user_by('login', $user_login);
        $result['key'] = true;
        $result['user_id'] = $user->ID;
    }

    wp_send_json($result);
}

add_action('wp_ajax_reset_pass_key', 'checkKey');
add_action('wp_ajax_nopriv_reset_pass_key', 'checkKey');


/**
 * set new password
 */
function setNewPassword()
{

    wp_set_password($_POST['password'], (int) $_POST['user_id']);

    $result['password'] = $_POST['password'];
    $result['user_id'] = (int) $_POST['user_id'];
    $result['newPass'] = true;

    wp_send_json($result);
}

add_action('wp_ajax_reset_pass_new', 'setNewPassword');
add_action('wp_ajax_nopriv_reset_pass_new', 'setNewPassword');
