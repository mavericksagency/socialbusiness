<?php

//remove dots excerpt
function new_excerpt_more( $more ) {
    return '...';
}
add_filter('excerpt_more', 'new_excerpt_more');


// pagination
function pagination($pages = '', $range = 3){  
    $showitems = ($range * 2)+1;  

    global $paged;
    if(empty($paged)){ $paged = 1;}

    if($pages == ''){
       global $wp_query;
       $pages = $wp_query->max_num_pages;
       if(!$pages){
           $pages = 1;
       }
   }   

   if(1 != $pages){
        echo "<ul class=\"numbers\">";
        for ($i=1; $i <= $pages; $i++){
            if (1 != $pages &&( !($i >= $paged+$range+1 || $i <= $paged-$range-1) || $pages <= $showitems )){
               echo ($paged == $i)? "<li><span>".$i."</span></li>":"<li><a href='".get_pagenum_link($i)."' class=\"page-numbers\">".$i."</a></li>";
            }
        }  
        echo "</ul>";
        $next_prev = '<ul class="right-paginator">';
        if($paged != $pages){
            $next_prev .= '<li><a href="'.get_pagenum_link($paged + 1).'">Next</a></li>';
        }
        if($paged != 1){
            $next_prev .= '<li><a href="'.get_pagenum_link($paged - 1).'">Previous</a></li>';
        }
        $next_prev .= '</ul>';
        echo $next_prev;
   }
}


//remove sumbols
function cleanPhone($phone){
    $vowels = array(" ", "-", ")", "(");
    return str_replace($vowels, "", $phone);
}

//remove 
function cleanWebSiteUrl($website){
    $vowels = array("https://", "http://");
    return str_replace($vowels, "", $website);
}

//get variation author 
function getOptionsAuthor($user_id){
    $args       = array(
        'post_type'         => array('resources','enterprises'),
        'posts_per_page'    => -1,
        'meta_query'        => array(
            'author'        => array(
                'key'       => 'page_administrators',
                'value'     => 'user_'.$user_id,
                'compare'   => 'LIKE',
            ),
        )
    );

    $result     = new WP_Query($args);

    return $result->posts; 
} 


//get all terms
function get_all_terms($post_id){
    $arga       = array(

    );
    $terms = wp_get_post_terms(
        $post_id,
        array('geo', 'service', 'topics', 'type_event', 'language', 'type_enterprises', 'beneficiaries')
    );

    return $terms;
}

function show_all_terms($post_id){
    global $type;
    $terms = get_all_terms($post_id);
    foreach($terms as $term){
        if($type == $term->name){
            continue;
        }
        echo '<a href="'.get_term_link( (int)$term->term_id, $term->taxonomy ).'"  class="tag '.$term->taxonomy.'">'.$term->name.'</a>';
    }
}

function get_terms_ids($post_id){
    $terms_ids  = array();
    $terms      = get_all_terms($post_id);
    foreach($terms as $term){
        $terms_ids[] = $term->term_id;
    }
    return $terms_ids;
}

function adminCheck($post_id){
    global $user_id;

    

    $str = get_field('public', $post_id);
    $arr = explode('_', $str);
    if($arr[0] == 'user'){
        if($arr[1] == $user_id){
            return true;
        }
    }else{
        $post_pars_id = $arr[1];
        $str_admins = get_field('page_administrators', $post_pars_id);
        
        if($str_admins){
            $admins_arr = explode(',', $str_admins);
            if(in_array('user_'.$user_id, $admins_arr)){
                return true;
            }else{
                return false;
            }
        }else{
            return false;
        }
    }
}

//has lector post
function has_lector(){
    global $user_id;

    $args = array(
        'post_type'     => 'resources',
        'post_per_page' => 1,
        'post_status'   => 'any',
        'meta_query'    => array(
            'relation'  => 'AND',
            array(
                'key'   => 'type',
                'value' => 'lector'
            ),
            array(
                'key'       => 'page_administrators',
                'value'     => 'user_'.$user_id,
                'compare'   => 'LIKE'
            )
        )
    );

    $wp_query   = new WP_Query($args);

    return !$wp_query->have_posts();
}

//get correct name type

function correntTypeName($type){

    switch ($type) {
        case 'enterprises':
            return 'підприємство';
        case 'event':
            return 'подію';
        case 'knowledge_base':
            return 'базу знань';
        case 'resources':
            return 'ресурс';
        case 'opportunities':
            return 'вакансію';
        case 'news':
            return 'новину';
    }
}

//get url edit ent/resourse
function editUrl($type, $status){
    global $post;

    if($status == 'draft'){
        if($type == 'resources'){
            return get_the_permalink(53).'?'.get_field('type', $post->ID).'&post_id='.$post->ID;
        }else if($type == 'enterprises'){
            return get_the_permalink(53).'?enterprises&post_id='.$post->ID;
        }      
    }else{
        return get_the_permalink($post->ID);
    }
}

//get current name type

function getTypeName($type){
    switch ($type) {
        case 'lector':
            return ' лектора';
        case 'universities':
            return ' ВНЗ';
        case 'support':
            return ' організацію яка підтримує соціальний бізнес';
        case 'educ':
            return ' освітній майданчик';
        case 'funds':
            return ' фонд';
    }
}

//get current name type

function getTypeNamePosts($type){
    switch ($type) {
        case 'news':
            return 'Новини';
        case 'event':
            return 'Події';
        case 'knowledge_base':
            return 'База знань';
        case 'opportunities':
            return 'Вакансії';
        default:
            return 'Всі публікації';
    }
}

//get type name post
function getNameTypePost($post_id, $post_type){
    if($post_type == 'news'){
        return 'Новина';
    }else if($post_type == 'opportunities'){
        return 'Вакансія';
    }else if($post_type == 'event'){
        $types = get_the_terms($post_id, 'type_event');
        return $types[0]->name;
    }else if($post_type == 'knowledge_base'){
        $type     =   get_field('type', $post_id);
        $category = get_field_object('field_5bc5dad95b031');
        return $category['choices'][$type];
    }else if($post_type == 'resources'){
        $type     =     get_field('type', $post_id);
        $category =     get_field_object('field_5b74070a20a68');
        return $category['choices'][$type];
    }else{
        return false;
    }
}

function getIconPostType($post_id, $typeTypePost){
    if($typeTypePost == 'knowledge_base'){
        $type     =   get_field('type', $post_id);
        if($type){
            switch($type){
                case 'international_cases':
                    return 'world-kb';
                case 'сases_user':
                    return 'user-kb';
                case 'books':
                    return 'book-kb';
                case 'articles':
                    return 'article-kb';
                case 'benefits':
                    return 'studybook-kb';
                case 'motivational_videos':
                    return 'video-kb';
                case 'courses':
                    return 'course-kb';
                case 'motivational_materials':
                    return 'motivation-kb';
                default:
                    return 'book';
            }
        }else{
            return $type;
        }
    }else{
        return $typeTypePost;
    }
}


//get name service
function getTermsName($terms){
    $termsName = array();
    foreach($terms as $term){
        $termsName[] = $term->name;
    }
    return implode(', ', $termsName);
}

//get type enterprice and resourse
function get_type_resourse($post){
    if($post->post_type == 'enterprises'){
        return 'Підприємство';
    }else if($post->post_type == 'resources'){
        $type = get_field('type', $post->ID);
        switch($type){
            case 'lector':
                return 'Лектор';
            case 'universities':
                return 'ВНЗ';
            case 'support':
                return 'Орг, які підтримують розвиток СП';
            case 'educ':
                return 'Освітні майданчики';
            case 'funds':
                return 'Фонди, фінансові установи';
        }
    }
}


//clean function author
function getCleanAuthor($post){
    $admins     = get_field('page_administrators', $post->ID);
    $admins     = explode(',', $admins);
    $authors    = array();
    foreach($admins as $author_item){
        if($author_item){
            $authors[] = $author_item;
        }
    }
    return $authors;
}

function getTypeTaxName($termName){
    switch($termName){
        case 'geo':
            return 'розташуванням';
        case 'service':
            return 'сферою';
        case 'topics':
            return 'тематикою';
        case 'language':
            return 'мовою';
        case 'type_event':
            return 'типом події';
        case 'type_enterprises':
            return 'типом підприємства';
        case 'beneficiaries':
            return 'бенефіціалом';
    }
}

/**
 * get author name (specific function)
 */

function getAuthorAdminName($el_id){
    $user_admin_id = explode('_',$el_id)[1];
    $userPost = get_user_by('ID', $user_admin_id);
    return get_the_author_meta('first_name', $userPost->ID). ' '. get_the_author_meta('last_name', $userPost->ID);
}


function getMonthSlug($num){
    switch($num){
        case 1:
            return 'січ';
        case 2:
            return 'лют';
        case 3:
            return 'бер';
        case 4:
            return 'кві';
        case 5:
            return 'тра';
        case 6:
            return 'чер';
        case 7:
            return 'лип';
        case 8:
            return 'сер';
        case 9:
            return 'вер';
        case 10:
            return 'жов';
        case 11:
            return 'лис';
        case 12:
            return 'гру';
    }
}
