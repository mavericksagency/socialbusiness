<?php

function sb_theme_styles_and_scripts(){
    
    //style
    wp_register_style('normalize', get_template_directory_uri() . '/css/normalize.css');
    wp_register_style('slick', get_template_directory_uri() . '/css/slick.css');
    wp_register_style('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css');
    wp_register_style('styles', get_template_directory_uri() . '/css/styles.css', array(), '6.1.1');
    
    wp_register_style('my-style', get_template_directory_uri() . '/css/my-style.css');


    wp_enqueue_style('normalize');
    wp_enqueue_style('slick');
    wp_enqueue_style('jquery-ui');
    wp_enqueue_style('styles');
    wp_enqueue_style('my-style');

    //scripts

    wp_deregister_script('jquery');

    wp_register_script('jquery', "https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js", array(), '3.3.1' );
    wp_register_script('slick', get_template_directory_uri().'/js/slick.min.js', array('jquery'), '1.0', true);
    wp_register_script('animation', get_template_directory_uri().'/js/animation.js', array('slick'), '1.0', true);
    wp_register_script('main', get_template_directory_uri().'/js/main.js', array('slick'), '1.0', true);
    wp_register_script('datapicker-urk', get_template_directory_uri().'/js/datapicker-urk.js', array('jquery-ui'), '1.0', true);
    wp_register_script('jquery-ui', 'https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js', array('slick'), '1.0', true);
    wp_register_script('markerclusterer', get_template_directory_uri().'/js/markerclusterer.js', array('jquery-ui'), '1.0', true);
    wp_register_script('map', 'https://maps.googleapis.com/maps/api/js?key=AIzaSyBdvO5Ru763vd61h4WctTsQn1IVEb-JbGE&libraries=places', array('slick'), '1.0', true);
    wp_register_script('script', get_template_directory_uri().'/js/script.js', array('map'), '1.0', true);

    
    //active scripts
    wp_enqueue_script('jquery');
    wp_enqueue_script('slick');
    wp_enqueue_script('animation');
    wp_enqueue_script('main');
    wp_enqueue_script('map');
    wp_enqueue_script('markerclusterer');
    wp_enqueue_script('datapicker-urk');
    wp_enqueue_script('jquery-ui');
    wp_enqueue_script('script');

	wp_localize_script('main', 'api_settings', array(
		'root'          => esc_url_raw( rest_url()),
        'nonce'         => wp_create_nonce('wp_rest'),
        'ajax_url'      => site_url().'/wp-admin/admin-ajax.php',
        'template'      => get_bloginfo('template_url').'/',
        'user_id'       => get_current_user_id()
    ));
    
    wp_enqueue_media();
}