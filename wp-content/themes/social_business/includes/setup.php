<?php 

function sb_setup_theme(){
    //add thumbnails
    add_theme_support('post-thumbnails');
    add_theme_support('title-tag');

    register_nav_menu('primary', 'Головне меню', 'sb');
    register_nav_menu('left-side', 'Бокове меню', 'sb');
    register_nav_menu('sub-left-menu', 'Додаткове меню', 'sb');
    register_nav_menu('footer-menu', 'Меню у футері', 'sb');

    //remove item in amin bar
    function post_remove (){ 
        remove_menu_page('edit.php');
    }
    add_action('admin_menu', 'post_remove');

    //register option page
    if( function_exists('acf_add_options_page') ) {
        acf_add_options_page(array(
            'page_title' 	=> 'Налаштування теми',
            'menu_title'	=> 'Налаштування теми',
            'menu_slug' 	=> 'theme-general-settings',
            'capability'	=> 'edit_posts',
            'redirect'      => false
        ));
    }

    //registerm image size
    if ( function_exists( 'add_image_size' ) ) {
        add_image_size('news-image', 600, 400, true);
        add_image_size('logo', 300, 300, true);
    }
}


//blocked admin panel
function no_admin_init() {      
    global $required_capability, $redirect_to, $current_user;      
    // Is this the admin interface?
    if (
        // Look for the presence of /wp-admin/ in the url
        stripos($_SERVER['REQUEST_URI'],'/wp-admin/') !== false
        &&
        // Allow calls to async-upload.php
        stripos($_SERVER['REQUEST_URI'],'async-upload.php') == false
        &&
        // Allow calls to admin-ajax.php
        stripos($_SERVER['REQUEST_URI'],'admin-ajax.php') == false
    ) {         
        // Does the current user fail the required capability level?
        if ($current_user->roles[0] != 'administrator' && $current_user->roles[0] != 'editor'){        
            $redirect_to = get_option('home');              
            // Send a temporary redirect
            wp_redirect($redirect_to,302);              
        }           
    }       
}
// Add the action with maximum priority
add_action('init','no_admin_init', 21);


// add_action('init','register_query_params');
// function register_query_params() { 
//     global $wp; 
//     $wp->add_query_var('topics'); 
//     $wp->add_query_var('type_event'); 
//     $wp->add_query_var('service'); 
//     $wp->add_query_var('beneficiaries'); 
//     $wp->add_query_var('type_enterprises'); 
//     $wp->add_query_var('geo'); 
//     $wp->add_query_var('map'); 
//     $wp->add_query_var('type'); 
// }

add_action( 'admin_menu', 'add_user_menu_bubble' );
function add_user_menu_bubble() {
    global $menu;

    //enterprises
    $args = array(
        'post_type'     => 'enterprises',
        'post_status'   => 'draft',
        'posts_per_page'=> 1
    );

    $query  = new WP_Query($args);

    $postsCount['enterprises'] = $query->found_posts;

    //event
    $args['post_type']  = 'event';
    $query  = new WP_Query($args);
    $postsCount['event'] = $query->found_posts;

    //opportunities
    $args['post_type']  = 'opportunities';
    $query  = new WP_Query($args);
    $postsCount['opportunities'] = $query->found_posts;

    //news
    $args['post_type']  = 'news';
    $query  = new WP_Query($args);
    $postsCount['news'] = $query->found_posts;

    //knowledge_base
    $args['post_type']  = 'knowledge_base';
    $query  = new WP_Query($args);
    $postsCount['knowledge_base'] = $query->found_posts;

    //resources
    $args['post_type']  = 'resources';
    $query  = new WP_Query($args);
    $postsCount['resources'] = $query->found_posts;

    //online_cons
    $args['post_type']  = 'online_cons';
    $query  = new WP_Query($args);
    $postsCount['online_cons'] = $query->found_posts;

    foreach($menu as $key => $value){

        switch ($menu[$key][2]){

            case 'edit.php?post_type=enterprises':
                if($postsCount['enterprises']){
                    $menu[$key][0] .= '<span class="update-plugins"><span class="plugin-count">'.$postsCount['enterprises'].'</span></span>';
                }
                break;

            case 'edit.php?post_type=event':
                if($postsCount['event']){
                    $menu[$key][0] .= '<span class="update-plugins"><span class="plugin-count">'.$postsCount['event'].'</span></span>';
                }
                break;

            case 'edit.php?post_type=opportunities':
                if($postsCount['opportunities']){
                    $menu[$key][0] .= '<span class="update-plugins"><span class="plugin-count">'.$postsCount['opportunities'].'</span></span>';
                }
                break;

            case 'edit.php?post_type=news':
                if($postsCount['news']){
                    $menu[$key][0] .= '<span class="update-plugins"><span class="plugin-count">'.$postsCount['news'].'</span></span>';
                }
                break;

            case 'edit.php?post_type=knowledge_base':
                if($postsCount['knowledge_base']){
                    $menu[$key][0] .= '<span class="update-plugins"><span class="plugin-count">'.$postsCount['knowledge_base'].'</span></span>';
                }
                break;

            case 'edit.php?post_type=resources':
                if($postsCount['resources']){
                    $menu[$key][0] .= '<span class="update-plugins"><span class="plugin-count">'.$postsCount['resources'].'</span></span>';
                }
                break;

            case 'edit.php?post_type=online_cons':
                if($postsCount['online_cons']){
                    $menu[$key][0] .= '<span class="update-plugins"><span class="plugin-count">'.$postsCount['online_cons'].'</span></span>';
                }
                break;
        }
    }
}


// wp_oembed_add_provider('https://youtu.be/*', 'https://youtube.com/oembed');
// wp_oembed_add_provider('#https://(www\.)?youtube.com/watch.*#i', 'https://youtube.com/oembed', true);
// wp_oembed_add_provider('http://youtu.be/*', 'https://youtube.com/oembed');
// wp_oembed_add_provider('#http://(www\.)?youtube.com/watch.*#i', 'https://youtube.com/oembed', true);
// wp_oembed_add_provider('#https://(www\.)?youtube.com/watch.*#i', 'http://youtube.com/oembed?scheme=https', true);
// wp_oembed_add_provider('https://youtu.be/*', 'http://youtube.com/oembed?scheme=https', false);


function convertYoutube($string) {
    return preg_replace(
        "/\s*[a-zA-Z\/\/:\.]*youtu(be.com\/watch\?v=|.be\/)([a-zA-Z0-9\-_]+)([a-zA-Z0-9\/\*\-\_\?\&\;\%\=\.]*)/i",
        "<iframe width=\"560\" height=\"315\" src=\"//www.youtube.com/embed/$2\" frameborder=\"0\" allow=\"accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture\" allowfullscreen></iframe>",
        $string
    );
}
