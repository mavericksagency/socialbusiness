<?php
$headers  = 'From: '.get_bloginfo('name').' <no-reply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n" .
            'Reply-To: '.get_bloginfo('name').' <no-reply@'.$_SERVER['HTTP_HOST'].'>' . "\r\n" ."Content-type: text/html; charset=utf-8 \r\n";


//hook send email admin

// function create_send_email($post_id, $post, $update) {
//     global $headers;

//     if($update || !$post_id){
//         return;
//     }

// 	$subject = 'Створено новий запис "'. $post->post_title.'"';

// 	$message = '<p><a href="'.$_SERVER['HTTP_HOST'].'/wp-admin/post.php?post='.$post_id.'&action=edit">Редактувати запис</a></p>';

//     $mailto = get_field('email', 'option');

//     wp_mail($mailto, $subject, $message, $headers);
// }
// add_action('wp_insert_post', 'create_send_email', 50, 3);            


function createNewQuestion($post_id){
    global $headers;

    $subject = "На сайті було задане нове питання.";

    $message = '<p>Було творено нове питання "'.get_the_title($post_id).'"</p>';

    $mailto = get_field('email', 'option');

    wp_mail($mailto, $subject, $message, $headers);
}

function notificationConNewQuestion($email, $post_id){
    global $headers;

    $subject = 'Було створено нове питання "'.get_the_title($post_id).'"';


    $message = '<p>На сайті було задане нове питання "'.get_the_title($post_id).'". 
    Ви назначені відповідальним за нього. 
    Питання знаходиться за <a href="'.get_the_permalink($post_id).'">адресою</a></p>'.$email; 

    wp_mail($email, $subject, $message, $headers);
}

function notificationtoConsultant($email, $post_id){
    global $headers;

    $subject = "Новий коментарій від користувача.";

    $message = '<p>Новий коментарій на питання <a href="'.get_the_permalink($post_id).'">'.get_the_title($post_id).'"</a></p>';

    $mailto = $email;

    wp_mail($mailto, $subject, $message, $headers);
}

function notificationtoUser($email, $post_id){
    global $headers;
    
    $subject = "Консультан відповів на ваше питання.";

    $message = '<p>Новий коментарій на питання <a href="'.get_the_permalink($post_id).'">'.get_the_title($post_id).'"</a></p>';

    $mailto = $email;

    wp_mail($mailto, $subject, $message, $headers);
}