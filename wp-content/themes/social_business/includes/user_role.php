<?php
remove_role('author');
remove_role('contributor');
remove_role('subscriber');
remove_role('user_r');


// remove_role('editor');
// remove_role('consultant');
// remove_role('user_m');
// remove_role('user_vis');
// remove_role('guest');

add_role('editor', 'Редактор',
	array(
		'moderate_comments'			=> true,
		'manage_categories'			=> true,
		'manage_links'				=> true,
		'edit_others_posts'			=> true,
		'edit_pages'				=> true,
		'edit_others_pages'			=> true,
		'edit_published_pages'		=> true,
		'publish_pages'				=> true,
		'delete_pages'				=> true,
		'delete_others_pages'		=> true,
		'delete_published_pages'	=> true,
		'delete_others_posts'		=> true,
		'delete_private_posts'		=> true,
		'edit_private_posts'		=> true,
		'read_private_posts'		=> true,
		'delete_private_pages'		=> true,
		'edit_private_pages'		=> true,
		'read_private_pages'		=> true,
		'edit_published_posts'		=> true,
		'upload_files'				=> true,
		'publish_posts'				=> true,
		'delete_published_posts'	=> true,
		'edit_posts'				=> true,
		'delete_posts'				=> true,
		'read'						=> true
	)
);
add_role('consultant', 'Консультант',
	array(
		'read'         => true,
		'edit_posts'   => false,
		'delete_posts' => false,
		'upload_files' => true,
	)
);
add_role('user_m', 'Учасник',
	array(
		'read'         => true,
		'edit_posts'   => false,
		'delete_posts' => false,
		'upload_files' => true,
	)
);
add_role('user_vis', 'Відвідувач',
	array(
		'read'         => true,
		'edit_posts'   => false,
		'delete_posts' => false,
		'upload_files' => true,
	)
);
add_role('guest', 'Гість',
	array(
		'read'         => true,
		'edit_posts'   => false,
		'delete_posts' => false,
		'upload_files' => true,
	)
);