<?php 
	global $user_id;
	if($user_id){
?>
<div class="right-column settings-column selection-settings anchor-sections-column">
	<div class="block selection-menu anchors-menu section-block">
		<h2 class="section-title selection-title">
			Розділи
		</h2>
		<div class="selection-units">
			<ul class="section-list">
				<li>
					<a class="section-link" href="<?= get_the_permalink(486); ?>">
						<span class="section-text">Мої закладки</span>
					</a>	
				<li>
				<li>
					<a class="section-link" href="<?= get_the_permalink(546); ?>?post=enterprises">
						<span class="section-text">Мої підприємства</span>
					</a>	
				<li>
				<li>
					<a class="section-link" href="<?= get_the_permalink(546); ?>?post=event">
						<span class="section-text">Мої події</span>
					</a>	
				</li>
				<li>
					<a class="section-link" href="<?= get_the_permalink(546); ?>?post=knowledge_base">
						<span class="section-text">Моя база знань</span>
					</a>	
				</li>
				<li>
					<a class="section-link" href="<?= get_the_permalink(546); ?>?post=resources">
						<span class="section-text">Мої ресурси</span>
					</a>	
				</li>
				<li>
					<a class="section-link" href="<?= get_the_permalink(546); ?>?post=opportunities">
						<span class="section-text">Мої вакансії</span>
					</a>	
				</li>
				<li>
					<a class="section-link" href="<?= get_the_permalink(546); ?>?post=news">
						<span class="section-text">Мої новини</span>
					</a>	
				</li>
				<li>
					<a class="section-link" href="/online_cons/?my_question">
						<span class="section-text">Мої питання</span>
					</a>	
				</li>
			</ul>
		</div>
	</div>		
</div>
<?php } ?>