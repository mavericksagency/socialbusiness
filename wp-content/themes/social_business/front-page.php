<?php 
    get_header();
?>
<main class="index-main">
	<?php
		$sec_1		= get_field('sec_1');
	?>
	<section class="index-block index-flex white-block main-block">
		<div class="main-wrap">
			<div class="index-content-item">
				<span class="index-icon">
					<svg class="icon">
						<use xlink:href="#index-icon">
					</svg>
				</span>
				<h1 class="index-title">
					Соціальне підприємництво в Україні
				</h1>
				<span class="index-description">
					<?= $sec_1['text']; ?>
				</span>
				<a href="<?= $sec_1['button_url']; ?>" class="btn-raised">
					<?= $sec_1['button_text']; ?>
				</a>
			</div>
			<div class="main-svg-block">

				<img class="image hidden" src="<?php bloginfo('template_directory'); ?>/img/main.jpg">

				<svg class="main-svg mask-svg hidden" width="100%" height="100%">
					<defs>
						<mask id="mask">
							<rect width="100%" height="100%" fill="#fff"/>
							<svg class="svg" id="svg" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Слой_1" x="0px" y="0px" viewBox="0 0 1926.5 1898" style="enable-background:new 0 0 1926.5 1898;" xml:space="preserve">
								<g>
									<g>
										
											<circle class="st0 st st-lg" cx="1664.2" cy="1040.9" r="119.2"/>
										
											<circle class="st0 st st-lg st00" cx="1545.7" cy="605.9" r="133.4"/>
										
											<circle class="st0 st st-lg" cx="1427" cy="1425.6" r="170.6"/>
										
											<circle class="st0 st st-lg" cx="946.1" cy="1606.8" r="170.6"/>
										
											<circle class="st0 st st-sm" cx="1474.3" cy="333.3" r="34.9"/>
								
											<circle class="st0 st st-sm" cx="1754.8" cy="823.5" r="36.1"/>
										
											<circle class="st0 st st-sm" cx="1517.1" cy="829.7" r="37.7"/>
										
											<circle class="st0 st st-md" cx="1307.2" cy="781.7" r="45.9"/>
										
											<circle class="st0 st st-md" cx="1305.6" cy="567" r="51.1"/>
										
											<circle class="st0 st st-md" cx="1699.2" cy="1275.8" r="49.5"/>
										
											<circle class="st0 st st-sm" cx="1445.3" cy="1139.2" r="30.2"/>
										
											<circle class="st0 st st-md" cx="1398.2" cy="949.3" r="63.2"/>
										
											<circle class="st0 st st-md" cx="1254.5" cy="1698.8" r="49.5"/>
										
											<circle class="st0 st st-md" cx="1179.1" cy="1225.3" r="63.2"/>
										
											<circle class="st0 st st-sm" cx="1099.2" cy="1381.4" r="24.7"/>
										
											<circle class="st0 st st-md" cx="954.2" cy="1275" r="49.5"/>
										
											<circle class="st0 st st-md" cx="643.6" cy="1697.7" r="51.8"/>

										<path class="st0 st-path" d="M681.7,800.8c-88.1,157.2-32.1,356,125,444.1l-258.7,461.4C137.8,1476.3-8.3,957.4,221.7,547.2    s749-556.3,1159.1-326.4l-255.1,455C968.7,587.7,769.9,643.6,681.7,800.8z"/>
									</g>
								</g>
							</svg>
						</mask>
					</defs>
					<rect class="rect" width="200%" height="200%" fill="#fff" fill-opacity="1" mask="url(#mask)" />
				</svg>

				<svg class="main-svg color-svg hidden" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" version="1.1" id="Слой_1" x="0px" y="0px" viewBox="0 0 1926.5 1898" style="enable-background:new 0 0 1926.5 1898;" xml:space="preserve">
					<g>
						<g>
							
								<circle class="st0 st st-lg" cx="1664.2" cy="1040.9" r="119.2"/>
							
								<circle class="st0 st st-lg st00" cx="1545.7" cy="605.9" r="133.4"/>
							
								<circle class="st0 st st-lg" cx="1427" cy="1425.6" r="170.6"/>
							
								<circle class="st0 st st-lg" cx="946.1" cy="1606.8" r="170.6"/>
							
								<circle class="st0 st st-sm" cx="1474.3" cy="333.3" r="34.9"/>
					
								<circle class="st0 st st-sm" cx="1754.8" cy="823.5" r="36.1"/>
							
								<circle class="st0 st st-sm" cx="1517.1" cy="829.7" r="37.7"/>
							
								<circle class="st0 st st-md" cx="1307.2" cy="781.7" r="45.9"/>
							
								<circle class="st0 st st-md" cx="1305.6" cy="567" r="51.1"/>
							
								<circle class="st0 st st-md" cx="1699.2" cy="1275.8" r="49.5"/>
							
								<circle class="st0 st st-sm" cx="1445.3" cy="1139.2" r="30.2"/>
							
								<circle class="st0 st st-md" cx="1398.2" cy="949.3" r="63.2"/>
							
								<circle class="st0 st st-md" cx="1254.5" cy="1698.8" r="49.5"/>
							
								<circle class="st0 st st-md" cx="1179.1" cy="1225.3" r="63.2"/>
							
								<circle class="st0 st st-sm" cx="1099.2" cy="1381.4" r="24.7"/>
							
								<circle class="st0 st st-md" cx="954.2" cy="1275" r="49.5"/>
							
								<circle class="st0 st st-md" cx="643.6" cy="1697.7" r="51.8"/>

							<path class="st0" d="M681.7,800.8c-88.1,157.2-32.1,356,125,444.1l-258.7,461.4C137.8,1476.3-8.3,957.4,221.7,547.2    s749-556.3,1159.1-326.4l-255.1,455C968.7,587.7,769.9,643.6,681.7,800.8z"/>
						</g>
					</g>
				</svg>
			</div>

			<div class="main-svg-block double hidden">
			</div>
		</div>
	</section>
	<?php
		$sec_2		= get_field('sec_2');
	?>
	<section class="index-block index-flex border-bottom">
		<div class="main-wrap main-wrap-reverse">
			<div class="index-content-item">
				<span class="index-icon">
					<svg class="icon">
						<use xlink:href="#index-icon">
					</svg>
				</span>
				<h2 class="index-title">
					Про проект
				</h2>
				<span class="index-description">
					<?= $sec_2['text']; ?>
				</span>
				<a href="<?= $sec_2['button_url']; ?>" class="btn">
					<?= $sec_2['button_text']; ?>
				</a>
			</div>
			<div class="index-image">
				<img class="img" src="<?php bloginfo('template_directory'); ?>/img/about-project.svg">
			</div>
		</div>
	</section>
	<?php
		$sec_3		= get_field('sec_3');
	?>
	<section class="index-block index-flex">
		<div class="main-wrap">
			<div class="index-content-item">
				<span class="index-icon">
					<svg class="icon">
						<use xlink:href="#index-icon">
					</svg>
				</span>
				<h2 class="index-title">
					Про партнерів
				</h2>
				<span class="index-description">
					<?= $sec_3['text']; ?>
				</span>
				<a href="<?= $sec_3['button_url']; ?>" class="btn">
					<?= $sec_3['button_text']; ?>
				</a>
			</div>
			<div class="index-image">
				<img class="img" src="<?php bloginfo('template_directory'); ?>/img/about-partners.svg">
			</div>
		</div>
	</section>
	<?php
		$sec_4		= get_field('sec_4');
	?>
	<section class="main-enumeration index-block white-block border-bottom">
		<div class="main-wrap">
			<h2 class="index-title">
				Знаходь або створюй
			</h2>
			<ul class="index-types">
				<li class="index-types-item">
					<a href="/enterprises/">
						<div class="index-types-icon">
							<svg class="icon">
								<use xlink:href="#buildings">
							</svg>
						</div>
						<span class="index-types-text">
							Підприємства
						</span>
					</a>
				</li>
				<li class="index-types-item">
					<a href="/online_cons/">
						<div class="index-types-icon">
							<svg class="icon">
								<use xlink:href="#question">
							</svg>
						</div>
						<span class="index-types-text">
							Консультація
						</span>
					</a>
				</li>
				
				<li class="index-types-item">
					<a href="/knowledge_base/">
						<div class="index-types-icon">
							<svg class="icon">
								<use xlink:href="#education">
							</svg>
						</div>
						<span class="index-types-text">
							База знань
						</span>
					</use>
				</li>
				<li class="index-types-item">
					<a href="/resources/">
						<div class="index-types-icon">
							<svg class="icon">
								<use xlink:href="#urban-buildings">
							</svg>
						</div>
						<span class="index-types-text">
							Ресурси
						</span>
					</a>
				</li>
			</ul>
			<a href="<?= $sec_3['button_url']; ?>" class="btn go-more">
				<?= $sec_3['button_text']; ?>
			</a>
		</div>
	</section>
	<?php
		$sec_5		= get_field('sec_5');
	?>
	<section class="index-block index-flex white-block border-bottom">
		<div class="main-wrap main-wrap-reverse">
			<div class="index-content-item">
				<div class="index-title-n-icon">
					<span class="index-icon circle-icon">
						<svg class="icon">
							<use xlink:href="#book">
						</svg>
					</span>
					<h2 class="index-title">
						Навчальні матеріали
					</h2>
				</div>
				<span class="index-description">
					<?= $sec_5['text']; ?>
				</span>
				<a href="<?= $sec_5['button_url']; ?>" class="btn">
					<?= $sec_5['button_text']; ?>
				</a>
			</div>
			<div class="index-image index-panels-images">
				<img class="img" src="<?php bloginfo('template_directory'); ?>/img/img-2.png" alt="">
				<!-- <img class="img" src="<?php bloginfo('template_directory'); ?>/img/panel.png" alt="">
				<img class="img" src="<?php bloginfo('template_directory'); ?>/img/panel.png" alt="">
				<img class="img" src="<?php bloginfo('template_directory'); ?>/img/panel.png" alt=""> -->
			</div>
		</div>
	</section>
	<?php
		$sec_6		= get_field('sec_6');
	?>
	<section class="index-block index-flex white-block border-bottom">
		<div class="main-wrap">
			<div class="index-content-item">
				<div class="index-title-n-icon">
					<span class="index-icon circle-icon">
						<svg class="icon">
							<use xlink:href="#buildings">
						</svg>
					</span>
					<h2 class="index-title">
						Ресурси
					</h2>
				</div>
				<span class="index-description">
					<?= $sec_6['text']; ?>
				</span>
				<a href="<?= $sec_6['button_url']; ?>" class="btn">
					<?= $sec_6['button_text']; ?>
				</a>
			</div>
			<div class="index-image">
				<img class="img" src="<?php bloginfo('template_directory'); ?>/img/img-1.png" alt="">
			</div>
		</div>
	</section>
	<?php
		$sec_7		= get_field('sec_7');
	?>
	<section class="index-block index-flex white-block border-bottom">
		<div class="main-wrap main-wrap-reverse">
			<div class="index-content-item">
				<div class="index-title-n-icon">
					<span class="index-icon circle-icon">
						<svg class="icon">
							<use xlink:href="#question">
						</svg>
					</span>
					<h2 class="index-title">
						Консультація
					</h2>
				</div>
				<span class="index-description">
					<?= $sec_7['text']; ?>
				</span>
				<a href="<?= $sec_7['button_url']; ?>" class="btn">
					<?= $sec_7['button_text']; ?>
				</a>
			</div>
			<div class="index-image">
				<img class="img" src="<?php bloginfo('template_directory'); ?>/img/img-3.png" alt="">
			</div>
		</div>
	</section>
	<section class="main-panels index-block">
		<div class="main-wrap">
			<h2 class="index-title">
				Читай найсвіжіше від SB in UA
			</h2>
			<div class="panels-grid">
				<?php 
					$args = array(
						'post_type'			=> 'news',
						'posts_per_page'	=> 6,
						'order'				=> 'DESC',
						'meta_query' 		=> array(
							array(
								'key' 			=> 'sb_in_ua',
								'value' 		=> 1,
                				'compare'       => '='
							)
						)
					);
					$query	= new WP_Query($args);
					if($query->posts){
						while($query->have_posts()){
							$query->the_post();
							get_template_part('partials/post/post-short');
						}
						wp_reset_postdata();
					}
				?>
			</div>
			<?php if($query->posts){ ?>
			<a href="/news/?socbusinua=1" class="btn go-more">
				Більше
			</a>
			<?php } ?>
		</div>
	</section>
</main>
<?php
    get_footer();
?>