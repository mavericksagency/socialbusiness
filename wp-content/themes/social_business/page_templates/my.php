<?php 
    /**
     * Template Name: Мої ***
     */
    get_header(); 
    global $user_id;
    $type       =   array('enterprises', 'resources');
    if(in_array($_GET['post'], $type)){
        $post_template  = 'post-enterprises';
        $block      = '';
    }else{
        $post_template  = 'post-short';
        $block      = '<div class="panels-grid">';
    }
    global $my_page;
    $my_page = true;
?>
<main class="main">
    <div class="main-grid selection-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <div class="center-column user-selection">
            <?php 
            if($user_id){
                echo $block;
                $args 		= array(
                        'post_type'		    => $_GET['post'],
                        'post_per_page'	    => -1,
                        'order'			    => 'DESC',
                        'post_status'       => array('publish', 'draft'),
                        'meta_query'        => array(
                            'relation'      => 'OR',
                            'public'        => array(
                                'key'       => 'public',
                                'value'     => 'user_'.$user_id,
                                'compare'   => 'LIKE',
                            ),
                            'user'          => array(
                                'key'       => 'page_administrators',
                                'value'     => 'user_'.$user_id.',',
                                'compare'   => 'LIKE',
                            ),
                        ),
                );
                $query		= new WP_Query($args);
                    if($query->have_posts()){
                        while($query->have_posts()){
                            $query->the_post();
                            get_template_part('partials/post/'.$post_template);
                        }
                    }else{
                        if($_GET['post'] == 'resources'){
                            $url = get_the_permalink(632);
                        }else{
                            $url = get_the_permalink(53).'?'.$_GET['post'];
                        }
                        get_template_part('partials/no-result');
                    }
                if($block){
                    echo '</div>';
                }
            }else{
                get_template_part('partials/no-access');
            }
			?>
        </div>
         <?php get_sidebar('cabinet'); ?>
    </div>
</main>
<?php get_footer(); ?>