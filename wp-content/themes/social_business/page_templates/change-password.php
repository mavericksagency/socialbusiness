<?php 
get_header(); 
/**
 * Template Name: Зміна пароля
 */
global $current_user, $user_id;
    ?>
<main class="main">
    <div class="main-grid account-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <?php 
            if($user_id){
                ?>
                <form action="#" method="POST" class="center-column" id="change_password">
                    <div class="account-block shadow-block settings-block" id="main_info">
                        <div class="account-header red">
                            <span class="setting-icon bg-color">
                                <svg class="icon svg-fill">
                                    <use xlink:href="#buildings">
                                </svg>
                            </span>
                            <h2 class="setting-title">
                                Змінюємо пароль
                            </h2>
                        </div>
                        <div class="settings-fields">
                            <input type="hidden" name="hash" value="<?= $current_user->data->user_pass ?>">
                            <input type="hidden" name="user_id" value="<?= $current_user->ID ?>">
                            <div class="field">
                                <label class="input-label">
                                    <span class="input-desc">Поточний пароль</span>
                                    <input type="password" minLength="8" required name="current_password" class="input" placeholder="Введіть пароль">
                                    <span class="valid-notification"></span>
                                </label>
                                <label class="input-label">
                                    <span class="input-desc">Новий пароль</span>
                                    <input type="password" minLength="8" required name="new_password" id="new_password" class="input" placeholder="Введіть пароль">
                                    <span class="valid-notification"></span>
                                </label>
                                <label class="input-label">
                                    <span class="input-desc">Повторити новий пароль</span>
                                    <input type="password" minLength="8" required name="repeat_new_password"  id="repeat_new_password" class="input" placeholder="Введіть пароль">
                                    <span class="valid-notification"></span>
                                </label>
                            </div>
                            <div class="account-footer">
                                <button type="submit" class="btn-raised submit">
                                    <span class="save-text">
                                        Змінити
                                    </span>
                                </button>
                                <span class="saved-text">
                                    Змінено
                                </span>
                            </div>
                            <div>
                                <p class="text">Якщо ви не памятаєте пароль можете його <a href="#" class="pass-forgot">відновити</a></p>
                            </div>
                        </div>
                    </div>
                </form>
                <?php
            }else{
                get_template_part('partials/no-access');
            }
            get_sidebar('cabinet');
        ?>
    </div>
    
</main>
<?php get_footer(); ?>