<?php 
	/**
	 * Template Name: Пошук faq
	 */
    get_header(); 
?>
	<main class="main">
		<div class="main-grid">
			<div class="left-column">
				<?php get_sidebar() ?>
			</div>
			<div class="center-column">
				<?php get_template_part('partials/search-faq'); ?>
				<div class="questions-theme block shadow-block">
					<div class="account-header green">
						<span class="setting-icon bg-color">
							<svg class="icon svg-fill">
								<use xlink:href="#person">
							</svg>
						</span>
						<h1 class="setting-title">
							<?= $_POST['search']; ?>
						</h1>
					</div>
					<div class="questions-theme-body">
                        <?php 
							$args 		= array(
								'post_type'			=> 'faq',
								'posts_per_page'	=> -1,
								's'					=> $_POST['search']
							);
							$query 		= new WP_Query($args);
							if($query->posts){
								foreach($query->posts as $post_faq){
									?>
									<div class="accordion question-accordeon">
										<div class="accordion-header">
											<div class="accordion-title">
												<span class="accordion-title__text">
													<?= $post_faq->post_title; ?>
												</span>
											</div>
											<div class="accordion-control">
												<svg class="icon plus">
													<use xlink:href="#plus">
												</use></svg>
												<svg class="icon minus">
													<use xlink:href="#minus">
												</use></svg>
											</div>
										</div>
										<div class="accordion-inner">
											<?= wpautop($post_faq->post_content) ?>
										</div>
									</div>
									<?php
								}
							}else{
								?>
								<h2>За вашим запитом нічого не знайдено.</h2>
							<?php
							}
						?>
					</div>
				</div>
			</div>
			<div class="right-column settings-column">
				<div class="block anchors-menu section-block filter-settings">
					<h2 class="section-title">
						Розділи
					</h2>
					<button class="btn-close filters-close">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<ul class="section-list">
                        <?php
                        $terms = get_terms(array(
                            'taxonomy'		=> 'faq_category',
                            'hide_empty' 	=> true
                        ));
                        foreach($terms as $term){
                            $term_url	= get_term_link($term->term_id, $term->taxonomy);
                            ?>   
                            <li>
                                <a class="section-link" href="<?= $term_url; ?>">
                                    <span class="section-icon">
                                        <svg class="icon">
                                            <use xlink:href="#star">
                                        </svg>
                                    </span>
                                    <span class="section-text">
                                        <?= $term->name; ?>
                                    </span>
                                </a>
                            </li>
                            <?php
                        }
                        ?> 
					</ul>
				</div>
			</div>
		</div>
	</main>
<?php get_footer(); ?>