<?php 
	/**
     * Template Name: FAQ
     */
	get_header();
?>
<main class="main">
	<div class="main-grid">
		<div class="left-column">
			<?php get_sidebar('menu'); ?>
		</div>
		<div class="double-column block shadow-block support-main-block">
			<div class="support-header">
				<h1 class="support-title">
					Ласкаво просимо до підтримки!
				</h1>
				<span class="support-subtitle">
					Тут ви можете знайти поширені питання у роботі з сайтом
				</span>
				<?php get_template_part('partials/search-faq'); ?>
			</div>
			<div class="support-body">
				<?php
					$terms = get_terms(array(
						'taxonomy'		=> 'faq_category',
						'hide_empty' 	=> true
					));
					foreach($terms as $term){
						$term_url	= get_term_link($term->term_id, $term->taxonomy);
						?>
				<div class="support-theme">
					<a href="<?= $term_url; ?>" class="support-theme-title">
						<div class="theme-icon green bg-color">
							<svg class="icon svg-fill">
								<use xlink:href="#question">
							</svg>
						</div>
						<h2 class="theme-title">
							<?= $term->name; ?>
						</h2>
					</a>
					<ul class="support-theme-questions">
						<?php 
							$args 		= array(
								'post_type'			=> 'faq',
								'posts_per_page'	=> 3,
								'tax_query' 		=> array(
									array(
										'taxonomy' 	=> $term->taxonomy,
										'field'    	=> 'term_id',
										'terms'    	=> $term->term_id
									)
								)
							);
							$query 		= new WP_Query($args);
							foreach($query->posts as $post_faq){
								echo '<li><a href="'.$term_url.'#'.$post_faq->post_name.'">'.$post_faq->post_title.'</a></li>';
							}
						?>
					</ul>
					<a href="<?= $term_url; ?>" class="btn support-theme-more">
						Більше
					</a>
				</div>
				<?php
					}
				?>
			</div>
		</div>
	</div>
</main>

<?php get_footer(); ?>