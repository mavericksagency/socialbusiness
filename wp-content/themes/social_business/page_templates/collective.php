<?php

/**
 * Template Name: Вибірка з параметрами
 */
get_header();
the_post();
global $type, $service, $geo, $inUa, $type_enterprises, $beneficiaries, $type_event, $topics, $type_knowledge_base, $type_resourse;
$filters            = get_field('filter');
$type_post          = get_field('types');

$type_knowledge_base = in_array('type_knowledge_base', $filters);
$type_resourse      = in_array('type_resourse', $filters);
$service             = in_array('service', $filters);
$geo                 = in_array('geo', $filters);
$inUa                 = in_array('inUa', $filters);
//$type_enterprises   = in_array('type_enterprises', $filters);
$beneficiaries      = in_array('beneficiaries', $filters);
$type_event         = in_array('type_event', $filters);
$topics             = in_array('topics', $filters);

?>
<main class="main">
    <div class="main-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <div class="center-column">
            <?php get_template_part('partials/search-form');
            // if(in_array('resources', $type_post) || in_array('enterprises', $type_post)){
            //     $template = 'post-enterprises';
            // }else{
            //     $template = 'post-short';
            // }
            ?>

            <div id="content" class="panels-grid">
                <?php
                $args         = array(
                    'post_type'        => $type_post,
                    'posts_per_page' => 10,
                    'post_status'   => 'publish',
                    'order'            => 'DESC'
                );

                if ($type_knowledge_base || $inUa || $type_resourse) {
                    $args['meta_query'] = array(
                        'relation' => 'OR'
                    );


                    if ($type_knowledge_base) {
                        if (isset($_GET['type_knowledge_base'])) {
                            $type_knowledge_base = explode(",", $_GET['$type_knowledge_base']);
                        }
                        $types_knowledge_base            = get_field('type_knowledge_base');

                        $args['meta_query']['type_knowledge_base'] = array(
                            'key'           => 'type',
                            'value'         => $types_knowledge_base,
                            'compare'       => 'IN'
                        );
                    }

                    if ($type_resourse) {
                        if (isset($_GET['type_resourse'])) {
                            $type_resourse = explode(",", $_GET['$type_resourse']);
                        }
                        $types_resourse                = get_field('type_resourse');
                        $args['meta_query']['type_resourse'] = array(
                            'key'           => 'type',
                            'value'         => $types_resourse,
                            'compare'       => 'IN'
                        );
                    }

                    if ($inUa) {
                        $args['meta_query']['socbusinua'] = array(
                            'key'           => 'sb_in_ua',
                            'value'         => 1,
                            'compare'       => '='
                        );
                    }
                }

                $query        = new WP_Query($args);
                if ($query->have_posts()) {
                    while ($query->have_posts()) {
                        $query->the_post();
                        get_template_part('partials/post/post-short');
                    }
                    wp_reset_postdata();
                } else {
                    ?>
                    <div class="clear-block">
                        <img class="clear-image" src="<?php bloginfo('template_directory'); ?>/img/no-result.svg">
                        <h2 class="clear-title">Немає активних підприємств</h2>
                    </div>
                <?php
                }
                ?>
            </div>
            <div class="load" id="load"></div>
            <!-- test 
            
            <?php
            echo '<pre>';
            print_r($args);
            echo '</pre>';
            ?>
            area -->
            <div class="load-more-sec text-center" <?php if (!$query->have_posts() || $query->max_num_pages == 1) {
                                                        echo 'style="display: none;"';
                                                    } ?>>
                <button type="button" id="load_more_filter" class="btn">Завантажити ще</button>
            </div>
            <script>
                var post_type = '<?= implode(',', $type_post); ?>';
                var current_page = 2;
                var max_pages = '<?= $query->max_num_pages; ?>';
            </script>
        </div>
        <div class="right-column settings-column">
            <?php get_sidebar('filter') ?>
        </div>
    </div>
</main>
<?php get_footer(); ?>