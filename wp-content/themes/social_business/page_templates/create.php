<?php 
get_header(); 
/**
 * Template Name: Створити
 */
global $current_user, $user_id;
    ?>
<main class="main">
    <div class="main-grid account-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <?php 
            if($user_id){
                if(isset($_GET['enterprises'])){
                    //підприємства
                    get_template_part('partials/edit/enterprises');
                }else if(isset($_GET['vacancy']) || isset($_GET['opportunities'])){
                    //вакансії
                    get_template_part('partials/edit/vacancy');
                }else if(isset($_GET['event'])){
                    //події
                    get_template_part('partials/edit/event');
                }else if(isset($_GET['news'])){
                    //новини
                    get_template_part('partials/edit/news');
                }else if(isset($_GET['educ'])){
                    //освітні майданчики
                    get_template_part('partials/edit/educ');
                }else if(isset($_GET['support'])){
                    //організації
                    get_template_part('partials/edit/support');
                }else if(isset($_GET['funds'])){
                    //фонди
                    get_template_part('partials/edit/funds');
                }else if(isset($_GET['universities'])){
                    //ВНЗ
                    get_template_part('partials/edit/universities');
                }else if(isset($_GET['lector'])){
                    //Лектор
                    get_template_part('partials/edit/lector');
                }else if(isset($_GET['organizations'])){
                    //організація
                    get_template_part('partials/edit/organizations');
                }else if(isset($_GET['knowledge_base'])){
                    //база знань
                    get_template_part('partials/edit/knowledgebase');
                }else{
                    echo 'Шаблон відсутній!';
                }
            }else{
                get_template_part('partials/no-access');
            }
            get_sidebar('cabinet');
        ?>
    </div>
    
</main>
<?php get_footer(); ?>