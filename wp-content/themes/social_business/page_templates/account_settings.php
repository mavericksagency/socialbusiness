<?php 
    get_header();  
    /**
     * Template Name: Налаштування аккаунта
     */
    global $current_user, $user_id; 
    ?>
<main class="main">
    <div class="main-grid account-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <?php 
            if(!$user_id){
                get_template_part('partials/no-access-edit');
            }else{
        ?>
        <form id="user_info" action="#" method="POST" class="center-column">
            <div class="account-block shadow-block settings-block" id="profile_info_1">
                <div class="account-header blue">
                    <span class="setting-icon bg-color">
                        <svg class="icon svg-fill">
                            <use xlink:href="#person">
                        </svg>
                    </span>
                    <h2 class="setting-title">
                        Загальні налаштування
                    </h2>
                </div>
                <div class="settings-fields" id="general">
                    <div class="field">
                        <span class="field-title">
                        Фото профілю
                        </span>
                        <?php 
                            //get photo user
                            
                            $avatar = get_field('avatar', 'user_'.$user_id);
                            
                            ?>
                        <input type="hidden" name="id_photo" id="id_photo" <?php if($avatar){ echo 'value="'.$avatar['ID'].'"'; } ?>>
                        <!-- for loader add class 'loading' -->
                        <label class="field-content field-upload-photo">
                            <input type="file" name="photo" id="photo_load" size="500000" accept=".png,.jpg,.jpeg,.gif" class="input-photo">
                            <span class="add-photo" id="photo">
                                <span class="photo-icon" <?php if($avatar){ echo 'style="background-image: url('.$avatar['url'].')"'; } ?>>
                                    <?php if(!$avatar){ ?>
                                    <svg class="icon">
                                        <use xlink:href="#photo">
                                    </svg>
                                    <?php } ?>
                                </span>
                                <span class="add-photo-text">
                                Завантажити фото
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="field">
                        <label class="input-label">
                        <span class="input-desc">Ім'я</span>
                        <input type="text" name="first_name" class="input" value="<?= get_the_author_meta('first_name', $user_id); ?>" required placeholder="Введіть ім'я" maxlength="60">
                        <span class="valid-notification"></span>
                        </label>
                    </div>
                    <div class="field">
                        <label class="input-label">
                        <span class="input-desc">Прізвище</span>
                        <input type="text" name="last_name" class="input" value="<?= get_the_author_meta('last_name', $user_id); ?>" required placeholder="Введіть прізвище" maxlength="60">
                        <span class="valid-notification"></span>
                        </label>
                    </div>
                    <div class="field">
                        <label class="input-label">
                        <span class="input-desc">Дата народження</span>
                        <input type="text" name="day_birth" class="input datepicker" autocomplete="off" value="<?= get_field('date_of_birth', 'user_'.$user_id); ?>" placeholder="Введіть дату народження">
                        <span class="valid-notification"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="account-block shadow-block settings-block" id="profile_info_2">
                <div class="account-header red">
                    <span class="setting-icon bg-color">
                        <svg class="icon svg-fill">
                            <use xlink:href="#map-mark">
                        </svg>
                    </span>
                    <h2 class="setting-title">
                        Контакти
                    </h2>
                </div>
                <?php 
                    $countact_info = get_field('contacts', 'user_'.$user_id);
                    
                    
                    
                    ?>
                <div class="settings-fields">
                    <div class="field">
                        <label class="input-label">
                        <span class="input-desc">Країна</span>
                        <input type="text" name="contact_country" value="<?= $countact_info['country']; ?>" class="input" placeholder="Введіть назву країни" maxlength="60">
                        <span class="valid-notification"></span>
                        </label>
                    </div>
                    <div class="field">
                        <label class="input-label">
                        <span class="input-desc">Місто</span>
                        <input type="text" name="contact_city" value="<?= $countact_info['city']; ?>" class="input" placeholder="Введіть місто" maxlength="60">
                        <span class="valid-notification"></span>
                        </label>
                    </div>
                    <div class="field has-show">
                        <label class="input-label">
                        <span class="input-desc">Телефон</span>
                        <input type="tel" name="contact_phone" value="<?= $countact_info['phone']; ?>" class="input" placeholder="Введіть телефон" maxlength="15">
                        <span class="valid-notification"></span>
                        </label>
                        <label class="show-in-profile">
                            <input type="checkbox" name="hide_phone" class="input-show" <?php if($countact_info['hide_phone']){ echo 'checked'; } ?> value="1">
                            <span class="show-label-inner">
                                <span class="show-image">
                                    <svg class="icon">
                                        <use xlink:href="#lock">
                                    </svg>
                                </span>
                                <span class="not-show-text text">
                                Приховано в профілі
                                </span>
                                <span class="show-text text">
                                Загальнодоступно
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="field has-show">
                        <label class="input-label">
                        <span class="input-desc">Електронна пошта</span>
                        <input type="email" name="contact_email" value="<?= $current_user->data->user_email; ?>" class="input" disabled placeholder="Введіть e-mail" maxlength="100">
                        <span class="valid-notification"></span>
                        </label>
                        <label class="show-in-profile">
                            <input type="checkbox" class="input-show" name="hide_email" <?php if($countact_info['hide_email']){ echo 'checked'; } ?> value="1">
                            <span class="show-label-inner">
                                <span class="show-image">
                                    <svg class="icon">
                                        <use xlink:href="#lock">
                                    </svg>
                                </span>
                                <span class="not-show-text text">
                                Приховано в профілі
                                </span>
                                <span class="show-text text">
                                Загальнодоступно
                                </span>
                            </span>
                        </label>
                    </div>
                    <div class="field has-show">
                        <label class="input-label">
                        <span class="input-desc">Веб-сайт</span>
                        <input type="url" name="contact_website" value="<?= $countact_info['website']; ?>" class="input" placeholder="Введіть адресу сайту" maxlength="100">
                        <span class="valid-notification"></span>
                        </label>
                        <label class="show-in-profile">
                            <input type="checkbox" class="input-show" name="hide_website" <?php if($countact_info['hide_website']){ echo 'checked'; } ?> value="1">
                            <span class="show-label-inner">
                                <span class="show-image">
                                    <svg class="icon">
                                        <use xlink:href="#lock">
                                    </svg>
                                </span>
                                <span class="not-show-text text">
                                Приховано в профілі
                                </span>
                                <span class="show-text text">
                                Загальнодоступно
                                </span>
                            </span>
                        </label>
                    </div>
                    <?php 
                        $social_network = get_field('social_network', 'user_'.$user_id);
                    ?>
                    <div class="field">
                        <span class="field-title">
                            Соціальні мережі
                        </span>
                        <label class="socn-label">
                            <span class="soc-identify facebook-color">
                                <span class="soc-icon bg-color">
                                    <svg class="icon svg-fill">
                                        <use xlink:href="#facebook">
                                    </svg>
                                </span>
                                <span class="soc-name">
                                    Facebook
                                </span>
                            </span>
                            <input type="text" name="facebook" value="<?= $social_network['facebook']; ?>" class="input" placeholder="" maxlength="100">
                        </label>
                        <label class="socn-label">
                            <span class="soc-identify instagram-color">
                                <span class="soc-icon bg-color">
                                    <svg class="icon svg-fill">
                                        <use xlink:href="#instagram">
                                    </svg>
                                </span>
                                <span class="soc-name">
                                    Instagram
                                </span>
                            </span>
                            <input type="text" name="instagram" value="<?= $social_network['instagram']; ?>" class="input" placeholder="" maxlength="100">
                        </label>
                        <label class="socn-label">
                            <span class="soc-identify twitter-color">
                                <span class="soc-icon bg-color">
                                    <svg class="icon svg-fill">
                                        <use xlink:href="#twitter">
                                    </svg>
                                </span>
                                <span class="soc-name">
                                    Twitter
                                </span>
                            </span>
                            <input type="text" name="twitter" value="<?= $social_network['twitter']; ?>" class="input" placeholder="" maxlength="100">
                        </label>
                        <label class="socn-label">
                            <span class="soc-identify linkedin-color">
                                <span class="soc-icon bg-color">
                                    <svg class="icon svg-fill">
                                        <use xlink:href="#linkedin">
                                    </svg>
                                </span>
                                <span class="soc-name">
                                    Linkedin
                                </span>
                            </span>
                            <input type="text" name="linkedin" value="<?= $social_network['linkedin']; ?>" class="input" placeholder="" maxlength="100">
                        </label>
                        <label class="socn-label">
                            <span class="soc-identify youtube-color">
                                <span class="soc-icon bg-color">
                                    <svg class="icon svg-fill">
                                        <use xlink:href="#youtube">
                                    </svg>
                                </span>
                                <span class="soc-name">
                                    Youtube
                                </span>
                            </span>
                            <input type="text" name="youtube" value="<?= $social_network['youtube']; ?>" class="input" placeholder="" maxlength="100">
                        </label>
                    </div>
                </div>
            </div>
            <div class="account-block shadow-block settings-block" id="div_education">
                <div class="account-header violet">
                    <span class="setting-icon bg-color">
                        <svg class="icon svg-fill">
                            <use xlink:href="#education">
                        </svg>
                    </span>
                    <h2 class="setting-title">
                        Освіта
                    </h2>
                </div>
                <?php 
                    $education_info = get_field('education', 'user_'.$user_id);
                    ?>
                <div class="settings-fields">
                    <div class="field">
                        <div class="field has-show">
                            <label class="input-label">
                            <span class="input-desc">ВНЗ</span>
                            <input type="text" name="university" value="<?= $education_info['university']; ?>" class="input" placeholder="Введіть ВНЗ" maxlength="300">
                            <span class="valid-notification"></span>
                            </label>
                        </div>
                    </div>
                    <div class="field">
                        <div class="field has-show">
                            <label class="input-label">
                            <span class="input-desc">Факультет</span>
                            <input type="text" name="faculty" value="<?= $education_info['faculty']; ?>" class="input" placeholder="Факультет" maxlength="300">
                            <span class="valid-notification"></span>
                            </label>
                        </div>
                    </div>
                    <!-- <div class="field">
                        <span class="field-title">
                        Форма навчання
                        </span>
                        <div class="field-group half-width">
                            <select name="study_div">
                                <option value="1" <?php if($education_info[ 'study_div']=='1' ){ echo 'selected'; } ?>>Стаціонар</option>
                                <option value="2" <?php if($education_info[ 'study_div']=='2' ){ echo 'selected'; } ?>>Дистанційна</option>
                            </select>
                        </div>
                    </div> -->
                </div>
            </div>
            <div class="account-block shadow-block settings-block" id="div_job">
                <div class="account-header brown">
                    <span class="setting-icon bg-color">
                        <svg class="icon svg-fill">
                            <use xlink:href="#bag">
                        </svg>
                    </span>
                    <h2 class="setting-title">
                        Кар'єра
                    </h2>
                </div>
                <?php 
                    $career_info = get_field('career', 'user_'.$user_id);
                    
                    
                    
                    ?>
                <div class="settings-fields">
                    <div class="field has-show">
                        <label class="input-label">
                        <span class="input-desc">Місце роботи</span>
                        <input type="text" name="work_place" value="<?= $career_info['work_place']; ?>" class="input" placeholder="Введіть місце роботи" maxlength="300">
                        <span class="valid-notification"></span>
                        </label>
                    </div>
                    <div class="field">
                        <label class="input-label">
                        <span class="input-desc">Посада</span>
                        <input type="text" name="position" value="<?= $career_info['position']; ?>" class="input" placeholder="Введіть посаду" maxlength="100">
                        <span class="valid-notification"></span>
                        </label>
                    </div>
                </div>
            </div>
            <div class="account-block shadow-block settings-block" id="other_info">
                <div class="account-header brown">
                    <span class="setting-icon bg-color">
                        <svg class="icon svg-fill">
                            <use xlink:href="#bag">
                        </svg>
                    </span>
                    <h2 class="setting-title">
                        Інше
                    </h2>
                </div>
                <div class="settings-fields">
                    <div class="field">
                        <span class="field-title">
                        Про себе
                        </span>
                        <label class="input-label full-width">
                        <span class="input-desc"></span>
                        <textarea class="textarea" name="description" placeholder="Декілька слів про себе" rows="10" maxlength="1000"><?= get_field('about_myself', 'user_'.$user_id); ?></textarea>
                        <span class="valid-notification"></span>
                        </label>
                    </div>
                </div>
                <div class="account-footer">
                    <button type="submit" class="btn-raised submit">
                    <span class="save-text">
                    Зберегти
                    </span>
                    </button>
                    <span class="saved-text">
                    Збережено
                    </span>
                </div>
            </div>
        </form>
        <?php } ?>
        <?php 
            if($user_id){
                get_sidebar('cabinet'); 
            }
        ?>
    </div>
</main>
<?php get_footer(); ?>