<?php 
get_header(); 
/**
 * Template Name: Ресурси
 */
    global $user_id;
    ?>
<main class="main">
    <div class="main-grid account-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <?php 
?>
    <div class="center-column" id="ajax_enterprise">
    <?php if($user_id){ ?>
        <div class="account-block shadow-block settings-block" id="main_info">
            <div class="account-header red">
                <span class="setting-icon bg-color">
                    <svg class="icon svg-fill">
                        <use xlink:href="#buildings">
                    </svg>
                </span>
                <h2 class="setting-title">
                    Оберіть ресурс для створення
                </h2>
            </div>
            <ul class="section-list">
                <?php $createPage = get_the_permalink(53); ?>
                <?php if(has_lector()){ ?>
                <li><a class="section-link" href="<?= $createPage; ?>?lector"><span class="section-text">Лектора</span></a></li>
                <?php } ?>
                <li><a class="section-link" href="<?= $createPage; ?>?universities"><span class="section-text">ВНЗ</span></a></li>
                <li><a class="section-link" href="<?= $createPage; ?>?support"><span class="section-text">Організація, що підтримує соціальний бізнес</span></a></li>
                <li><a class="section-link" href="<?= $createPage; ?>?funds"><span class="section-text">Фонд</span></a></li>
                <li><a class="section-link" href="<?= $createPage; ?>?educ"><span class="section-text">Освітній майданчик</span></a></li>
                <!-- <li><a class="section-link" href="<?= $createPage; ?>?organizations"><span class="section-text">Організацію</span></a></li> -->
            </ul>
        </div>
        <?php }else{
            get_template_part('partials/no-access');
        } ?>
    </div>
    <?php get_sidebar('cabinet'); ?>
    </div>
</main>
<?php get_footer(); ?>