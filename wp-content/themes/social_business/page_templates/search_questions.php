<?php 
/**
 * Template Name: Консультація
 */
	get_header();
	
	if($_GET['question']){
		get_template_part('/partials/edit/message');
	}else{
?>

<main class="main">
	<div class="main-grid">
		<div class="left-column">
			<?php get_sidebar(); ?>
		</div>
		<div class="center-column">
			<form action="<?= get_the_permalink(544); ?>" type="POST" class="search-form">
				<button class="btn-transparent">
					<svg class="icon">
						<use xlink:href="#search">
					</svg>
				</button>
				<label class="input-with-icon main-search">
					<input name="s" type="text" required placeholder="Введіть ключові слова">
				</label>
			</form>
			<button class="btn-settings">
				<svg class="icon">
					<use xlink:href="#controls">
				</svg>
			</button>
			<div class="search-tabs">
				<button class="search-tab-item current-item">
					<span class="text">Всі питання</span>
					<span class="number">22 445</span>
				</button>
				<button class="search-tab-item">
					<span class="text">Мої питання</span>
					<span class="number">5</span>
				</button>

				<a href="?question=1" class="btn add-question">
					Задати запитання
				</a>
			</div>
			<div class="block shadow-block panel-main-lg question-panel">
				<div class="question-panel-main">
					<a href="/" class="question-title">
						Lorem ipsum dolor sit amet, consectetur adipisicing elit. Reiciendis voluptates harum aut, illum veniam quo dolore id voluptate consequatur labore?
					</a>
					<div class="panel-footer">
						<div class="tags">
							<a href="/" class="tag">Lorem</a>
							<a href="/" class="tag">Ipsum</a>
							<a href="/" class="tag">Ipsum lorem</a>
						</div>
					</div>
				</div>
				<div class="question-panel-author">
					<div class="author-image blue bg-color">
						<svg class="icon svg-fill">
							<use xlink:href="#person">
						</svg>
					</div>
					<a href="/" class="author-name">
						Ігор Прохоренко
					</a>
				</div>
			</div>
			<div class="block shadow-block panel-main-lg question-panel">
				<div class="question-panel-main">
					<a href="/" class="question-title">
						Lorem ipsum dolor sit amet.
					</a>
					<div class="panel-footer">
						<div class="tags">
							<a href="/" class="tag">Lorem</a>
							<a href="/" class="tag">Ipsum</a>
							<a href="/" class="tag">Ipsum lorem</a>
						</div>
					</div>
				</div>
				<div class="question-panel-author">
					<div class="author-image blue bg-color">
						<img src="img/img-person.png" alt="Person">
					</div>
					<a href="/" class="author-name">
						Ігор Прохоренко
					</a>
				</div>
			</div>
		</div>
		<div class="right-column settings-column">
			<?php get_sidebar('filter') ?>
		</div>
	</div>
</main>

<?php 
	}
	get_footer();
?>