<?php 
get_header(); 
/**
 * Template Name: Створити підприємство
 */
global $current_user, $user_id;
    ?>
<main class="main">
    <div class="main-grid account-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <?php 
            if(isset($_GET['enterprises'])){
                get_template_part('partials/edit/enterprises');
            }else if(isset($_GET['event'])){
                get_template_part('partials/edit/event');
            }else{
                echo 'Шаблон відсутній!';
            }
        ?>
        <div class="right-column anchor-sections-column">
            <div class="block anchors-menu section-block">
                <h2 class="section-title">
                    Розділи
                </h2>
                <div class="section-accordeons">
                    <div class="accordion navscroll" data-move-to="#main_info">
                        <div class="accordion-header">
                            <div class="accordion-title">
                                <div class="accordion-image">
                                    <svg class="icon">
                                        <use xlink:href="#person">
                                    </svg>
                                </div>
                                <span class="accordion-title__text">Основна інформація</span>
                            </div>
                        </div>
                    </div>
                    <div class="accordion navscroll" data-move-to="#contacts">
                        <div class="accordion-header">
                            <div class="accordion-title">
                                <div class="accordion-image">
                                    <svg class="icon">
                                        <use xlink:href="#person">
                                    </svg>
                                </div>
                                <span class="accordion-title__text">Контакти</span>
                            </div>
                        </div>
                    </div>
                    <div class="accordion navscroll" data-move-to="#workers">
                        <div class="accordion-header">
                            <div class="accordion-title">
                                <div class="accordion-image">
                                    <svg class="icon">
                                        <use xlink:href="#person">
                                    </svg>
                                </div>
                                <span class="accordion-title__text">Працівники</span>
                            </div>
                        </div>
                    </div>
                    <div class="accordion navscroll" data-move-to="#other_info">
                        <div class="accordion-header">
                            <div class="accordion-title">
                                <div class="accordion-image">
                                    <svg class="icon">
                                        <use xlink:href="#person">
                                    </svg>
                                </div>
                                <span class="accordion-title__text">Додаткова інформація</span>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
<?php get_footer(); ?>