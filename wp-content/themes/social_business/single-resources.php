<?php 
    global $type, $bookmarks;
    get_header(); 
    the_post();
    $contact_person                 =   get_field('contact_person');
    $typeResourse                   =   get_field('type');
    $phone                          =   get_field('phone');
    $email                          =   get_field('email');
    $url_soc                        =   get_field('url_soc');
    $education                      =   get_field('education');
    $keys                           =   get_field('keys');
    $org_before                     =   get_field('org_before');
    $faculty                        =   get_field('faculty');
    $address                        =   get_field('address');
    $contact_person                 =   get_field('contact_person');
    $support                        =   get_field('support');
    $about                          =   get_field('about');

    $admins                         =   get_field('page_administrators');
    $admins                         =   explode(',', $admins);
    $sbInUAPost                     =   get_field('sb_in_ua', $post->ID);
    if($typeResourse == 'lector'){
        get_template_part('partials/post/lector');
    }else{
?>
<main class="main">
    <div class="main-grid">
    <div class="left-column">
        <?php get_sidebar(); ?>
    </div>
    <div class="double-column">
        <div class="panel-main-lg page-main-panel <?= 'class-'.$post->post_type; ?>">
            <div class="item-main">
                <?php 
                    $logo   = get_the_post_thumbnail_url($post->ID, 'full');
                ?>
                <div class="item-image <?php if(!$logo){echo 'inner-svg';}?>" <?php if($logo){ echo 'style="background-image: url('.$logo.')";'; } ?>>
                    <?php if(!$logo){ 
                        $iconPublish   	= getIconPostType($post->ID, $post->post_type);
                        ?>
                    <svg class="icon svg-fill">
                        <use xlink:href="#<?= $iconPublish; ?>">
                    </svg>
                    <?php } ?>
                </div>
                <div class="item-info">
                    <h1 class="item-title">
                        <?php the_title(); ?>
                        <?php 
                            if($sbInUAPost){
                                echo '<span class="sb-mark">SBinUA</span>';
                            }
                        ?>
                    </h1>
                    <span class="item-desc"><?= get_type_resourse($post); ?></span>
                </div>
            </div>
            <div class="panel-main-buttons">
                <a href="#" class="btn-main-panel btn btn-share share-button" data-url="<?php the_permalink(); ?>">
                    <svg class="icon">
                        <use xlink:href="#share">
                    </svg>
                </a>
                <button class="btn-main-panel btn btn-mark add_bookmark <?php if(in_array($post->ID, $bookmarks, FALSE)){ echo 'added'; } ?>" data-id="<?= $post->ID; ?>">
                    <svg class="icon">
                        <use xlink:href="#star">
                    </svg>
                    <span class="text">До закладок</span>
                </button>
            </div>
        </div>
        <div class="main-grid user-grid">
            <div class="center-column">
                <div class="info-source shadow-block">
                    <h3 class="info-title">Контакти</h3>
                    <ul class="info-list">
                        <?php if($address){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#map-mark-stroke">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Адреса</span>
                                    <span class="text"><?= $address ?></span>
                                </div>
                            </li>
                        <?php } ?>
                        <?php if($city){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#map-mark-stroke">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Місто</span>
                                    <span class="text"><?= $city ?></span>
                                </div>
                            </li>
                        <?php } ?>
                        <?php if($phone){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#telephone">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Телефон</span>
                                    <a href="tel:<?= $phone ?>" class="info-link"><?= $phone ?></a>
                                </div>
                            </li>
                        <?php } ?>
                        <?php if($email){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#mail">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Email</span>
                                    <a href="mailto:<?= $email ?>" target="_blank" class="info-link"><?= $email ?></a>
                                </div>
                            </li>
                        <?php } ?>
                        <?php if($url_soc){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#www">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Посилання на сайт або сторінку</span>
                                    <a href="<?= $url_soc ?>" target="_blank" class="info-link"><?= $url_soc ?></a>
                                </div>
                            </li>
                        <?php } ?>
                        <?php if($education){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#educ">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Освіта</span>
                                    <span class="text"><?= $education ?></span>
                                </div>
                            </li>
                        <?php } ?>
                        <?php if($keys){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#keys">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Ключові кваліфікації</span>
                                    <span class="text"><?= $keys ?></span>
                                </div>
                            </li>
                        <?php } ?>
                        
                        
                    </ul>
                    <div class="more">
                        <button class="more-text btn-transparent">
                            <span class="text active">Детальна інформація</span>
                            <span class="text">Приховати детальну інформацію</span>
                        </button>
                    </div>
                    <div class="more-panel">
                        <h3 class="info-title">Детальна інформація</h3>
                        <ul class="info-list">
                            <?php if($org_before){ ?>
                                <li class="info-list-item">
                                    <span class="info-type">
                                        <svg class="icon">
                                            <use xlink:href="#organizations">
                                        </svg>
                                    </span>
                                    <div class="info-content">
                                        <span class="info-text">Організації, з якими раніше працювали, і які можуть дати рекомендації</span>
                                        <span class="text"><?= $org_before ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php 
                            //services
                            $getTerms   = get_the_terms($post->ID, 'service');
                            if($getTerms){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#business-sphere">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Сфери послуг:</span>
                                    <span class="text"><?= getTermsName($getTerms); ?></span>
                                </div>
                            </li>
                            <?php } ?>
                            <?php 
                            //topics
                            $getTerms   = get_the_terms($post->ID, 'topics');
                            if($getTerms){ ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#theme">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Тема:</span>
                                    <span class="text"><?= getTermsName($getTerms); ?></span>
                                </div>
                            </li>
                            <?php } ?>
                            <?php if($faculty){ ?>
                                <li class="info-list-item">
                                    <span class="info-type">
                                        <svg class="icon">
                                            <use xlink:href="#faculty">
                                        </svg>
                                    </span>
                                    <div class="info-content">
                                        <span class="info-text">Факультет, на якому викладається соціальне підприємництво</span>
                                        <span class="text"><?= $faculty ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php if($contact_person['person'] && $contact_person['position']){ ?>
                                <li class="info-list-item">
                                    <span class="info-type">
                                        <svg class="icon">
                                            <use xlink:href="#person">
                                        </svg>
                                    </span>
                                    <div class="info-content">
                                        <span class="info-text">Контактна особа</span>
                                        <span class="text"><?= $contact_person['person'].' '.$contact_person['position'] ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                            <?php if($support){ 
                                $supportVariation = get_field_object('field_5bc72fab7ee66');
                                ?>
                                <li class="info-list-item">
                                    <span class="info-type">
                                        <svg class="icon">
                                            <use xlink:href="#support">
                                        </svg>
                                    </span>
                                    <div class="info-content">
                                        <span class="info-text">Підтримка, яка надається соціальному підприємництву</span>
                                        <span class="text"><?= $supportVariation['choices'][$support] ?></span>
                                    </div>
                                </li>
                            <?php } ?>
                        </ul>
                        <?php if($about){ ?>
                            <div class="info-article">
                                <span class="info-title">
                                    Про ресурс <?= getTypeName($typeResourse); ?>
                                </span>
                                <div class="info-article-text">
                                    <p>
                                        <?= $about ?>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                        <?php if($post->post_content){ ?>
                            <div class="info-article">
                                <span class="info-title">
                                    Додаткова інформація 
                                </span>
                                <div class="info-article-text">
                                    <p>
                                        <?= apply_filters('the_content', $post->post_content); ?>
                                    </p>
                                </div>
                            </div>
                        <?php } ?>
                    </div>
                </div>
                <div class="wall shadow-block">
                    <div class="wall-header">
                        <?php 
                            $args = array(
                                'post_type'         => array('news', 'event', 'opportunities', 'knowledge_base'),
                                'posts_per_page'    => 10,
                                'order'             => 'DESC',
                                'meta_query'        => array(
                                    'public'        => array(
                                        'key'       => 'public',
                                        'value'     => $post->post_type.'_'.$post->ID,
                                        'compare'   => 'LIKE',
                                    )
                                )
                            );
                            $query_all              = new WP_Query($args);
                            get_template_part('partials/wall-result');
                        ?>
                </div>

            </div>

            <div class="right-column">
                <div class="block siblings-block">
                    <h2 class="block-title">
                        Редактори
                    </h2>
                    <?php 
                        foreach($admins as $user_admin){
                            $user_admin_id  = explode('_', $user_admin)[1];
                            if($user_admin_id){
                                get_template_part('partials/author/in-post-short');
                            }
                        }
                    ?>
                </div>
                <button class="btn-block share-button" data-url="<?php the_permalink(); ?>">
                    <svg class="icon">
                        <use xlink:href="#share">
                    </svg>
                    <span class="text">
                        Поширити сторінку
                    </span>
                </button>
                <?php if(in_array('user_'.$user_id, $admins)){ ?>
                <a href="<?= get_the_permalink(53) ?>?<?= $typeResourse; ?>&post_id=<?= $post->ID; ?>" class="btn-block edit-pen">
                    <svg class="icon">
                        <use xlink:href="#pen">
                    </svg>
                    <span class="text">
                        Редагувати
                    </span>
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<?php 
}
get_footer();