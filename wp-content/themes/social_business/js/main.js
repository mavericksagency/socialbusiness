$(document).ready(function() {
	accordion();
	openCreateMenu();
	slider();
	//addToFavourite();
	modalFilter();
	cleanFilters();
	openMore();
	// wallTabs();
	moreAuthors();
	selectStylize();
	showInProfile();
	//anchors();
	deleteFromLoaded();
	openMobileMenu();
	openUserMenu();
	openTopMenu();
	openSettings();
	userSelectionMenu();
	createMenuMob();
	openSearchModal();

	$(window).on('resize', function() {
		openTopMenu();
		createMenuMob();
	});

	/*
	* register - next step, open more inputs
	*/
	$('.btn-register-next-step').on('click', function(e){
		e.preventDefault();
		var nextStep = false,
			countEl = 0;
		$('.register-first-step').each(function(){
			var el = $(this);
			if(el.children('input').val() == 0){
				el.addClass('valid-false');
				nextStep = false;
			} else {
				el.removeClass('valid-false');
				countEl++;
				if (countEl == $('.register-first-step').length){
					nextStep = true;
				}
			}
		});

		if (nextStep) {
			$('.register-first-step').removeClass('valid-false');
			$('.sign-form .input-label').removeClass('register-next-step');
			$('.btn-register-next-step').addClass('display-none');
			$('.btn-go').removeClass('display-none');
		}
	});


	/*
	* open main menu when scroll up
	 */
	var scrollTopW = 0;
	$(window).on('scroll', function(){
		var sct = $(this).scrollTop();
		var topMin;
		if($(window).width() > 1200){
			topMin = 200;
		} else {
			topMin = 100;
		}
		if (sct > scrollTopW) {
			$('.header').removeClass('header-fixed');
		}
		if(($(this).scrollTop() > 200) && (sct <= scrollTopW)){
			$('.header').addClass('header-fixed');
		}
		scrollTopW = sct;
	});

	/*
	*open post as a window, when click on '.open-post'
	*/
	// $('.open-post').on('click', function(e) {
	// 	e.preventDefault();
	// 	openModal($('.post-modal'));
	// });

	/*
	* sign in - open modal
	*/
	$('.btn-enter').on('click', function(e) {
		e.preventDefault();
		openModal($('.sign-in'));
		if($('.mob-sidemenu').hasClass('open-menu')){
			$('.mob-sidemenu').removeClass('open-menu');
		}
	});

	/*
	* sign up - open modal
	 */
	$('.btn-register').on('click', function(e) {
		e.preventDefault();
		openModal($('.sign-up'));
		if($('.mob-sidemenu').hasClass('open-menu')){
			$('.mob-sidemenu').removeClass('open-menu');
		}
	});

	/*
	* interaction when question is not solved
	*/
	$('.btn-not-solved').on('click', function() {
		$(this).parents('.simple-footer').addClass('display-none');
		$(this).parents('.communication-footer').find('.not-solved-footer').removeClass('display-none');
	});
	$('.btn-cancel').on('click', function() {
		var communicationFooter = $(this).parents('.communication-footer');
		communicationFooter.find('.simple-footer').removeClass('display-none');
		communicationFooter.find('.not-solved-footer').addClass('display-none');
		$('.not-solved-comment').addClass('display-none');
	});

	/*
	* add comment to unsolved question
	 */
	$('.add-comment').on('click', function(e) {
		e.preventDefault();
		$(this).parents('.not-solved-footer').addClass('display-none');
		$('.not-solved-comment').removeClass('display-none');
	});

	/*
	* add one more job, university etc.
	 */
	$('.add-more-settings').on('click', function() {
		var newElement = $(this).parent().prev().clone();
		newElement.insertBefore($(this).parent());
	});

	/*
	* open list with categories in user, sourse or community page (in the wall, down of the page)
	*/
	$('.wall-all').on('click', function(e) {
		e.preventDefault();
		$('.wall-all').next().toggleClass('categories-open');
	});
	$(document).on('keyup', function(event) {
		if (($('.wall-categories-list').hasClass('categories-open')) && (event.keyCode == '27')) {
			$('.wall-categories-list').removeClass('categories-open');
		}
	});
	$(document).on('click', function(event) {
		if (($('.wall-categories-list').hasClass('categories-open')) && !$(event.target).closest('.wall-all, .wall-categories-list').length) {
			$('.wall-categories-list').removeClass('categories-open');
		}
	});


	// /*sign open-close modals*/
	// $('.to-sign-up').on('click', function(e){
	// 	e.preventDefault();
	// 	closeOpenNextModal($(this).parents('.sign-modal'), $('.sign-up'));
	// });
	// $('.to-sign-in').on('click', function(e){
	// 	e.preventDefault();
	// 	closeOpenNextModal($(this).parents('.sign-modal'), $('.sign-in'));
	// });
	// $('.pass-forgot').on('click', function(e){
	// 	e.preventDefault();
	// 	closeOpenNextModal($(this).parents('.sign-modal'), $('.pass-recovery-mail'));
	// });
	// $('.btn-recov-go').on('click', function(e){
	// 	e.preventDefault();
	// 	if($(this).parents('pass-recovery-mail')){
	// 		closeOpenNextModal($('.pass-recovery-mail'), $('.pass-recovery-key'));
	// 	} else if($(this).parents('pass-recovery-key')){
	// 		closeOpenNextModal($('.pass-recovery-key'), $('.pass-recovery-newpass'));
	// 	}
	// });
});

/*
*open accordion (e. right side in cabinet)
*/
function accordion() {
	$('.accordion-header').on('click', function() {
		$(this).parents('.accordion').toggleClass('accordion-open');
	});
}

/*
* slide to the anchor
*/
// function anchors() {
// 	$(document).on('click', 'a[href^="#"]', function(event) {
// 		event.preventDefault();
// 		$('html, body').animate({
// 			scrollTop: $($.attr(this, 'href')).offset().top - 100
// 		}, 500);
// 	});
// }

/* 
* open 'create' menu in side menu
*/
function openCreateMenu() {
	$('.btn-create__menu').on('click', function() {
		if(api_settings.user_id == 0){
			openModal($('#sign-in'));
			return false;
		}
		$(this).parents('.menu-create').toggleClass('menu-create-open');
	});
}

/* 

* add sliders

*/
function slider() {
		$('.pages-slider').slick({
			dots: false,
			arrows: true,
			infinite: false,
			speed: 300,
			slidesToShow: 4,
			variableWidth: true,
			swipe: true,
			prevArrow: $('.pages-content .prev'),
			nextArrow: $('.pages-content .next'),
			responsive: [{
				breakpoint: 1367,
				settings: {
					slidesToShow: 3,
				}
			}, {
				breakpoint: 481,
				settings: {
					slidesToShow: 1,
					arrows: false,
				}
			}, ]
		});
		$('.pages-controls .prev').attr('aria-disabled', 'true').addClass('away');
		$('.pages-slider').on('afterChange', function() {
			$('.pages-controls .slider-control').each(function() {
				if ($(this).attr('aria-disabled') == 'true') {
					$(this).addClass('away');
				} else {
					$(this).removeClass('away');
				}
			})
		});
		$('.sign-slider').slick({
			dots: true,
			arrows: false,
			infinite: false,
			speed: 300,
			slidesToShow: 1,
		});
	}
	/*

	* identificator "add to list 'favourite'"

	*/
// function addToFavourite() {
// 		$('.mark').on('click', function() {
// 			if (!$(this).parents('.panel').hasClass('marked')) {
// 				$(this).parents('.panel').addClass('marked');
// 			} else {
// 				$(this).parents('.panel').removeClass('marked');
// 			}
// 		});
// 		$('.mark-with-icon').on('click', function() {
// 			if (!$(this).parents('.page-panel').hasClass('marked')) {
// 				$(this).parents('.page-panel').addClass('marked');
// 			} else {
// 				$(this).parents('.page-panel').removeClass('marked');
// 			}
// 		});
// 		$('.btn-mark').on('click', function() {
// 			if (!$(this).parents('.panel-main-lg').hasClass('marked')) {
// 				$(this).parents('.panel-main-lg').addClass('marked');
// 			} else {
// 				$(this).parents('.panel-main-lg').removeClass('marked');
// 			}
// 		});
// 	}
	/*

	* open filter block as a modal to choose items

	*/
function modalFilter() {
		$('.btn-tag-add').on('click', function() {
			$('body').addClass('body-overflow');
			$(this).next('.modal-filter').addClass('modal-filter-open');
			$modal = $(this).next('.modal-filter');
			$modal.find('.close-modal-filter').on('click', function() {
				closeFilterModal($modal);
			});
			$(window).on('click', function(event) {
				if (event.target == $('.modal-filter-open').get(0)) {
					closeFilterModal($modal);
				}
			});
			$(document).keyup(function(event) {
				if (event.keyCode == 27) closeFilterModal($modal);
			});
		});
		$('.btn-remove').on('click', function() {
			$(this).parents('.modal-filter').find('input[type="checkbox"]').prop('checked', false);;
		});
	}
	/*

	* open filter's modal block  

	* $modal - modal that was opened

	*/
function closeFilterModal($modal) {
		$modal.removeClass('modal-filter-open');
		$('body').removeClass('body-overflow');
	}
	/*

	* remove all choosed filters

	*/
function cleanFilters() {
		$('#clean-filters').on('click', function() {
			$('.filters-panel').find('input').prop('checked', false);
		});
	}
	/*

	* open detail info in user, source or community page

	*/
function openMore() {
		$('.more-text').on('click', function(e) {
			e.preventDefault();
			$(this).parent().next('.more-panel').toggleClass('open-more');
			if ($(this).parent().next('.more-panel').hasClass('open-more')) {
				$(this).find('.text').eq(0).removeClass('active');
				$(this).find('.text').eq(1).addClass('active');
			} else {
				$(this).find('.text').eq(1).removeClass('active');
				$(this).find('.text').eq(0).addClass('active');
			}
		})
	}
	/* 

	* open tab in user and company page

	*/
	// function wallTabs(){
	// 	$('.wall-tab').on('click', function(){
	// 		$(this).parent().find('.wall-tab').removeClass('active-tab');
	// 		$(this).addClass('active-tab');
	// 	});
	// }
	/*

	* open more authors panel in post page

	 */
function moreAuthors() {
		$('.many-authors').on('click', function() {
			$(this).find('.authors-amount').toggleClass('show');
		});
		$(document).on('click', function(event) {
			if ((!$(event.target).closest(".many-authors").length) && ($('.authors-amount').hasClass('show'))) {
				$('.authors-amount').removeClass('show');
			}
		});
	}
	/*

	* open modal

	* $modal - modal that must be opened. ex: $('.post-modal')

	*/
function openModal($modal) {
		$('body').addClass('body-overflow');
		$modal.addClass('open-modal');
		$modal.find('.close-modal').on('click', function() {
			closeModal($modal);
		});
		$(document).keyup(function(event) {
			if (event.keyCode == 27) closeModal($modal);
		});
	}
	/*

	* close modall

	* $modal - modal that must be closed. ex: $('.post-modal')

	*/
function closeModal($modal) {
		$modal.removeClass('open-modal');
		$('body').removeClass('body-overflow');
		$('body').removeClass('body-shadow');
	}
	/*

	* another style for select-option tags

	*/
function selectStylize(element) {
		if(!element){
			element = 'select';
		}
		
		$(element).each(function() {
			var $this = $(this),
				numberOfOptions = $(this).children('option').length;
			$this.addClass('select-hidden');
			$this.wrap('<div class="select"></div>');
			$this.after('<div class="select-inner"></div>');
			var $styledSelect = $this.next('div.select-inner');
			if($this.find('option:eq(0)').val() != $this.val()){
				$styledSelect.text($this.find('option:selected').text());
			} else{
				$styledSelect.text($this.children('option').eq(0).text());
				$this.find('option:eq(0)').attr('selected', true);
			}
			var $list = $('<ul />', {
				'class': 'select-list'
			}).insertAfter($styledSelect);
			for (var i = 0; i < numberOfOptions; i++) {
				if (!$(this).children('option').eq(i).attr('hidden')) {
					$('<li />', {
						text: $this.children('option').eq(i).text(),
						rel: $this.children('option').eq(i).val()
					}).appendTo($list);
				}
			}
			$list.hide();
			var $listItems = $list.children('li');
			$styledSelect.click(function(e) {
				e.stopPropagation();
				$('div.select-inner.active').not(this).each(function() {
					$(this).removeClass('active').next('ul.select-list').hide();
				});
				$(this).toggleClass('active').next('ul.select-list').toggle();
			});
			$listItems.click(function(e) {
				e.stopPropagation();
				$styledSelect.text($(this).text()).removeClass('active');
				$value = $(this).attr("rel");
				$this.val($value).change();
				$list.hide();
			});
			$(document).click(function() {
				$styledSelect.removeClass('active');
				$list.hide();
			});
		});
	}
	/*

	* mark in user cabinet whether field private or public

	*/
function showInProfile() {
		$('.show-in-profile').on('click', function() {
			$(this).toggleClass('not-show');
		})
	}
	/*

	delete file from loaded in message page

	 */
function deleteFromLoaded() {
		$('.loaded-file').each(function() {
			$(this).find('.btn-delete').on('click', function() {
				$(this).parents('.loaded-file').remove();
			});
		});
	}
	/* 

	* open mobile menu  < 1200px

	*/
function openMobileMenu() {
		$('.menu-icon').on('click', function() {
			$('body').addClass('body-overflow');
			$('body').addClass('body-shadow');
			$('.mob-sidemenu').addClass('open-menu');
			$('.mob-sidemenu').find('.btn-close').on('click', function() {
				closeMobileMenu();
			});
		});
		$(document).on('keyup', function(event) {
			if (($('.mob-sidemenu').hasClass('open-menu')) && (event.keyCode == '27')) {
				closeMobileMenu();
			}
		});
		$(document).on('click', function(event) {
			if ((!$(event.target).closest(".menu-icon,.mob-sidemenu").length) && ($('.mob-sidemenu').hasClass('open-menu'))) {
				closeMobileMenu();
			}
		});

		function closeMobileMenu() {
			$('body').removeClass('body-overflow');
			$('body').removeClass('body-shadow');
			$('.mob-sidemenu').removeClass('open-menu');
		}
	}
	/*

	*

	* open user menu when click on person's image or name in header

	 */
function openUserMenu() {
		$('.user-short-info').on('click', function() {
			if (!$('.top-user').hasClass('user-menu-open')) {
				$('.top-user').addClass('user-menu-open');
			} else {
				$('.top-user').removeClass('user-menu-open');
			}
		});
		$(document).on('keyup', function(event) {
			if (($('.top-user').hasClass('user-menu-open')) && (event.keyCode == '27')) {
				$('.top-user').removeClass('user-menu-open');
			}
		});
		$(document).on('click', function(event) {
			if (!$(event.target).closest('.user-menu, .top-user').length) {
				$('.top-user').removeClass('user-menu-open');
			}
		});
	}
	/*

	* open top mobile menu

	 */
function openTopMenu() {
		if ($(window).width() <= 768) {
			$('.mob-page-title').on('click', function() {
				if (!$('.top-menu').hasClass('top-menu-open')) {
					$('.top-menu').addClass('top-menu-open');
				} else {
					$('.top-menu').removeClass('top-menu-open');
				}
			});
			// $(document).on('keyup', function(event){
			// 	if(($('.top-menu').hasClass('top-menu-open')) && (event.keyCode == '27')){
			// 		$('.top-menu').removeClass('top-menu-open');
			// 	}
			// });
			$(document).on('click', function(event) {
				if (!$(event.target).closest('.top-menu, .mob-page-title').length) {
					$('.top-menu').removeClass('top-menu-open');
				}
			});
		}
	}
	/*

	* open setting panel in mobile

	 */
function openSettings() {
		$('.btn-settings').on('click', function() {
			$('body').addClass('body-overflow');
			$('.settings-column').addClass('settings-open');
			$('.filters-close').on('click', function() {
				$('.settings-column').removeClass('settings-open');
				$('body').removeClass('body-overflow');
			})
		});
	}
	/*open user menu in pages user_* in mobile*/
function userSelectionMenu() {
		if ($(window).width() <= 820) {
			$('.selection-title').on('click', function() {
				$(this).parents('.selection-menu').toggleClass('open-selection-menu');
			});
		}
	}
/*
* open notifications
* type - 'error', 'success' or 'info'
* title - title of notification, string
* text - message of notification, string
*/
function notification(type, title, text) {
		$('.notification').removeClass('success');
		$('.notification').removeClass('error');
		$('.notification').removeClass('info');
		$('.notification').addClass('open-notification');
		$('.notification').addClass(type);
		$('.notification-title').text(title);
		$('.notification-content').text(text);
		$('.notification .btn-close').on('click', function() {
			closeNotification(type);
		});
		$(document).on('keyup', function() {
			if (($('.notification').hasClass('open-notification')) && (event.keyCode == '27')) {
				closeNotification(type);
			}
		});
		if (type != 'error') {
			setTimeout(function() {
				closeNotification(type);
			}, 4000);
		}
	}
	/*

	* close notification

	* type - 'error', 'success' or 'info'

	*/
function closeNotification(type) {
		$('.notification').removeClass('open-notification');
		setTimeout(function() {
			$('.notification').removeClass(type);
			$('.notification-title').text('');
			$('.notification-content').text('');
		}, 500);
	}
	/*
	 * 'create menu' becomes small in mobile when scrolling down
	 * and becomes normal when scrolling up
	 */
function createMenuMob() {
	if ($(window).width() <= 1200) {
		var lastScroll = 0;
		$(window).on('scroll', function() {
			var st = $(this).scrollTop();
			if (st > lastScroll) {
				$('.mob-menu-create').addClass('sm-create');
			} else {
				if ($('.mob-menu-create').hasClass('sm-create')) {
					$('.mob-menu-create').removeClass('sm-create');
				}
			}
			lastScroll = st;
		});
		$('.menu-create .btn-close').on('click', function() {
			$('.menu-create').removeClass('menu-create-open');
		});
	}
}


/*
* open modal with Search
*/
function openSearchModal(){
	$('.top-search').on('click', function(e){
		e.preventDefault();
		$('body').addClass('body-overflow');
		$('.modal-search').addClass('open-modal');
		$('.modal-search .btn-close').on('click', function(){
			$('body').removeClass('body-overflow');
			$('.modal-search').removeClass('open-modal');
		});
		$(document).on('keyup', function(event){
			if($('.modal-search').hasClass('open-modal') && (event.keyCode == '27')){
				$('.modal-search').removeClass('open-modal');
			}
		});
	});
	
}


/*
*  close previous modal and open next
*  $closeModal - modal, that must be closed
*  $openModal - modal, that must be opened
*/
function closeOpenNextModal($closeModal, $openModal){
	closeModal($closeModal);
	openModal($openModal);
}