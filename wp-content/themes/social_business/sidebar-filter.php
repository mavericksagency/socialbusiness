<?php global $type, $service, $geo, $inUa, $type_enterprises, $beneficiaries, $type_event, $topics, $map, $type_knowledge_base, $type_resourse; ?>
<form class="filter-settings" id="filterForm">
	<div class="filters-header"> 
		<div class="filters-icon">
			<svg class="icon">
				<use xlink:href="#controls">
			</svg>
		</div>
		<h2 class="filters-title">
			Параметри пошуку
		</h2>
		<button type="button" class="btn-close filters-close">
			<svg class="icon">
				<use xlink:href="#close">
			</svg>
		</button>
	</div>
	<div class="filters-panel">
		<?php 
		if(isset($map)){ 
			echo '<div class="filter-block filter-violet">
			<h3 class="filter-type">
				За виглядом
			</h3>';
			if(isset($_GET['map']) && $_GET['map'] == 1){
				echo '<a class="btn-transparent btn-map-list" href="/enterprises/?map=0">
							<svg class="icon">
								<use xlink:href="#noteList">
							</svg>
							<span class="text">Перелік</span>
						</a>
						<input type="hidden" name="map" value="1"/>';
			}else{
				echo '<a class="btn-transparent btn-map-list" href="/enterprises/?map=1">
							<svg class="icon">
								<use xlink:href="#mapList">
							</svg>
							<span class="text">Карта</span>
						</a>';
			}
			echo '</div>';
		} ?>
		<?php if($type_resourse){ 
			if(isset($_GET['type_resourse'])){
				$type_resourse = explode(",", $_GET['$type_resourse']);
			}
			$types_resourse				= get_field('type_resourse');
			global $type_resourse_field;
			$type_resourse_field		= get_field_object('field_5b74070a20a68');
			?>
			<div class="filter-block filter-type">
				<h3 class="filter-type">
					За типом "Ресурсу"
				</h3>
				<button type="button" class="btn-tag-add">
					<span class="tag-add__name">
						Оберіть тип
					</span>
					<span class="tag-add__control">
						<svg class="icon">
							<use xlink:href="#plus">
						</svg>
					</span>
				</button>
				<div class="modal-filter">
					<div class="modal-inner">
						<h2 class="modal-title">
							Оберіть тип
						</h2>
						<button type="button" class="modal-close close-modal-filter">
							<svg class="icon">
								<use xlink:href="#close">
							</svg>
						</button>
						<div class="tag-block tag-filter">
							<?php
								foreach($type_resourse_field['choices'] as $key=>$term){
									?>
									<label class="tag <?= $term->taxonomy ?>">
										<input type="checkbox" name="type_resourse" <?php if(isset($_GET['beneficiaries_ids']) && in_array($key, $type_resourse)){ echo 'checked'; }else if($types_resourse && in_array($key, $types_resourse)){  echo 'checked'; } ?> value="<?= $key; ?>">
										<span class="tag-style">
											<svg class="icon tag-type">
												<use xlink:href="#filter">
											</svg>
											<svg class="icon tag-close">
												<use xlink:href="#close">
											</svg>
											<span class="text"><?= $term; ?></span>
										</span>
									</label>
									<?php
								}
							?>						
						</div>
						<div class="modal-footer-buttons">
							<button type="button" class="btn btn-remove">Очистити</button>
							<button type="button" class="btn-raised close-modal-filter">Застосувати</button>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php if($type_knowledge_base){ 
			if(isset($_GET['type_knowledge_base'])){
				$type_knowledge_base = explode(",", $_GET['$type_knowledge_base']);
			}
			$types_knowledge_base			= get_field('type_knowledge_base');
			global $type_knowledge_base_field;
			$type_knowledge_base_field		= get_field_object('field_5bc5dad95b031');
			?>
			<div class="filter-block filter-type">
				<h3 class="filter-type">
					За типом "Бази знань"
				</h3>
				<button type="button" class="btn-tag-add">
					<span class="tag-add__name">
						Оберіть тип
					</span>
					<span class="tag-add__control">
						<svg class="icon">
							<use xlink:href="#plus">
						</svg>
					</span>
				</button>
				<div class="modal-filter">
					<div class="modal-inner">
						<h2 class="modal-title">
							Оберіть тип
						</h2>
						<button type="button" class="modal-close close-modal-filter">
							<svg class="icon">
								<use xlink:href="#close">
							</svg>
						</button>
						<div class="tag-block tag-filter">
							<?php
								foreach($type_knowledge_base_field['choices'] as $key=>$term){
									?>
									<label class="tag <?= $term->taxonomy ?>">
										<input type="checkbox" name="type_knowledge_base" <?php if(isset($_GET['beneficiaries_ids']) && in_array($key, $type_knowledge_base)){ echo 'checked'; }else if($types_knowledge_base && in_array($key, $types_knowledge_base)){ echo 'checked'; } ?> value="<?= $key; ?>">
										<span class="tag-style">
											<svg class="icon tag-type">
												<use xlink:href="#filter">
											</svg>
											<svg class="icon tag-close">
												<use xlink:href="#close">
											</svg>
											<span class="text"><?= $term; ?></span>
										</span>
									</label>
									<?php
								}
							?>						
						</div>
						<div class="modal-footer-buttons">
							<button type="button" class="btn btn-remove">Очистити</button>
							<button type="button" class="btn-raised close-modal-filter">Застосувати</button>
						</div>
					</div>
				</div>
			</div>
		<?php } ?>
		<?php if($geo){ 
			if(isset($_GET['geo_ids'])){
				$geo = explode(",", $_GET['geo_ids']);
			}
			?>
		<div class="filter-block filter-location">
			<h3 class="filter-type">
				За розташуванням
			</h3>
			<button type="button" class="btn-tag-add">
				<span class="tag-add__name">
					Оберіть
				</span>
				<span class="tag-add__control">
					<svg class="icon">
						<use xlink:href="#plus">
					</svg>
				</span>
			</button>
			<div class="modal-filter">
				<div class="modal-inner">
					<h2 class="modal-title">
						Оберіть
					</h2>
					<?php
						$args = array(
							'taxonomy' => 'geo',
							'hide_empty' => true
						);
						$terms = get_terms( $args );
					?>
					<button type="button" class="modal-close close-modal-filter">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<div class="tag-block tag-filter">
						<?php
							foreach($terms as $term){
						?>
						<label class="tag <?= $term->taxonomy ?>">
							<input type="checkbox" name="geo_ids" <?php if(isset($_GET['geo_ids']) && in_array($term->term_id, $geo)){ echo 'checked'; } ?> value="<?= $term->term_id; ?>">
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#map-mark">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text"><?= $term->name; ?></span>
							</span>
						</label>
						<?php
							}
						?>
					</div>
					<div class="modal-footer-buttons">
						<button type="button" class="btn btn-remove">Очистити</button>
						<button type="button" class="btn-raised close-modal-filter">Застосувати</button>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($service){ 
			if(isset($_GET['service_ids'])){
				$service = explode(",", $_GET['service_ids']);
			}
			?>
		<div class="filter-block filter-sphere">
			<h3 class="filter-type">
				За сферою
			</h3>
			<button type="button" class="btn-tag-add">
				<span class="tag-add__name">
					Оберіть сферу
				</span>
				<span class="tag-add__control">
					<svg class="icon">
						<use xlink:href="#plus">
					</svg>
				</span>
			</button>
			<div class="modal-filter">
				<?php
					$args = array(
						'taxonomy' => 'service',
						'hide_empty' => true
					);
					$terms = get_terms( $args );
				?>
				<div class="modal-inner">
					<h2 class="modal-title">
						Оберіть сферу
					</h2>
					<button type="button" class="modal-close close-modal-filter">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<div class="tag-block tag-filter">
						<?php
							foreach($terms as $term){
								?>
								<label class="tag <?= $term->taxonomy ?>">
									<input type="checkbox" name="service_ids" <?php if(isset($_GET['service_ids']) && in_array($term->term_id, $service)){ echo 'checked'; } ?> value="<?= $term->term_id; ?>">
									<span class="tag-style">
										<svg class="icon tag-type">
											<use xlink:href="#sphere">
										</svg>
										<svg class="icon tag-close">
											<use xlink:href="#close">
										</svg>
										<span class="text"><?= $term->name; ?></span>
									</span>
								</label>
								<?php
							}
						?>						
					</div>
					<div class="modal-footer-buttons">
						<button type="button" class="btn btn-remove">Очистити</button>
						<button type="button" class="btn-raised close-modal-filter">Застосувати</button>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($type_enterprises){ 
			if(isset($_GET['type_enterprises_ids'])){
				$type_enterprises = explode(",", $_GET['$type_enterprises_ids']);
			}
			?>
		<div class="filter-block filter-type">
			<h3 class="filter-type">
				За типом підприємства
			</h3>
			<button type="button" class="btn-tag-add">
				<span class="tag-add__name">
					Оберіть тип
				</span>
				<span class="tag-add__control">
					<svg class="icon">
						<use xlink:href="#plus">
					</svg>
				</span>
			</button>
			<div class="modal-filter">
				<?php
					$args = array(
						'taxonomy' => 'type_enterprises',
						'hide_empty' => true
					);
					$terms = get_terms( $args );
				?>
				<div class="modal-inner">
					<h2 class="modal-title">
						Оберіть тип
					</h2>
					<button type="button" class="modal-close close-modal-filter">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<div class="tag-block tag-filter">
						<?php
							foreach($terms as $term){
								?>
								<label class="tag <?= $term->taxonomy ?>">
									<input type="checkbox" name="type_enterprises_ids" <?php if(isset($_GET['type_enterprises_ids']) && in_array($term->term_id, $type_enterprises)){ echo 'checked'; } ?> value="<?= $term->term_id; ?>">
									<span class="tag-style">
										<svg class="icon tag-type">
											<use xlink:href="#filter">
										</svg>
										<svg class="icon tag-close">
											<use xlink:href="#close">
										</svg>
										<span class="text"><?= $term->name; ?></span>
									</span>
								</label>
								<?php
							}
						?>						
					</div>
					<div class="modal-footer-buttons">
						<button type="button" class="btn btn-remove">Очистити</button>
						<button type="button" class="btn-raised close-modal-filter">Застосувати</button>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($beneficiaries){ 
			if(isset($_GET['beneficiaries_ids'])){
				$beneficiaries = explode(",", $_GET['beneficiaries_ids']);
			}
			?>
		<div class="filter-block filter-type">
			<h3 class="filter-type">
				За бенефіціалом
			</h3>
			<button type="button" class="btn-tag-add">
				<span class="tag-add__name">
					Оберіть бенефіціал
				</span>
				<span class="tag-add__control">
					<svg class="icon">
						<use xlink:href="#plus">
					</svg>
				</span>
			</button>
			<div class="modal-filter">
				<?php
					$args = array(
						'taxonomy' => 'beneficiaries',
						'hide_empty' => true
					);
					$terms = get_terms( $args );
				?>
				<div class="modal-inner">
					<h2 class="modal-title">
						Оберіть бенефіціал
					</h2>
					<button type="button" class="modal-close close-modal-filter">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<div class="tag-block tag-filter">
						<?php
							foreach($terms as $term){
								?>
								<label class="tag <?= $term->taxonomy ?>">
									<input type="checkbox" name="beneficiaries_ids" <?php if(isset($_GET['beneficiaries_ids']) && in_array($term->term_id, $beneficiaries)){ echo 'checked'; } ?> value="<?= $term->term_id; ?>">
									<span class="tag-style">
										<svg class="icon tag-type">
											<use xlink:href="#filter">
										</svg>
										<svg class="icon tag-close">
											<use xlink:href="#close">
										</svg>
										<span class="text"><?= $term->name; ?></span>
									</span>
								</label>
								<?php
							}
						?>						
					</div>
					<div class="modal-footer-buttons">
						<button type="button" class="btn btn-remove">Очистити</button>
						<button type="button" class="btn-raised close-modal-filter">Застосувати</button>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($type_event){ 
			if(isset($_GET['type_event_ids'])){
				$type_event = explode(",", $_GET['type_event_ids']);
			}
			global $type_event_get;
			$type_event_get			= get_field('type_event');
			?>
		<div class="filter-block filter-type">
			<h3 class="filter-type">
				За типом події
			</h3>
			<button type="button" class="btn-tag-add">
				<span class="tag-add__name">
					Оберіть тип
				</span>
				<span class="tag-add__control">
					<svg class="icon">
						<use xlink:href="#plus">
					</svg>
				</span>
			</button>
			<div class="modal-filter">
				<?php
					$args = array(
						'taxonomy' => 'type_event',
						'hide_empty' => true
					);
					$terms = get_terms( $args );
				?>
				<div class="modal-inner">
					<h2 class="modal-title">
						Оберіть тип
					</h2>
					<button type="button" class="modal-close close-modal-filter">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<div class="tag-block tag-filter">
						<?php
							foreach($terms as $term){
								?>
								<label class="tag <?= $term->taxonomy ?>">
									<input type="checkbox" name="type_event_ids" <?php if(isset($_GET['type_event_ids']) && in_array($term->term_id, $type_event)){ echo 'checked'; }else if($type_event_get && in_array($term->term_id, $type_event_get)){ echo 'checked'; } ?> value="<?= $term->term_id; ?>">
									<span class="tag-style">
										<svg class="icon tag-type">
											<use xlink:href="#filter">
										</svg>
										<svg class="icon tag-close">
											<use xlink:href="#close">
										</svg>
										<span class="text"><?= $term->name; ?></span>
									</span>
								</label>
								<?php
							}
						?>						
					</div>
					<div class="modal-footer-buttons">
						<button type="button" class="btn btn-remove">Очистити</button>
						<button type="button" class="btn-raised close-modal-filter">Застосувати</button>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($topics){ 
			if(isset($_GET['topics_ids'])){
				$topics = explode(",", $_GET['topics_ids']);
			}
			?>
		<div class="filter-block filter-theme">
			<h3 class="filter-type">
				За темою
			</h3>
			<button type="button" class="btn-tag-add">
				<span class="tag-add__name">
					Оберіть тему
				</span>
				<span class="tag-add__control">
					<svg class="icon">
						<use xlink:href="#plus">
					</svg>
				</span>
			</button>
			<div class="modal-filter">
				<?php
					$args = array(
						'taxonomy' => 'topics',
						'hide_empty' => true
					);
					$terms = get_terms( $args );
				?>
				<div class="modal-inner">
					<h2 class="modal-title">
						Оберіть тип
					</h2>
					<button type="button" class="modal-close close-modal-filter">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<div class="tag-block tag-filter">
						<?php
							foreach($terms as $term){
								?>
								<label class="tag <?= $term->taxonomy ?>">
									<input type="checkbox" name="topics_ids" <?php if(isset($_GET['topics_ids']) && in_array($term->term_id, $topics)){ echo 'checked'; } ?> value="<?= $term->term_id; ?>">
									<span class="tag-style">
										<svg class="icon tag-type">
											<use xlink:href="#theme">
										</svg>
										<svg class="icon tag-close">
											<use xlink:href="#close">
										</svg>
										<span class="text"><?= $term->name; ?></span>
									</span>
								</label>
								<?php
							}
						?>						
					</div>
					<div class="modal-footer-buttons">
						<button type="button" class="btn btn-remove">Очистити</button>
						<button type="button" class="btn-raised close-modal-filter">Застосувати</button>
					</div>
				</div>
			</div>
		</div>
		<?php } ?>
		<?php if($inUa){ ?>
		<div class="filter-block last-filter">
			<label class="switcher">
				<input type="checkbox" value="1" <?php if(isset($_GET['socbusinua'])){ echo 'checked'; } ?> name="socbusinua">
				<span class="switcher-style">
					<span class="switcher-icon"></span>
				</span>
				<span class="switcher-text">Тільки публікації Social Business IN UA</span>
			</label>
		</div>
		<?php } ?>
		<button type="button" class="btn filters-close">
			Застосувати фільтри
		</button>
	</div>
	<button type="button" class="btn btn-clean" id="clean-filters">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
		<span class="text">
			Очистити <span class="add-text">фільтри</span>
		</span>
	</button>
</form>