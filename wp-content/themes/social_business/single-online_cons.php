<?php 
    get_header(); 
    the_post();
?>
<main class="main">
    <div class="main-grid grid-mob-column">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <div class="center-column">
            <div class="account-block shadow-block communication-block">
                <div class="account-header">
                    <h1 class="setting-title">
                        Консультація
                    </h1>
                </div>
                <div class="communication-body">
                    <div class="communication-block">
                        <h2 class="communication-type">
                            Запитання
                        </h2>
                        <div class="block shadow-block">
                            <div class="question-header">
                                <h3 class="question-title">
                                    <?php the_title(); ?>
                                </h3>
                            </div>
                            <div class="communication-message">
                                <?php 
                                    //get author id, cunsultant object, avatar
                                    $author_id      = get_field('id_author');
                                    $consultant     = get_field('consultant');
                                    //get avatar author
                                    $avatar = get_field('avatar', 'user_'.$author_id);
                                ?>
                                <a href="<?= get_author_posts_url($author_id ); ?>" class="communication-person">
                                    <div class="person-icon blue bg-color" <?php if($avatar){ echo 'style="background-image: url('.$avatar['url'].')"'; } ?>>
                                        <?php 
                                            if(!$avatar){
                                        ?>
                                        <svg class="icon svg-fill">
                                            <use xlink:href="#person">
                                        </svg>
                                        <?php 
                                            }
                                        ?>
                                    </div>
                                    <span class="person-name"><?= get_the_author_meta('first_name', $author_id). ' '. get_the_author_meta('last_name', $author_id) ?></span>
                                </a>
                                <div class="communication-text">
                                    <?php 
                                        the_content();
                                    ?>
                                </div>
                                <div class="communication-documents">
                                    <?php
                                        $file = get_field('file');
                                        if($file){
                                    ?>
                                    <a href="<?= $file['url']; ?>" target="_blank" class="loaded-file">
                                        <span class="loaded-icon red bg-color">
                                            <svg class="icon svg-fill">
                                                <use xlink:href="#file">
                                                </use>
                                            </svg>
                                        </span>
                                        <span class="loaded-text">
                                            <?= $file['title']; ?>
                                        </span>
                                    </a>
                                    <?php } ?>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php 
                        $args = array(
                            'no_found_rows'       => true,
                            'orderby'             => '',
                            'order'               => 'ASC',
                            'parent'              => '',
                            'post_author__in'     => '',
                            'post_author__not_in' => '',
                            'post_id'             => $post->ID,
                        );
                        $comments = get_comments($args);
                        if($comments){        
                    ?>
                    <div class="communication-block">
                        <h2 class="communication-type">
                            Відповідь
                        </h2>
                        <div class="block block-answer">
                            <?php 
                                $last_author_comment = false;
                                foreach($comments as $comment){ 
                                    if($author_id == $comment->user_id){
                                        $last_author_comment = true;
                                    }else{
                                        $last_author_comment = false;
                                    }
                                    //get avatar
                                    $avatar = get_field('avatar', 'user_'.$comment->user_id);   
                            ?>
                                    <div class="communication-message">
                                        <a href="<?= get_author_posts_url($comment->user_id); ?>" class="communication-person">
                                            <span class="person-icon blue bg-color" <?php if($avatar){ echo 'style="background-image: url('.$avatar['url'].')"'; } ?>>
                                                <?php 
                                                    if(!$avatar){
                                                ?>
                                                <svg class="icon svg-fill">
                                                    <use xlink:href="#person">
                                                </svg>
                                                <?php 
                                                    }
                                                ?>
                                            </span>
                                            <span class="person-name"><?= get_the_author_meta('first_name', $comment->user_id). ' '. get_the_author_meta('last_name', $comment->user_id) ?></span>
                                        </a>
                                        <div class="communication-text">
                                            <?= wpautop($comment->comment_content); ?>
                                        </div>
                                        <div class="communication-documents">
                                            <?php
                                                $file = get_field('file', 'comment_'.$comment->comment_ID);
                                                if($file){
                                            ?>
                                            <a href="<?= $file['url']; ?>" target="_blank" class="loaded-file">
                                                <span class="loaded-icon red bg-color">
                                                    <svg class="icon svg-fill">
                                                        <use xlink:href="#file">
                                                        </use>
                                                    </svg>
                                                </span>
                                                <span class="loaded-text">
                                                    <?= $file['title']; ?>
                                                </span>
                                            </a>
                                            <?php } ?>
                                        </div>
                                    </div>
                            <?php } ?>
                        </div>
                    </div>
                <?php } 
                //question not done
                if(!get_field('status_question')){ ?>
                    <div class="communication-footer message-footer">
                        <?php 
                            if(!$last_author_comment && $user_id == $author_id){
                        ?>
                        <div class="simple-footer">
                            <button class="btn btn-another btn-not-solved" type="button">Проблема не вирішена</button>
                            <div class="message-button">
                                <button type="submit" class="btn-raised submit btn-solve" id="question-done" data-question="<?= $post->ID ?>">
                                    <span class="save-text">
                                        Відповідь задовільна
                                    </span>
                                </button>
                                <span class="saved-text">
                                    Питання вирішено
                                </span>
                            </div>
                        </div>
                        <div class="not-solved-footer display-none">
                            <div class="add-comment-text">
                                Якщо проблема не була вирішена, ви можете <a class="add-comment" href="#">коментувати відповідь</a>
                            </div>
                            <button class="btn btn-cancel" type="button">Скасувати</button>
                        </div>
                        <?php } ?>
                        <form method="POST" action="#" id="add_comment" class="not-solved-comment <?php if($consultant['ID'] != $user_id){ echo 'display-none'; } ?>">
                            <div class="field">
                                <input type="hidden" name="post_id" value="<?= $post->ID; ?>">
                                <input type="hidden" name="file_id" id="file_attached" value="">
                                <label class="input-label full-width">
                                    <span class="input-desc"></span>
                                    <textarea class="textarea" name="comment" placeholder="Ваший коментарій" required minlength="25" rows="10"></textarea>
                                    <span class="valid-notification"></span>
                                </label>
                            </div>
                            <div class="message-footer">
                            <div class="add-files-block">
									<label class="add-file" id="add_file_answer">
										<input type="file" id="load_file">
										<span class="btn-add-file">
											<svg class="icon">
												<use xlink:href="#paperclip">
											</use></svg>
										</span>
									</label>
									<div class="files-list" id="file-list"></div>
								</div>
                                <div class="comment-buttons">
                                    <div class="message-button">
                                        <button type="submit" class="btn-raised submit">
                                            <span class="save-text">
                                                Надіслати
                                            </span>
                                        </button>
                                        <span class="saved-text">
                                            Надіслано
                                        </span>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                <?php 
                //question done comment form disabled
                }else{
                    ?>
                    <div class="communication-footer message-footer">
                        <div class="simple-footer">
                            <button class="btn btn-another btn-not-solved display-none" type="button">Проблема не вирішена</button>
                            <div class="message-button question-solved">
                                <button type="submit" class="btn-raised submit btn-solve load done">
                                    <span class="save-text">
                                        Відповідь задовільна
                                    </span>
                                </button>
                                <span class="saved-text">
                                    Питання вирішено
                                </span>
                            </div>
                        </div>
                    </div>
                    
                    <?php
                } 
                ?>
                </div>
            </div>
        </div>
    <div class="right-column">
        <?php           
            $service    = get_the_terms($post->ID, 'service')[0];
            $topics     = get_the_terms($post->ID, 'topics')[0];
            $args 		= array(
                'post_type'			=> $post->post_type,
                'posts_per_page'	=> 5,
                'post_status'   	=> 'publish',
                'order'				=> 'DESC',
                'post__not_in'		=> array((int)$post->ID),
                'tax_query'         => array(
                    'relation'      => 'AND'
                )
            ); 
            if($service){
                $args['tax_query'][$service->taxonomy] = array(
                    'taxonomy' => $service->taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $service->term_id
                );
            }
            if($topics){
                $args['tax_query'][$topics->taxonomy] = array(
                    'taxonomy' => $topics->taxonomy,
                    'field'    => 'term_id',
                    'terms'    => $topics->term_id
                );
            }
            $query 		= new WP_Query($args);
            if($query->posts){
        ?>
        <div class="block message-posts more-posts">
                <h2>Інші запитання</h2>
            <?php
                global $post_pop;
                foreach($query->posts as $post_pop){
                    get_template_part('partials/post/post_cons');
                }
            ?>
        </div>
        <?php 
            }
        ?>
    </div>
    
</main>
<?php get_footer();