<?php
//const 
global $user_id, $current_user, $bookmarks;

// echo get_query_var();
// wp_die();

$user_id =  get_current_user_id();
if($user_id){
    $current_user 	= wp_get_current_user();
	$bookmarks		= explode(',', get_field('bookmarks', 'user_'.$user_id));
}else{
    $bookmarks = array();
}

// DISABLE BOTH default WordPress new user notification emails
if ( ! function_exists( 'wp_new_user_notification' ) ) :
    function wp_new_user_notification( $user_id, $deprecated = null, $notify = '' ) {
        return;
    }
endif;

//setup

//includes
include(get_template_directory(). '/includes/front/enqueue.php');
include(get_template_directory(). '/includes/emails_notification.php');
include(get_template_directory(). '/includes/question-hook.php');
include(get_template_directory(). '/includes/ednpoints.php');
include(get_template_directory(). '/includes/setup.php');
include(get_template_directory(). '/includes/additional_func.php');
include(get_template_directory(). '/includes/post_type_and_tax.php');
include(get_template_directory(). '/includes/user_role.php');
include(get_template_directory(). '/includes/walker.php');

//hooks

//scripts and style
add_action('wp_enqueue_scripts', 'sb_theme_styles_and_scripts');
//post type and tax
add_action('init', 'register_post_types');
//themes settings
add_action('after_setup_theme', 'sb_setup_theme');
//remove admin panel
add_filter('show_admin_bar', '__return_false');



