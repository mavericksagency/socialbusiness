<?php 
	global $type, $service, $geo, $inUa, $topics, $user_id, $title_page;
    $title_page = 'Онлайн консультація';

    get_header(); 
	$service 	= true;
	$topics		= true;
	if(isset($_GET['question'])){
		get_template_part('/partials/edit/message');
	}else{
?>
<main class="main">
	<div class="main-grid">
		<div class="left-column">
			<?php get_sidebar(); ?>
		</div>
		<div class="center-column">
			<?php 
				$args 		= array(
					'post_type'		=> 'online_cons',
					'posts_per_page'=> 10,
					'post_status'   => 'publish',
					'order'			=> 'DESC'
				);
				$query		= new WP_Query($args);
				$args['posts_per_page']			= 10;
				$args['post_status']   			= array('publish', 'draft');
				$args['meta_query']				= array(
					'author' 					=> array(
						'key'     				=> 'id_author',
						'value'   				=> $user_id,
						'compare' 				=> '=',
					),	
				);
				$query_my		= new WP_Query($args);
			?>
			<?php get_template_part('partials/search-form'); ?>
			<div class="search-tabs">
				<a href="/online_cons/" class="search-tab-item <?php if(!isset($_GET['my_question'])){ echo 'current-item'; } ?>">
					<span class="text">Всі питання</span>
					<span class="number"><?= $query->found_posts; ?></span>
				</a>
				<?php if($user_id && $query_my->posts){ ?>
				<a href="/online_cons/?my_question" class="search-tab-item <?php if(isset($_GET['my_question'])){ echo 'current-item'; } ?>">
					<span class="text">Мої питання</span>
					<span class="number"><?= $query_my->found_posts; ?></span>
				</a>
				
				<?php } ?>
				<?php if($user_id){ ?>
				<a href="?question" class="btn add-question">
					Задати запитання
				</a>
				<?php } ?>
			</div>
			<div id="content">
			<?php 
				if(!$user_id && isset($_GET['my_question'])){
					get_template_part('partials/no-access');		
				}else{
					if(isset($_GET['my_question'])){
						$query = $query_my;
					}
					if($query->have_posts()){
						while($query->have_posts()){
							$query->the_post();
							get_template_part('partials/post/question-short');
						}
					}else{
						?>
						<div class="clear-block">
							<img class="clear-image" src="<?php bloginfo('template_directory'); ?>/img/empty-list.svg">
							<h2 class="clear-title">Консультації відсутні</h2>
						</div>
						<?php
					}	
				}
			?>
			</div>
			<div class="load" id="load"></div>
            <div class="load-more-sec text-center" <?php if(!$query->have_posts() || $query->max_num_pages <= 1){ echo 'style="display: none;"'; } ?>>
                <button type="button" id="load_more_filter" class="btn">Завантажити ще</button>
            </div>
            <script>
                var post_type 		= 'online_cons';
                var current_page 	= 2; 
				var max_pages 		= '<?= $query->max_num_pages; ?>';
				<?php 
					if(isset($_GET['my_question'])){
						echo 'var author = '.$user_id;
					}
				?>
            </script>
		</div>
		<div class="right-column settings-column">
			<?php if(!(!$user_id && isset($_GET['my_question']))){ get_sidebar('filter'); } ?>
		</div>
	</div>
</main>
<?php 
	}
	get_footer();