<?php 
get_header(); ?>
	<main class="main">
		<div class="main-grid">
			<div class="left-column">
				<?php get_sidebar(); ?>
			</div>
			<div class="center-column">
				<?php 
					$args = array(
						'post_type'			=> array('resources', 'enterprises'),
						'posts_per_page'	=> 20,
						'order'             => 'DESC'
					);
					$query 				= new WP_Query($args);
					if($query->have_posts()){
				?>
				<div class="search-block">
					<h2 class="block-type">
						Сторінки
					</h2>
					<div class="pages-content">
						<div class="pages-slider">
							<?php 
								while($query->have_posts()){
									$query->the_post();
									get_template_part('partials/post/post-small-ent');
								}
							?>
						</div>
						<div class="pages-controls">
							<button class="prev slider-control">
								<svg class="icon">
									<use xlink:href="#back">
								</svg>
							</button>
							<button class="next slider-control">
								<svg class="icon">
									<use xlink:href="#next">
								</svg>
							</button>
						</div>
					</div>
				</div>
				<?php } ?>
				<div class="search-block">
					<h2 class="block-type">
						Публікації
					</h2>
					<div class="panels-grid">
						<?php 
							$args = array(
								'post_type'		    => array('event', 'opportunities', 'news', 'knowledge_base'),
								'posts_per_page'	=> 20,
						        'order'             => 'DESC'
							);
							$query 				= new WP_Query($args);
							if($query->have_posts()){
								while($query->have_posts()){
									$query->the_post();
									get_template_part('partials/post/post-short');
								}
							}else{
								echo '<h1>Завашим запитом нічого не знайдено!</h1>';
							}
						?>
					</div>
				</div>
			</div>
			<?php get_sidebar('cabinet'); ?>
		</div>
	</main>
<?php get_footer(); ?>