var gulp = require('gulp');
var sass = require('gulp-sass');
var browserSync = require('browser-sync');
var autoprefixer = require('gulp-autoprefixer');

gulp.task ('style', function() {
  return gulp.src('scss/*')
    .pipe(sass())
    .pipe(autoprefixer('last 5 versions'))
    .pipe(gulp.dest('css'))
    .pipe(browserSync.stream());
});

gulp.task('server', function() {
    var files = [
          '*.php',
          'css/*',
          'js/*',
          'scss/*',
       ];

    browserSync.init(files, {
        proxy: "sb.loc",
        baseDir: './'
    });
    gulp.watch(["scss/*"], ['style']);
    gulp.watch("*/*.php").on("change", browserSync.reload);
    gulp.watch("js/*").on("change", browserSync.reload);
});

gulp.task('default', ['server']);