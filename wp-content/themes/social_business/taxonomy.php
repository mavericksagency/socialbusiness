<?php 
get_header(); 
//get term
$term_page = get_term_by('slug', get_query_var('term'), get_query_var('taxonomy'));
?>
	<main class="main">
		<div class="main-grid">
			<div class="left-column">
				<?php get_sidebar(); ?>
			</div>
			<div class="center-column">
				<!-- <form action="#" class="search-form">
					<button class="btn-transparent">
						<svg class="icon">
							<use xlink:href="#search">
						</svg>
					</button>
					<label class="input-with-icon main-search">
						<input name="search" type="text" value="<?= $_POST['search'] ?>" placeholder="Що шукаємо?">
					</label>
				</form> 
				<button class="btn-settings">
					<svg class="icon">
						<use xlink:href="#controls">
					</svg>
				</button>
                -->
				<?php 
					$args = array(
						'post_type'		=> array('resources', 'enterprises'),
                        'posts_per_page'=> -1,
                        'post_status'   => 'publish',						
                        'tax_query'     => array(
                            array(
                                'taxonomy' => $term_page->taxonomy,
                                'field'    => 'term_id',
                                'terms'    => $term_page->term_id
                            )
                        )
					);
					$query_ent 				= new WP_Query($args);
					if($query_ent->have_posts()){
				?>
				<div class="search-block">
					<h2 class="block-type">
						Сторінки
					</h2>
					<div class="pages-content">
						<div class="pages-slider">
							<?php 
								while($query_ent->have_posts()){
									$query_ent->the_post();
									get_template_part('partials/post/post-small-ent');
								}
							?>
						</div>
						<div class="pages-controls">
							<button class="prev slider-control">
								<svg class="icon">
									<use xlink:href="#back">
								</svg>
							</button>
							<button class="next slider-control">
								<svg class="icon">
									<use xlink:href="#next">
								</svg>
							</button>
						</div>
					</div>
				</div>
                <?php } 
                $args = array(
                    'post_type'		=> array('event', 'opportunities', 'news', 'knowledge_base'),
                    'posts_per_page'=> '10',
                    'post_status'   => 'publish',
                    'tax_query'     => array(
                        array(
                            'taxonomy' => $term_page->taxonomy,
                            'field'    => 'term_id',
                            'terms'    => $term_page->term_id
                        )
                    )
                );
                $query 				= new WP_Query($args);
                ?>
				<div class="search-block">
                    <?php 
                    if($query->have_posts()){
                    ?>
					<h2 class="block-type">
						Публікації
					</h2>
					<div class="panels-grid">
						<?php 
                            while($query->have_posts()){
                                $query->the_post();
                                get_template_part('partials/post/post-short');
                            }
						?>
					</div>
                    <?php 
                    }
                    if(!$query->posts && !$query_ent->posts){
                        echo '<h1>Завашим запитом нічого не знайдено!</h1>';
                    }
                    ?>
				</div>
                
			</div>
			<div class="right-column settings-column">
				<form class="filter-settings" id="filterForm">
					<div class="filters-header">
						<div class="filters-icon">
							<svg class="icon">
								<use xlink:href="#controls">
							</use></svg>
						</div>
						<h2 class="filters-title">
							Параметри пошуку
						</h2>
						<button type="button" class="btn-close filters-close">
							<svg class="icon">
								<use xlink:href="#close">
							</use></svg>
						</button>
					</div>
					<div class="filters-panel">
						<div class="filter-block filter-location">
							<?php 
								global $wp_query;
								$term = $wp_query->get_queried_object();
							?>
							<h3 class="filter-type">
								За <?= getTypeTaxName($term->taxonomy); ?>
							</h3>
							<h3><b><?= $term->name; ?></b></h3>
						</div>
					</div>
				</form>
			</div>
		</div>
	</main>
<?php get_footer();