<?php 
    get_header(); 
    $author         = get_queried_object();
    $author_roles   = $author->roles;
    $author_role    = array_shift($author_roles);
    $email          = get_the_author_meta('email', $author->ID);
?>
<main class="main">
    <div class="main-grid">
    <div class="left-column">
        <?php get_sidebar(); ?>
    </div>
    <div class="double-column">
        <div class="panel-main-lg page-main-panel">
            <div class="item-main">
                <?php 
                    //get avatar author
                    $avatar = get_field('avatar', 'user_'.$author->ID);
				?>
                <div class="item-image blue bg-color" <?php if($avatar){ echo 'style="background-image: url('.$avatar['url'].')"'; } ?>>
                    <?php 
                        if(!$avatar){
					?>
                    <svg class="icon svg-fill">
                        <use xlink:href="#person">
                    </svg>
                    <?php
                        }
					?>	
                </div>
                <div class="item-info">
                    <h1 class="item-title"><?= get_the_author_meta('first_name', $author->ID). ' '. get_the_author_meta('last_name', $author->ID) ?></h1>
                    <span class="item-desc"><?= translate_user_role( $wp_roles->roles[$author_role]['name']); ?></span>
                </div>
            </div>
            <div class="panel-main-buttons">
                <a href="/" class="btn-main-panel btn btn-share">
                    <svg class="icon">
                        <use xlink:href="#share">
                    </svg>
                </a>
                <button class="btn-main-panel btn btn-mark add_bookmark <?php if(in_array($post->ID, $bookmarks, FALSE)){ echo 'added'; } ?>" data-id="<?= $post->ID; ?>">
                    <svg class="icon">
                        <use xlink:href="#star">
                    </svg>
                    <span class="text">До закладок</span>
                </button>
                <a href="/" class="btn-main-panel btn btn-tuning edit-pen">
                    <svg class="icon">
                        <use xlink:href="#pen">
                    </svg>
                </a>
            </div>
        </div>
        <div class="main-grid user-grid">
            <div class="center-column">
                <?php 
                    $contacts = get_field('contacts', 'user_'.$author->ID);
                    
                    ?>
                <div class="info-source shadow-block">
                    <h3 class="info-title">Контакти</h3>
                    <ul class="info-list">
                        <?php if($contacts['country'] && $contacts['city']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#map-mark-stroke">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Адреса</span>
                                <span class="text"><?= $contacts['country'].', '.$contacts['city'] ?></span>
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($contacts['hide_email'] != 1){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#mail">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">E-mail:</span>
                                <a class="info-link" href="mailto:<?= $email; ?>"><?= $email; ?></a>
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($contacts['hide_phone'] != 1 && $contacts['phone']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#telephone">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Телефон:</span>
                                <a class="info-link" href="tel:<?= cleanPhone($contacts['phone']); ?>"><?= $contacts['phone']; ?></a>
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($contacts['hide_website'] != 1 && $contacts['website']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#www">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Веб-сайт:</span>
                                <a class="info-link"  href="<?= $contacts['website']; ?>" target="_blank"><?= cleanWebSiteUrl($contacts['website']); ?></a>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                    <?php 
                        $education = get_field('education', 'user_'.$author->ID);
                        $job = get_field('career', 'user_'.$author->ID);
                        $about_myself = get_field('about_myself', 'user_'.$author->ID);
                        $social_network = get_field('social_network', 'user_'.$author->ID);
                        if($education['university'] || $job['work_place'] || $about_myself){
                        
                        ?>
                    <div class="more">
                        <button class="more-text btn-transparent">
                        <span class="text active">Детальна інформація</span>
                        <span class="text">Приховати детальну інформацію</span>
                        </button>
                    </div>
                    <?php } ?>
                    <div class="more-panel">
                        <?php 
                            //job || education
                            if($job || $education){
                            
                            ?>
                        <h3 class="info-title">Детальна інформація</h3>
                        
                        <?php 
                            //education
                            if($education && $education['university'] && $education['faculty']){
                            
                            ?>
                            <ul class="info-list">
                                <li class="info-list-item">
                                    <span class="info-type">
                                        <svg class="icon">
                                            <use xlink:href="#educ">
                                        </svg>
                                    </span>
                                    <div class="info-content">
                                        <span class="info-text">Освіта</span>
                                        <span class="text"><?= $education['university'] .' '. $education['faculty']?></span>
                                    </div>
                                </li>
                                </ul>
                        <?php } ?>

                        <?php 
                            //job
                            if($job && $job['work_place'] && $job['position']){
                            
                            ?>
                            <ul class="info-list">
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#bag-work">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Місце роботи</span>
                                    <span class="text">
                                    <?= $job['work_place'] .' ,'. $job['position']?>
                                    </span>
                                </div>
                            </li>
                            </ul>
                        <?php } ?>
                        
                        <ul class="info-list">
                            <?php 
                                foreach($social_network as $key_soc => $url){
                                    if(!$url){
                                        continue;
                                    }
                                	?>
                            <li class="info-list-item">
                                <span class="info-type <?= $key_soc; ?>-color bg-color">
                                    <svg class="icon svg-fill">
                                        <use xlink:href="#<?= $key_soc; ?>">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text"><?= $key_soc; ?></span>
                                    <a href="<?= $url; ?>" class="info-link" target="_blank">
                                    Посилання
                                    </a>
                                </div>
                            </li>
                            <?php
                                }
                                
                                ?>	
                        </ul>
                        <?php } ?>
                        <?php 
                            //about my self
                            if($about_myself){
                            
                            ?>
                        <div class="info-article">
                            <span class="info-title">
                            Про себе
                            </span>
                            <div class="info-article-text">
                                <p>
                                    <?= $about_myself; ?>
                                </p>
                            </div>
                        </div>
                        <?php 
                            }
                            
                            ?>
                    </div>
                </div>
                <?php 
                    //get post this user
                    $args = array(
                        'post_type'     => array('event', 'opportunities', 'news', 'knowledge_base', ),
                        'post_per_page' => 10,
                        'order'         => 'DESC',
                        'meta_query' => array(
                            'relation' => 'AND',
                            'public'        => array(
                                'key'       => 'public',
                                'value'     => 'user_'.$author->ID,
                                'compare'   => 'LIKE',
                            )
                        )
                    );

                    $wp_query = new WP_Query($args);
                ?>
                <div class="wall shadow-block">
                    <div class="wall-header">
                        <?php 
                            global $args;
                            $args = array(
                                'post_type'         => array('news', 'event', 'opportunities', 'knowledge_base'),
                                'posts_per_page'    => 10,
                                'order'             => 'DESC',
                                'meta_query'        => array(
                                    'public'        => array(
                                        'key'       => 'public',
                                        'value'     => 'user_'.$author->ID,
                                        'compare'   => 'LIKE',
                                    )
                                )
                            );
                            get_template_part('partials/wall-result');
                        ?>             
                </div>
            </div>
            <div class="right-column">
                <?php
                    //get enterprises user
                    $args = array(
                        'post_type'         => array('enterprises', 'resources'),
                        'post_per_page'     => -1,
                        'order'             => 'DESC',
                        'meta_query'        => array(
                            array(
                                'key'       => 'page_administrators',
                                'value'     => 'user_'.$author->ID.',',
                                'compare'   => 'LIKE',
                            )
                        )
                    );

                    $wp_query_enterprises = new WP_Query($args);
                    if($wp_query_enterprises->have_posts()){
                ?>
                <div class="block siblings-block">
                    <h2 class="block-title">
                        Сторінки
                    </h2>
                    <?php 
                        while($wp_query_enterprises->have_posts()){
                            $wp_query_enterprises->the_post();
                            get_template_part('partials/post/small-enterprice');
                        }
                    ?>
                    
                </div>
                <?php } ?>
                <button class="btn-block share-button" data-url="<?= get_author_posts_url($author->ID) ?>">
                    <svg class="icon">
                        <use xlink:href="#share">
                    </svg>
                    <span class="text">
                        Поширити сторінку
                    </span>
                </button>
                <?php if($author->ID == $user_id){ ?>
                <a href="<?= get_the_permalink(34); ?>" class="btn-block edit-pen">
                    <svg class="icon">
                        <use xlink:href="#pen">
                    </svg>
                    <span class="text">
                        Редагувати
                    </span>
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
</main>
<?php 
    get_footer(); 
?>