<div class="block selection-menu anchors-menu section-block">
	<h2 class="section-title selection-title">
		Розділи
	</h2>
	
	<div class="selection-units">
		<ul class="section-list">
			<li>
				<a class="section-link" href="/">
					<span class="section-icon">
						<svg class="icon">
							<use xlink:href="#star" />
						</svg>
					</span>
					<span class="section-text">
						Мої підприємства
					</span>
				</a>
			</li>
			<li>
				<a class="section-link section-current" href="/">
					<span class="section-icon">
						<svg class="icon">
							<use xlink:href="#person" />
						</svg>
					</span>
					<span class="section-text">
						Мої ресурси
					</span>
				</a>
			</li>
			<li>
				<a class="section-link" href="/">
					<span class="section-icon">
						<svg class="icon">
							<use xlink:href="#buildings" />
						</svg>
					</span>
					<span class="section-text">
						Мої публікації
					</span>
				</a>
			</li>
			<li>
				<a class="section-link" href="/">
					<span class="section-icon">
						<svg class="icon">
							<use xlink:href="#question" />
						</svg>
					</span>
					<span class="section-text">
						Мої питання
					</span>
				</a>
			</li>
		</ul>
	</div>
</div>