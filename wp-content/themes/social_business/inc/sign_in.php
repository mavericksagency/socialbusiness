<div class="sign-modal sign-in" id="sign-in">
	<button class="btn-close close-modal">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
	</button>
	<a href="#" class="sign-logo">
		<svg class="icon logo-white">
			<use xlink:href="#logo">
		</svg>
	</a>
	<div class="sign-modal-inner">
		<section class="sign-slider-section">
			<div class="sign-slider-section-inner">
				<div class="sign-slider">
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Шукайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/search.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Реєструйтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/register.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Додавайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/add.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Навчайтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/study.svg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="sign-form-section">
			<div class="sign-form-section-inner">
				<h1 class="sign-form-title">
					Вхід
				</h1>
				<div class="sign-form-subtitle">
					<span>Ще немає аккаунту?</span>
					<a href="#" class="openRegister">Зареєструйтеся</a>
				</div>
				<form action="#" class="sign-form" id="sign-form">
					<label class="input-label valid">
						<span class="input-desc">Email</span>
						<input type="email" class="input" required name="login" placeholder="Введіть ваш Email">
						<span class="valid-notification"></span>
					</label>
					<label class="input-label valid">
						<span class="input-desc">Пароль</span>
						<input type="password" class="input" required name="password" placeholder="Введіть пароль">
						<span class="valid-notification"></span>
					</label>
					<div class="sign-form-remember">
						<a href="#" class="pass-forgot">Забули пароль?</a>
					</div>
					<div class="sign-form-buttons">
						<button type="submit" class="btn btn-raised btn-go">
							Вхід
						</button>
						<a href="#" class="btn openRegister">
							Реєстрація
						</a>
					</div>
				</form>
				<div class="sign-social">
					<span class="sign-social-title">
						Вхід через
					</span>
					<div class="sign-social-reg">
						<a href="https://socialbusiness.in.ua/wp-login.php?loginSocial=facebook" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="facebook" data-popupwidth="475" data-popupheight="175" class="sign-social-link facebook-color">
							<span class="bg-color sign-social-icon">
								<svg class="icon svg-fill">
									<use xlink:href="#facebook">
								</svg>
							</span>
							<span class="sign-social-name">
								Facebook
							</span>
						</a>
						<a href="https://socialbusiness.in.ua/wp-login.php?loginSocial=google" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="google" data-popupwidth="600" data-popupheight="600" class="sign-social-link google-color">
							<span class="bg-color sign-social-icon">
								<svg class="icon svg-fill">
									<use xlink:href="#google-plus">
								</svg>
							</span>
							<span class="sign-social-name">
								Google+
							</span>
						</a>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>