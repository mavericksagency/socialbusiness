<div class="sign-modal sign-up" id="sign-up">
	<button class="btn-close close-modal">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
	</button>
	<a href="#" class="sign-logo">
		<svg class="icon logo-white">
			<use xlink:href="#logo">
		</svg>
	</a>
	<div class="sign-modal-inner">
		<section class="sign-slider-section">
			<div class="sign-slider-section-inner">
				<div class="sign-slider">
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Шукайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/search.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Реєструйтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/register.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Додавайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/add.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Навчайтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/study.svg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="sign-form-section">
			<div class="sign-form-section-inner">
				<h1 class="sign-form-title">
					Реєстрація
				</h1>
				<div class="sign-form-subtitle">
					<span>Вже маєте аккаунт?</span>
					<a href="#" class="openLogin">Вхід</a>
				</div>
				<form action="#" class="sign-form" id="register-form">
					<label class="input-label valid register-first-step">
						<span class="input-desc">Ваше Імя</span>
						<input type="text" class="input" required name="first_name" placeholder="Введіть ім'я">
						<span class="valid-notification"></span>
					</label>
					<label class="input-label valid register-first-step">
						<span class="input-desc">Ваш прізвище</span>
						<input type="text" class="input" required name="last_name" placeholder="Введіть прізвище">
						<span class="valid-notification"></span>
					</label>
					<label class="input-label valid register-next-step">
						<span class="input-desc">Ваш e-mail</span>
						<input type="email" class="input" required name="email" placeholder="Введіть e-mail">
						<span class="valid-notification"></span>
					</label>
					<label class="input-label valid register-next-step">
						<span class="input-desc">Пароль</span>
						<input type="password" class="input" required id="password" minlength="8" name="password" placeholder="Введіть пароль">
						<span class="valid-notification"></span>
					</label>
					<label class="input-label valid register-next-step">
						<span class="input-desc">Повторити Пароль</span>
						<input type="password" class="input" required id="password_repeat" minlength="8" name="repeat_password" placeholder="Введіть пароль">
						<span class="valid-notification"></span>
					</label>
					<div class="sign-form-buttons">
						<button type="button" class="btn btn-raised btn-register-next-step">
							Продовжити
						</button>
						<button type="submit" class="btn btn-raised btn-go display-none">
							Реєстрація
						</button>
					</div>
				</form>
				<div class="sign-social">
					<span class="sign-social-title">
						Реєстрація через
					</span>
					<div class="sign-social-reg">
						<a href="https://socialbusiness.in.ua/wp-login.php?loginSocial=facebook" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="facebook" data-popupwidth="475" data-popupheight="175" class="sign-social-link facebook-color">
							<span class="bg-color sign-social-icon">
								<svg class="icon svg-fill">
									<use xlink:href="#facebook">
								</svg>
							</span>
							<span class="sign-social-name">
								Facebook
							</span>
						</a>
						<a href="https://socialbusiness.in.ua/wp-login.php?loginSocial=google" data-plugin="nsl" data-action="connect" data-redirect="current" data-provider="google" data-popupwidth="600" data-popupheight="600" class="sign-social-link google-color">
							<span class="bg-color sign-social-icon">
								<svg class="icon svg-fill">
									<use xlink:href="#google-plus">
								</svg>
							</span>
							<span class="sign-social-name">
								Google+
							</span>
						</a>
					</div>
				</div>
			</div>
		</section>
	</div>
</div>