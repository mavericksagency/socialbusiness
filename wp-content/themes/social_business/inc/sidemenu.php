<nav class="side-menu">
	<ul class="side-menu__main">
	<li>
			<a href="/enterprises/">
				<svg class="icon">
					<use xlink:href="#buildings">
				</svg>	
				<span class="side-menu__text">
					Підприємства
				</span>
			</a>
		</li>
		<li>
			<a href="/online_cons/">
				<svg class="icon">
					<use xlink:href="#question">
				</svg>	
				<span class="side-menu__text">
					<?= get_the_title(135); ?>
				</span>
			</a>
		</li>
		<li>
			<a href="/event/">
				<svg class="icon">
					<use xlink:href="#calendar-events">
				</svg>	
				<span class="side-menu__text">
					Події
				</span>
			</a>
		</li>
		<li>
			<a href="/knowledge_base/">
				<svg class="icon">
					<use xlink:href="#education">
				</svg>	
				<span class="side-menu__text">
					База знань
				</span>
			</a>
		</li>
		<li>
			<a href="/resources/">
				<svg class="icon">
					<use xlink:href="#urban-buildings">
				</svg>	
				<span class="side-menu__text">
					Ресурси
				</span>
			</a>
		</li>
		<li>
			<a href="/opportunities/">
				<svg class="icon">
					<use xlink:href="#bag">
				</svg>	
				<span class="side-menu__text">
					Пошук роботи
				</span>
			</a>
		</li>
		<li>
			<a href="/news/">
				<svg class="icon">
					<use xlink:href="#list-block">
				</svg>	
				<span class="side-menu__text">
					Новини
				</span>
			</a>
		</li>
	</ul>
	<div class="menu-create">
		<button class="btn-create__menu">
			<svg class="icon">
				<use xlink:href="#plus">
			</svg>
			<span>Додати</span>
		</button>
		<div class="menu-create-inner">	
			<ul class="menu-create-list">
			<?php $createPage = get_the_permalink(53); ?>
				<li><a href="<?= $createPage; ?>?enterprises">Підприємство</a></li>
				<li><a href="<?= $createPage; ?>?event">Подію</a></li>
				<li><a href="<?= $createPage; ?>?vacancy">Вакансію</a></li>
				<li><a href="<?= $createPage; ?>?news">Новину</a></li>
				<li><a href="<?= get_the_permalink(632); ?>">Ресурс</a></li>			
				<li><a href="<?= $createPage; ?>?knowledge_base">Базу знань</a></li>
			</ul>
		</div>
	</div>
	<?php
		wp_nav_menu(array(
			'theme_location'  	=> 'sub-left-menu',
			'container'			=> false,
			'fallback_cb'		=> false,
			'items_wrap'      => '<ul class="side-menu__secondary">%3$s</ul>',
			'depth'				=> 1
		));
	?>
</nav>