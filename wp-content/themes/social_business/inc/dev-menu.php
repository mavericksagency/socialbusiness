<style>
	.dev-menu{
		position: fixed;
		bottom: 20px;
		right: 20px;
		z-index: 99999999;
		background-color: #000;
		color: #fff;
		font-size: 14px;
		padding: 10px;
		border: none;
	}

	.dev-menu:hover{
		color: #fff;
	}

	.dev-menu-list{
		display: none;
		position: fixed;
		padding: 0;
		bottom: 70px;
		right: 20px;
		z-index: 99999;
		list-style-type: none;
		background-color: #000;
	}

	.open-dev-menu{
		display: block;
	}

	.dev-menu-list li{
		padding: 0;
	}

	.dev-menu-list li a{
		color: #fff;
		padding: 5px 10px;
		font-size: 14px;
		display: block;
	}

	.dev-menu-list li a:hover{
		background-color: #ccc;
	}

</style>



<button id="dev-menu" class="dev-menu">
	Menu
</button>

<ul id="dev-menu-list" class="dev-menu-list">
	<li><a href="../uikit.php">UIKit</a></li>
	<li><a href="../search_community_list.php">Search Community List</a></li>
	<li><a href="../search_content.php">Search Content</a></li>
	<li><a href="../search_source.php">Search Source</a></li>
	<li><a href="../search_questions.php">Search Questions</a></li>
	<li><a href="../user_source.php">User Source</a></li>
	<li><a href="../community_page.php">Community Page</a></li>
	<li><a href="../lector_page.php">Lector Page</a></li>
	<li><a href="../user_page.php">User Page</a></li>
	<li><a href="../post.php">Post</a></li>
	<li><a href="../account_settings.php">Account settings</a></li>
	<li><a href="../community_settings.php">Community settings</a></li>
	<li><a href="../message.php">Message</a></li>
	<li><a href="../message_answer.php">Message Answer</a></li>
	<li><a href="../support.php">Support Page</a></li>
	<li><a href="../support_theme.php">Support Theme</a></li>
</ul>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	$('#dev-menu').on('click', function(){
		$('#dev-menu-list').toggleClass('open-dev-menu');
		$('#dev-menu-list li a').on('click', function(){
			$('#dev-menu-list').toggleClass('open-dev-menu');
		});
	});
</script>