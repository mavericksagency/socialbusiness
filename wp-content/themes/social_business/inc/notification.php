<div class="notification">
	<button class="btn-close">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
	</button>
	<div class="notification-header">
		<span class="notification-icon">
			<svg class="icon info-icon">
				<use xlink:href="#info">
			</svg>
			<svg class="icon success-icon">
				<use xlink:href="#success">
			</svg>
			<svg class="icon error-icon">
				<use xlink:href="#error">
			</svg>
		</span>
		<h3 class="notification-title">Lorem ipsum dolor.</h3>
	</div>
	<div class="notification-body">
		<p class="notification-content">
			Lorem ipsum dolor sit amet, consectetur adipisicing elit. Vel molestias laboriosam quas esse nostrum.
		</p>
	</div>
</div>