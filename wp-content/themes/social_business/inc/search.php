<div class="modal-search">
	<div class="modal-inner">
		<button class="btn-close">
			<svg class="icon">
				<use xlink:href="#close">
			</svg>
		</button>
		<form class="search" method="POST" action="<?= get_the_permalink(550); ?>">
			<button class="btn-search">
				<svg class="icon">
					<use xlink:href="#search">
				</svg>
			</button>
			<input type="text" name="search" class="search-input" placeholder="Знайти">
		</form>
	</div>
</div>