<div class="filter-settings">
	<div class="filters-header">
		<div class="filters-icon">
			<svg class="icon">
				<use xlink:href="#controls">
			</svg>
		</div>
		<h2 class="filters-title">
			Параметри пошуку
		</h2>
		<button class="btn-close filters-close">
			<svg class="icon">
				<use xlink:href="#close">
			</svg>
		</button>
	</div>

	<div class="filters-panel">
		<div class="filter-block">
			<h3 class="filter-type">
				За типом контенту
			</h3>
			<ul class="filter-list">
				<li>Блог</li>
				<li>Відео</li>
				<li>Блог</li>
				<li>Відео</li>
			</ul>
		</div>
		<div class="filter-block filter-violet">
			<h3 class="filter-type">
				За розташуванням
			</h3>
			<button class="btn-tag-add">
				<span class="tag-add__name">
					Додати місто
				</span>
				<span class="tag-add__control">
					<svg class="icon">
						<use xlink:href="#plus">
					</svg>
				</span>
			</button>
			<div class="modal-filter">
				<div class="modal-inner">
					<h2 class="modal-title">
						Оберіть місто
					</h2>
					<button class="modal-close close-modal-filter">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<div class="tag-block tag-filter">
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Київ</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Харків</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Львів</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Одеса</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Lorem</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Lorem ipsum</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Lorem ipsum dolor.</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Lorem ipsum.</span>
							</span>
						</label>
					</div>

					<div class="modal-footer-buttons">
						<button class="btn btn-remove">Очистити</button>
						<button class="btn-raised close-modal-filter">Застосувати</button>
					</div>
				</div>
			</div>
		</div>
		<div class="filter-block">
			<h3 class="filter-type">
				За сферою
			</h3>
			<button class="btn-tag-add">
				<span class="tag-add__name">
					Додати сферу
				</span>
				<span class="tag-add__control">
					<svg class="icon">
						<use xlink:href="#plus">
					</svg>
				</span>
			</button>
			<div class="modal-filter">
				<div class="modal-inner">
					<h2 class="modal-title">
						Оберіть сферу
					</h2>
					<button class="modal-close close-modal-filter">
						<svg class="icon">
							<use xlink:href="#close">
						</svg>
					</button>
					<div class="tag-block tag-filter">
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Освіта</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Господарство</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Культура</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Будівництво</span>
							</span>
						</label>
						<label class="tag">
							<input type="checkbox" checked>
							<span class="tag-style">
								<svg class="icon tag-type">
									<use xlink:href="#flower">
								</svg>
								<svg class="icon tag-close">
									<use xlink:href="#close">
								</svg>
								<span class="text">Туризм</span>
							</span>
						</label>
					</div>

					<div class="modal-footer-buttons">
						<button class="btn btn-remove">Очистити</button>
						<button class="btn-raised close-modal-filter">Застосувати</button>
					</div>
				</div>
			</div>
		</div>
		<div class="filter-block last-filter">
			<label class="switcher">
				<input type="checkbox">
				<span class="switcher-style">
					<span class="switcher-icon"></span>
				</span>
				<span class="switcher-text">Тільки публікації Social Business IN UA</span>
			</label>
		</div>
		<button class="btn filters-close">
			Застосувати фільтри
		</button>
	</div>

	<button class="btn btn-clean" id="clean-filters">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
		<span class="text">
			Очистити <span class="add-text">фільтри</span>
		</span>
	</button>
</div>