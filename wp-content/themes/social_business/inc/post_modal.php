<div class="post-modal">
	<button class="btn-close close-modal">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
	</button>
	<div class="modal-inner">
		<div class="center-column">
			<div class="post-header">
				<div class="post-type-icon violet bg-color br-color">
					<svg class="icon svg-fill">
						<use xlink:href="#star">
					</svg>
				</div>
				<div class="post-type-info">
					<a href="/" class="post-title">
						Mavericks Agency
					</a>
					<span class="post-date">
						5 серпня 2018
					</span>
				</div>
			</div>
			<div class="post-main">
				<div class="post-portrait">
					<img class="post-portrait-inner" src="../img/img.jpg">
				</div>
				<div class="post-article">
					<h2 class="post-title">
						Как завоевывать друзей и оказывать влияние на людей
					</h2>
					<div class="panel-tag">
						<div class="tags">
							<a href="/" class="tag">Київ</a>
							<a href="/" class="tag">Освіта</a>
							<a href="/" class="tag">Обслуговування</a>
							<a href="/" class="tag">Київ</a>
							<a href="/" class="tag">Освіта</a>
							<a href="/" class="tag">Обслуговування</a>
							<a href="/" class="tag">Київ</a>
							<a href="/" class="tag">Освіта</a>
							<a href="/" class="tag">Обслуговування</a>
							<a href="/" class="tag">Київ</a>
							<a href="/" class="tag">Освіта</a>
							<a href="/" class="tag">Обслуговування</a>
						</div>
					</div>


					<!-- event contacts -->
					<div class="post-contacts">
						<div class="contact-info">
							<div class="contact-icon">
								<svg class="icon">
									<use xlink:href="#calendar">
								</svg>
							</div>
							<div class="contact-content">
								<span class="contact-main">20 вересня, 19:30</span>
								<span class="contact-desc">через 1 місяць</span>
							</div>
						</div>
						<div class="contact-info">
							<div class="contact-icon">
								<svg class="icon">
									<use xlink:href="#map-mark">
								</svg>
							</div>
							<div class="contact-content">
								<a href="/" class="contact-main">Бізнес-центр "Доміно"</a>
								<span class="contact-desc">Київ, вул. Нижній Вал, 51</span>
							</div>
						</div>
					</div>

					<!-- user-button -->
					<div class="post-button">
						<a href="/" class="btn-with-icon">
							<svg class="icon">
								<use xlink:href="#download">
							</svg>
							<span class="text">
								Надіслати резюме
							</span>
						</a>
					</div>


					<!-- book button -->
					<div class="post-button">
						<a href="/" class="btn-with-icon">
							<svg class="icon">
								<use xlink:href="#download">
							</svg>
							<span class="text">
								Завантажити PDF
							</span>
						</a>
					</div>

					<!-- user contacts-->
					<div class="post-contacts">
						<div class="contact-info">
							<div class="contact-icon">
								<svg class="icon">
									<use xlink:href="#person">
								</svg>
							</div>
							<div class="contact-content">
								<span class="contact-main">Валентина</span>
								<span class="contact-desc">контакт</span>
							</div>
						</div>
						<div class="contact-info">
							<div class="contact-icon">
								<svg class="icon">
									<use xlink:href="#phone">
								</svg>
							</div>
							<div class="contact-content">
								<a href="tel:+ 38 099 999 84 34" class="contact-main">+ 38 099 999 84 34</a>
								<span class="contact-desc">контактний телефон</span>
							</div>
						</div>
					</div>
					<div class="post-article-text">
						<p>
							У Київ, на запрошення Української соціальної академії, завітали іноземні експерти з соціального підприємництва: Матіас Капс (Німеччина) –– лідер міжнародної організації Starkmacher, яка за 12 років реалізувала понад 20 соціальних проектів, та Петер Фабіан (Хорватія) –– прези дент Асоціації Economy of Communion. Фахівці дали українцям п’ять порад, як крок за кроком перетворити просто ідею соціальної ініціативи на працюючий бізнес.
						</p>
					</div>
				</div>
				<div class="post-footer">
					<div class="post-authors">
						<h4 class="post-authors-title">
							Автори
						</h4>
						<!-- only one author in post -->
						<!-- <div class="authors one-author">
							<a href="/" class="authors-logo">
								<svg class="icon">
									<use xlink:href="#person">
								</svg>
							</a>
							<a href="/" class="author-name">
								<span class="name">Ігор Прохоренко</span>
								<span class="desc">ТОВ "Company name"</span>
							</a>
						</div> -->

						<div class="authors many-authors">
							<div class="authors-logo violet bg-color">
								<svg class="icon svg-fill">
									<use xlink:href="#person">
								</svg>
							</div>
							<div class="author-name">
								<span class="name">Декілька авторів</span>
							</div>
							<button class="authors-open-more btn-down-arrow">
								<svg class="icon">
									<use xlink:href="#down-arrow">
								</svg>
							</button>
							<div class="authors-amount">
								<div class="authors ">
									<a href="/" class="authors-logo violet bg-color">
										<svg class="icon svg-fill">
											<use xlink:href="#person">
										</svg>
									</a>
									<a href="/" class="author-name">
										<span class="name">Ігор Прохоренко</span>
										<span class="desc">ТОВ "Company name"</span>
									</a>
								</div>
								<div class="authors ">
									<a href="/" class="authors-logo violet bg-color">
										<svg class="icon svg-fill">
											<use xlink:href="#person">
										</svg>
									</a>
									<a href="/" class="author-name">
										<span class="name">Ігор Прохоренко</span>
										<span class="desc">ТОВ "Company name"</span>
									</a>
								</div>
							</div>
						</div>
					</div>
					<div class="post-storage">
						<button class="btn-transparent">
							<svg class="icon">
								<use xlink:href="#share">
							</svg>
							<span class="text">
								Поширити
							</span>
						</button>
						<button class="btn-transparent btn-mark">
							<svg class="icon">
								<use xlink:href="#star">
							</svg>
							<span class="text">
								До закладок
							</span>
						</button>
					</div>
				</div>
			</div>
		</div> 
		<div class="right-column">
			<div class="more-posts">
				<h3 class="more-posts-header-title">
					Може бути цікаво
				</h3>
				<div class="more-posts-slider">
					<div class="more-post-item">
						<a href="/" class="more-post-image">
							<img class="img" src="img/img.jpg" alt="Image">
						</a>
						<div class="more-post-info">
							<a href="/" class="more-post-title">
								Як побудувати своє соціальне підприємство з нуля
							</a>
							<div class="more-post-type">
								<span class="type-icon">
									<svg class="icon">
										<use xlink:href="#star">
									</svg>
								</span>
								<span class="type-text">
									Блог
								</span>
							</div>
						</div>
					</div>
					<div class="more-post-item">
						<a href="/" class="more-post-image">
							<img class="img" src="img/img.jpg" alt="Image">
						</a>
						<div class="more-post-info">
							<a href="/" class="more-post-title">
								Як побудувати своє соціальне підприємство з нуля
							</a>
							<div class="more-post-type">
								<span class="type-icon">
									<svg class="icon">
										<use xlink:href="#star">
									</svg>
								</span>
								<span class="type-text">
									Блог
								</span>
							</div>
						</div>
					</div>
					<div class="more-post-item">
						<a href="/" class="more-post-image">
							<img class="img" src="img/img.jpg" alt="Image">
						</a>
						<div class="more-post-info">
							<a href="/" class="more-post-title">
								Як побудувати своє соціальне підприємство з нуля
							</a>
							<div class="more-post-type">
								<span class="type-icon">
									<svg class="icon">
										<use xlink:href="#star">
									</svg>
								</span>
								<span class="type-text">
									Блог
								</span>
							</div>
						</div>
					</div>
					<div class="more-post-item">
						<a href="/" class="more-post-image">
							<img class="img" src="img/img.jpg" alt="Image">
						</a>
						<div class="more-post-info">
							<a href="/" class="more-post-title">
								Як побудувати своє соціальне підприємство з нуля
							</a>
							<div class="more-post-type">
								<span class="type-icon">
									<svg class="icon">
										<use xlink:href="#star">
									</svg>
								</span>
								<span class="type-text">
									Блог
								</span>
							</div>
						</div>
					</div>
					<div class="more-post-item">
						<a href="/" class="more-post-image">
							<img class="img" src="img/img.jpg" alt="Image">
						</a>
						<div class="more-post-info">
							<a href="/" class="more-post-title">
								Як побудувати своє соціальне підприємство з нуля
							</a>
							<div class="more-post-type">
								<span class="type-icon">
									<svg class="icon">
										<use xlink:href="#star">
									</svg>
								</span>
								<span class="type-text">
									Блог
								</span>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>