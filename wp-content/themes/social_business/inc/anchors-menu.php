<div class="block anchors-menu section-block">
	<h2 class="section-title">
		Розділи
	</h2>
	<div class="section-accordeons">
		<div class="accordion">
			<div class="accordion-header">
				<div class="accordion-title">
					<div class="accordion-image">
						<svg class="icon">
							<use xlink:href="#person">
						</svg>
					</div>
					<span class="accordion-title__text">Загальні</span>
				</div>
				
				<div class="accordion-control">
					<svg class="icon plus">
						<use xlink:href="#plus">
					</use></svg>
					<svg class="icon minus">
						<use xlink:href="#minus">
					</use></svg>
				</div>
			</div>
			<div class="accordion-inner">
				<ul>
					<li><a href="/">Ім'я</a></li>
					<li><a href="/">Прізвище</a></li>
					<li><a href="/">По-батькові</a></li>
					<li><a href="/">Дата народження</a></li>
				</ul>
			</div>
		</div>
		<div class="accordion">
			<div class="accordion-header">
				<div class="accordion-title">
					<div class="accordion-image">
						<svg class="icon">
							<use xlink:href="#person">
						</svg>
					</div>
					<span class="accordion-title__text">Контакти</span>
				</div>
				
				<div class="accordion-control">
					<svg class="icon plus">
						<use xlink:href="#plus">
					</use></svg>
					<svg class="icon minus">
						<use xlink:href="#minus">
					</use></svg>
				</div>
			</div>
			<div class="accordion-inner">
				<ul>
					<li><a href="">Країна</a></li>
					<li><a href="/">Місто</a></li>
					<li><a href="/">Телефон</a></li>
					<li><a href="/">Електронна пошта</a></li>
					<li><a href="/">Веб-сайт</a></li>
					<li><a href="/">Соціальні мережі</a></li>
				</ul>
			</div>
		</div>
		<div class="accordion">
			<div class="accordion-header">
				<div class="accordion-title">
					<div class="accordion-image">
						<svg class="icon">
							<use xlink:href="#person">
						</svg>
					</div>
					<span class="accordion-title__text">Освіта</span>
				</div>
				
				<div class="accordion-control">
					<svg class="icon plus">
						<use xlink:href="#plus">
					</use></svg>
					<svg class="icon minus">
						<use xlink:href="#minus">
					</use></svg>
				</div>
			</div>
			<div class="accordion-inner">
				<ul>
					<li><a href="/">Країна</a></li>
					<li><a href="/">Місто</a></li>
					<li><a href="/">ВНЗ</a></li>
					<li><a href="/">Факультет</a></li>
					<li><a href="/">Форма навчання</a></li>
					<li><a href="/">Дата випуску</a></li>		
				</ul>
			</div>
		</div>
		<div class="accordion">
			<div class="accordion-header">
				<div class="accordion-title">
					<div class="accordion-image">
						<svg class="icon">
							<use xlink:href="#person">
						</svg>
					</div>
					<span class="accordion-title__text">Загальні</span>
				</div>
				
				<div class="accordion-control">
					<svg class="icon plus">
						<use xlink:href="#plus">
					</use></svg>
					<svg class="icon minus">
						<use xlink:href="#minus">
					</use></svg>
				</div>
			</div>
			<div class="accordion-inner">
				<ul>
					<li><a href="/">Ім'я</a></li>
					<li><a href="/">Прізвище</a></li>
					<li><a href="/">По-батькові</a></li>
					<li><a href="/">Дата народження</a></li>
				</ul>
			</div>
		</div>
		<div class="accordion">
			<div class="accordion-header">
				<div class="accordion-title">
					<div class="accordion-image">
						<svg class="icon">
							<use xlink:href="#person">
						</svg>
					</div>
					<span class="accordion-title__text">Загальні</span>
				</div>
				
				<div class="accordion-control">
					<svg class="icon plus">
						<use xlink:href="#plus">
					</use></svg>
					<svg class="icon minus">
						<use xlink:href="#minus">
					</use></svg>
				</div>
			</div>
			<div class="accordion-inner">
				<ul>
					<li><a href="/">Ім'я</a></li>
					<li><a href="/">Прізвище</a></li>
					<li><a href="/">По-батькові</a></li>
					<li><a href="/">Дата народження</a></li>
				</ul>
			</div>
		</div>
	</div>
</div>