<?php //reset password ?>
<div class="sign-modal pass-recovery pass-recovery-key" id="reset-pass-modal">
	<button class="btn-close close-modal">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
	</button>
	<a href="#" class="sign-logo">
		<svg class="icon logo-white">
			<use xlink:href="#logo">
		</svg>
	</a>
	<div class="sign-modal-inner">
		<section class="sign-slider-section">
			<div class="sign-slider-section-inner">
				<div class="sign-slider">
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Шукайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/search.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Реєструйтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/register.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Додавайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/add.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Навчайтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/study.svg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="sign-form-section">
			<div class="sign-form-section-inner">
				<h1 class="sign-form-title">
					Відновлення паролю
				</h1>
				<form action="#" class="sign-form" id="reset-password">
					<label class="input-label valid">
						<span class="input-desc">E-mail</span>
						<input type="email" class="input" name="email" required placeholder="Введіть email">
						<span class="valid-notification"></span>
					</label>
					<div class="sign-form-buttons">
						<button type="submit" class="btn btn-raised btn-go">
							Продовжити
						</button>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
<?php //check reset key ?>
<div class="sign-modal pass-recovery pass-recovery-mail" id="check-key-modal">
	<button class="btn-close close-modal">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
	</button>
	<a href="/" class="sign-logo">
		<svg class="icon logo-white">
			<use xlink:href="#logo">
		</svg>
	</a>
	<div class="sign-modal-inner">
		<section class="sign-slider-section">
			<div class="sign-slider-section-inner">
				
				<div class="sign-slider">
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Шукайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/search.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Реєструйтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/register.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Додавайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/add.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Навчайтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/study.svg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="sign-form-section">
			<div class="sign-form-section-inner">
				<h1 class="sign-form-title">
					Відновлення паролю
				</h1>
				<form action="#" class="sign-form" id="check-key">
					<label class="input-label valid">
						<span class="input-desc">Ключ для відновлення паролю</span>
						<input type="text" class="input" <?php if($_GET['key_reset']){ echo 'value="'.$_GET['key_reset'].'"'; } ?> name="key" required placeholder="Введіть ключ">
						<span class="valid-notification"></span>
					</label>
					<input type="hidden" class="input" name="login" <?php if($_GET['login']){ echo 'value="'.$_GET['login'].'"'; } ?> required id="input-login">
					<div class="sign-form-buttons">
						<button type="submit" class="btn btn-raised btn-go btn-recov-go">
							Продовжити
						</button>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>
<?php //new password ?>
<div class="sign-modal pass-recovery pass-recovery-newpass" id="new-password-modal">
	<button class="btn-close close-modal">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
	</button>
	<a href="#" class="sign-logo">
		<svg class="icon logo-white">
			<use xlink:href="#logo">
		</svg>
	</a>
	<div class="sign-modal-inner">
		<section class="sign-slider-section">
			<div class="sign-slider-section-inner">
				
				<div class="sign-slider">
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Шукайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/search.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Реєструйтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/register.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Додавайте
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/add.svg" alt="">
						</div>
					</div>
					<div class="sign-slider-item">
						<h2 class="slider-title">
							Навчайтеся
						</h2>
						<div class="slider-image">
							<img class="img" src="<?php bloginfo('template_directory'); ?>/img/study.svg" alt="">
						</div>
					</div>
				</div>
			</div>
		</section>
		<section class="sign-form-section">
			<div class="sign-form-section-inner">
				<h1 class="sign-form-title">
					Відновлення паролю
				</h1>
				<form action="#" class="sign-form" id="new-password">
					<label class="input-label valid">
						<span class="input-desc">Новий пароль</span>
						<input type="password" name="password" minLength="8" required class="input"  placeholder="Введіть новий пароль">
						<span class="valid-notification"></span>
					</label>
					<input type="hidden" name="user_id" id="user_id">
					<div class="sign-form-buttons">
						<button type="submit" class="btn btn-raised btn-go">
							Продовжити
						</button>
					</div>
				</form>
			</div>
		</section>
	</div>
</div>