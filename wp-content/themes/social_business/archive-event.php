<?php 
    global $service, $geo, $inUa, $type_event, $topics, $title_page;
    $title_page = 'Події';

    get_header(); 
	$service 	= true;
    $geo 		= true;
    $inUa       = true;
    $topics     = true;
    $type_event = true;
?>
<main class="main">
    <div class="main-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <div class="center-column">
            <?php get_template_part('partials/search-form'); ?>
            <div class="panels-grid" id="content">
            <?php 
            $args 		= array(
                    'post_type'		=> 'event',
                    'post_per_page'	=> 10,
                    'post_status'   => 'publish',
                    'order'			=> 'DESC'
            );
            $query		= new WP_Query($args);
				if($query->have_posts()){
					while($query->have_posts()){
						$query->the_post();
						get_template_part('partials/post/post-short');
					}
				}else{
					?>
					<div class="clear-block">
						<img class="clear-image" src="<?php bloginfo('template_directory'); ?>/img/no-result.svg">
						<h2 class="clear-title">На жаль, активних подій немає</h2>
					</div>
					<?php
				}				
			?>
            </div>
            <div class="load" id="load"></div>
            <div class="load-more-sec text-center" <?php if(!$query->have_posts() || $query->max_num_pages == 1){ echo 'style="display: none;"'; } ?>>
                <button type="button" id="load_more_filter" class="btn">Завантажити ще</button>
            </div>
            <script>
                var post_type = '<?= $post->post_type; ?>';
                var current_page = 2; 
                var max_pages = '<?= $query->max_num_pages; ?>';
            </script>
        </div>
        <div class="right-column settings-column">
            <?php get_sidebar('filter') ?>
        </div>
    </div>
</main>
<?php get_footer(); ?>