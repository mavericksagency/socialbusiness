<?php 
    
    global $service, $geo, $type_enterprises, $beneficiaries, $map, $title_page;
    $title_page = 'Підприємства';

    get_header(); 

	$service 	        = true;
    $geo 		        = true;
    $beneficiaries      = true;
    $type_enterprises   = true;
    $map                = true;
?>
<main class="main">
    <div class="main-grid">
        <div class="left-column">
            <?php get_sidebar(); ?>
        </div>
        <div class="center-column">
            <?php get_template_part('partials/search-form'); ?>
            <?php if($_GET['map'] == 0){ ?>
            <div id="content">
                <?php 
                $args 		= array(
                        'post_type'		=> 'enterprises',
                        'posts_per_page'=> 10,
                        'post_status'   => 'publish',
                        'order'			=> 'DESC'
                );
                $query		= new WP_Query($args);
                    if($query->have_posts()){
                        while($query->have_posts()){
                            $query->the_post();
                            get_template_part('partials/post/post-enterprises');
                        }
                        wp_reset_postdata();
                    }else{
                        ?>
                        <div class="clear-block">
                            <img class="clear-image" src="<?php bloginfo('template_directory'); ?>/img/no-result.svg">
                            <h2 class="clear-title">Немає активних підприємств</h2>
                        </div>
                        <?php
                    }
                ?>
            </div>
            <div class="load" id="load"></div>
            <div class="load-more-sec text-center" <?php if(!$query->have_posts() || $query->max_num_pages == 1){ echo 'style="display: none;"'; } ?>>
                <button type="button" id="load_more_filter" class="btn">Завантажити ще</button>
            </div>
            <?php }else{
                echo '<div class="map-enterprises" id="map_enterprises"></div>';
            } ?>
            <script>
                var post_type = '<?= $post->post_type; ?>';
                var current_page = 2; 
                var max_pages = '<?= $query->max_num_pages; ?>';
            </script>
        </div>
        <div class="right-column settings-column">
            <?php get_sidebar('filter') ?>
        </div>
    </div>
</main>
<?php get_footer(); ?>