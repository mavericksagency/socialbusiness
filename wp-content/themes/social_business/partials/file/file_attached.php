<?php 
    global $file_ID, $file_title;
?>
<div class="loaded-file">
    <span class="loaded-icon red bg-color">
        <svg class="icon svg-fill">
            <use xlink:href="#file">
        </svg>
    </span>
    <span class="loaded-text">
        <?= $file_title; ?>
    </span>
    <button class="btn-delete" type="button" data-id="<?= $file_ID; ?>">
        <svg class="icon">
            <use xlink:href="#close">
        </svg>
    </button>
</div>