<?php 
    global $user_id, $post;
?>
<div class="field">
    <span class="field-title">
        Опублікувати від
    </span>
    <?php 
        if($_GET['post_id']){
            $public = get_field('public', $_GET['post_id']);
        }
        $optionsAuthor = getOptionsAuthor($user_id);
    ?>
    <div class="field-group half-width">
        <?php 
            echo '<select name="public">
                <option value="user_'.$user_id.'">'.get_the_author_meta('first_name', $user_id).' '.get_the_author_meta('last_name', $user_id).'</option>';
                if($optionsAuthor){
                    foreach($optionsAuthor as $author){
                        ?>
                        <option <?php if($_GET['post_id'] && $public && $public == $author->post_type.'_'.$author->ID){ echo 'selected="selected"'; } ?> value="<?= $author->post_type.'_'.$author->ID ?>"><?= $author->post_title ?></option>
                        <?php
                    }
                }
            echo '</select>';
        ?>             
    </div>
</div>