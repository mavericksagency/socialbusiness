<div class="center-column">
    <div class="double-column">
        <div class="clear-block">
        	<img class="clear-image" src="<?php bloginfo('template_directory'); ?>/img/empty.svg">
            <h2 class="clear-title">У вас немає прав доступу!</h2>
            <br>
            <h3 class="clear-subtitle clear-text"><a href="#" class="url_register">Зареєструйтеся</a> або <a href="#" class="login_url">авторизуйтеся</a></h3>
        </div>
    </div>
</div>