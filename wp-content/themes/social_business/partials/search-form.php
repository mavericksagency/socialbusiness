<form action="#" class="search-form" id="search_form">
    <button class="btn-transparent">
        <svg class="icon">
            <use xlink:href="#search">
        </svg>
    </button>
    <label class="input-with-icon main-search">
    <input name="search" type="text" placeholder="Введіть назву підприємства">
    </label>
</form>
<button class="btn-settings">
    <svg class="icon">
        <use xlink:href="#controls">
    </svg>
</button>