<?php 
    global $user_id, $current_user;
?>
<div class="field">
    <span class="field-title">
        Опоблікувати від
    </span>
    <?php 
        $optionsAuthor = getOptionsAuthor($user_id);
    ?>
    <div class="field-group half-width">
        <?php 
            if($type_event){
                echo '<select name="topics">
                    <option value="'.$user_id.'">'.get_the_author_meta('first_name', $user_id).' '.get_the_author_meta('last_name', $user_id).'</option>';
                    if($optionsAuthor){
                        foreach($optionsAuthor as $author){
                            echo '<option value="'.$author->post_type.'_'.$author->ID.'">'.$author->post_title.'</option>';
                        }
                    }
                echo '</select>';
            }
        ?>             
    </div>
</div>