<?php 
    global $avatar, $author_id;
?>
<div class="question-panel-author">
    <div class="author-image blue bg-color" <?php if($avatar){ echo 'style="background-image: url('.$avatar['url'].')"'; } ?>>
        <?php 
            if(!$avatar){
        ?>
        <svg class="icon svg-fill">
            <use xlink:href="#person">
        </svg>
        <?php
            }
        ?>
    </div>
    <a href="<?= get_author_posts_url($author_id); ?>" class="author-name">
        <?= get_the_author_meta('first_name', $author_id). ' '. get_the_author_meta('last_name', $author_id) ?>
    </a>
</div>