<?php 
    global $user_admin_id, $wp_roles;
    
    $author         = get_user_by('ID', (int)$user_admin_id);
    if($author){
    $avatar         = get_field('avatar', 'user_'.$user_admin_id);
    $author_roles   = $author->roles;
    $author_role    = array_shift($author_roles);
?>
<div class="sibling-item">
    <div class="sibling-image <?php if(!$avatar){ echo 'inner-svg'; } ?>" <?php if($avatar){ echo 'style="background-image: url('.$avatar['url'].')";'; } ?>>
        <?php if(!$avatar){ ?>
        <svg class="icon svg-fill">
            <use xlink:href="#person">
        </svg>
        <?php } ?>
    </div>
    <div class="sibling-info">
        <a href="<?= get_author_posts_url($user_admin_id); ?>" class="sibling-title">
            <?= get_the_author_meta('first_name', (int)$user_admin_id). ' '. get_the_author_meta('last_name', (int)$user_admin_id) ?>
        </a>
        <span class="sibling-desc">
            <?= translate_user_role( $wp_roles->roles[$author_role]['name']); ?>
        </span>
    </div>
</div>
<?php } ?>