<?php
global $admins_area, $userPost, $admins, $user_id;
foreach($admins as $user_admin){
    if(!$user_admin){
        continue;
    }
    $user_admin_id = explode('_',$user_admin)[1];
    $userPost = get_user_by('ID', $user_admin_id);
    if($userPost->ID != $user_id){
        get_template_part('partials/user/small');
    }
}