<?php 
    global $post, $author_id, $bookmarks, $my_page;
    $img = get_the_post_thumbnail_url($post, 'news-image');
    $sbInUAPost = get_field('sb_in_ua', $post->ID);
?>
<div class="panel <?= 'class-'.$post->post_type; ?>">
    <div class="panel-header">
        <div class="panel-header-inner">
            <?php 
                $public = get_field('public', $post->ID);
                $array  = explode('_',$public);
                $id     = $array[1];
                if($array[0] == 'user'){
                    $url            = get_author_posts_url($id);
                    $image          = get_field('avatar', 'user_'.$id)['sizes']['logo'];
                    $title          = get_the_author_meta('first_name', $id). ' '. get_the_author_meta('last_name', $id);
                    $type_author    = 'class-user';
                    if(!$image ){
                        $iconPublish = 'person';
                    }
                }else{
                    $url            = get_the_permalink($id);
                    $image          = get_the_post_thumbnail_url($id, 'logo');
                    $title          = get_the_title($id);
                    $sbInUA         = get_field('sb_in_ua', $id);
                    $post_publish   = get_post($id);
                    $type_author    = 'class-'.$post_publish->post_type;

                    if($post->post_type == 'enterprises' || $post->post_type == 'resources'){
                        $type_author    = 'class-user';
                        $authors = getCleanAuthor($post); 
                        if(count($authors) > 1){
                            $iconPublish    = 'persons';
                        }else{
                            $iconPublish    = 'person';
                        }
                    }else{
                        $iconPublish    = getIconPostType($post_publish->ID, $post_publish->post_type);
                    }    
                }
                
                
            ?>
            <a <?php if(!$authors){ echo 'href="'.$url.'"'; } ?> class="panel-logo <?php if(!$image) {echo 'inner-svg ';}?> <?= $type_author; ?>">
                <?php if($image){
                    echo '<img src="'.$image.'" alt=""/>';
                }else{ 
                    $typeIcon   = getIconPostType($post->ID, $post->post_type);
                    ?>
                <svg class="icon svg-fill">
                    <use xlink:href="#<?= $iconPublish; ?>"></use>
                </svg>
                <?php } ?>
            </a>
            <?php 
                
            ?>
            <div class="panel-info">
                <?php 
                    if($post->post_type == 'enterprises' || $post->post_type == 'resources'){
                        $authors = getCleanAuthor($post);

                        if(count($authors) > 1){
                            $title_change = 'Декілька адміністраторів';
                        }else{
                            $title_change = getAuthorAdminName($authors[0]);
                        }
                        ?>
                        <span class="panel-author">
                            <?= $title_change; ?> 
                        </span>
                        <?php
                    }else{
                ?>
                <a href="<?= $url; ?>" class="panel-author">
                    <?= $title; ?> 
                    <?php 
                        if($sbInUA){
                            echo '<span class="sb-mark">SBinUA</span>';
                        }
                    ?>
                </a>
                <span class="panel-date">
                    <?= get_the_date('j F Y', $post->ID); ?>
                </span>
                <?php 
                }
                ?>
            </div>
        </div>
        <?php 
            if($my_page){
        ?>
        <a class="mark edit-pen" href="<?= get_the_permalink(53).'?'.$post->post_type.'&post_id='.$post->ID; ?>">
            <svg class="icon">
                <use xlink:href="#pen">
            </use></svg>
        </a>
        <?php
            }else{
        ?>
        <button class="mark add_bookmark <?php if(in_array($post->ID, $bookmarks, FALSE)){ echo 'added'; } ?>" data-id="<?= $post->ID; ?>">
            <svg class="icon">
                <use xlink:href="#star">
            </use></svg>
        </button>
        <?php } ?>
    </div>
    <?php 
        //
        if($post->post_status == 'draft'){ 
            $url_post = get_the_permalink(53).'?'.$post->post_type.'&post_id='.$post->ID; 
        }else{ 
            $url_post = get_the_permalink($post); 
        }
        global $type;
        $type = getNameTypePost($post->ID, $post->post_type);
        $typeIcon   = getIconPostType($post->ID, $post->post_type);
    ?>
    <a href="<?= $url_post ?>" class="panel-image brown bg-color open-post">
        <?php 
            //marker date
            if($post->post_type == 'event'){
                $date = explode(".", get_field('custom_date', $post->ID));
                ?>
                <span class="date-label">
                    <span class="month"><?= getMonthSlug($date[1]); ?></span>
                    <span class="day"><?= $date[0]; ?></span>
                </span>
                <?php
            }
        ?>
        <?php if(!$img){ ?>
        <svg class="image svg-fill">
            <use xlink:href="#<?= $typeIcon; ?>"></use>
        </svg>
        <?php }else{ ?>
            <img src="<?= $img; ?>" alt="">
        <?php } ?>
    </a>
    <div class="panel-title">
        <a href="<?= $url_post ?>" class="panel-title__text open-post">
            <?php 
                if($sbInUAPost && !$sbInUA){
                    echo '<span class="sb-mark">SBinUA</span>';
                }
            ?>
            <?php the_title(); ?> 
            <?php if($post->post_status == 'draft'){ echo '<div class="tag single-tag yellow">На затвердженні</div>'; } ?>
        </a>
        <!-- <span class="lang">укр</span> -->
    </div>
    <div class="panel-footer">
        <?php 
            if($type){
        ?>
        <span class="post-type">
            <svg class="icon">
                <use xlink:href="#<?= $typeIcon; ?>"></use>
            </svg>
            <span class="type">
                <?= $type; ?>
            </span>
        </span>
        <?php 
            }
        ?>
        <div class="tags">
            <?php 
                show_all_terms($post->ID);
            ?>
        </div>
    </div>
</div>