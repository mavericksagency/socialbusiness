<?php
global $post_pop;
$url 	= get_the_permalink($post_pop->ID);
$title	= get_the_title($post_pop->ID);
?>
<div class="more-post-item">
    <a href="<?= $url; ?>" class="more-post-title">
        <?= $title; ?>
    </a>
</div>