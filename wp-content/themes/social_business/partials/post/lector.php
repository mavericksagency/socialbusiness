<?php 
    global $post, $admins, $type, $user_id;


    $author_id      = explode('_', $admins[0])[1];
    $author         = get_user_by('ID', $author_id);
    $author_roles   = $author->roles;
    $author_role    = array_shift($author_roles);
    $email          = get_the_author_meta('email', $author->ID);
    $sbInUAPost     = get_field('sb_in_ua', $post->ID);
?>
<main class="main">
    <div class="main-grid">
    <div class="left-column">
        <?php get_sidebar(); ?>
    </div>
    <div class="double-column">
        <div class="panel-main-lg page-main-panel <?= 'class-'.$post->post_type; ?>">
            <div class="item-main">
                <?php 
                    //get avatar author
                    $avatar = get_the_post_thumbnail_url();
				?>
                <div class="item-image" <?php if($avatar){ echo 'style="background-image: url('.$avatar.')"'; } ?>>
                    <?php 
                        if(!$avatar){
					?>
                    <svg class="icon svg-fill">
                        <use xlink:href="#person">
                    </svg>
                    <?php
                        }
					?>	
                </div>
                <div class="item-info">
                    <h1 class="item-title">
                        <?php the_title(); ?>
                        <?php 
                            if($sbInUAPost){
                                echo '<span class="sb-mark">SBinUA</span>';
                            }
                        ?>
                    </h1>
                    <span class="item-desc"><?= get_type_resourse($post); ?></span>
                </div>
            </div>
            <div class="panel-main-buttons">
                <button class="btn-main-panel btn btn-mark add_bookmark <?php if($bookmarks && in_array($post->ID, $bookmarks, FALSE)){ echo 'added'; } ?>" data-id="<?= $post->ID; ?>">
                    <svg class="icon">
                        <use xlink:href="#star">
                    </svg>
                    <span class="text">До закладок</span>
                </button>
                <a href="/" class="btn-main-panel btn btn-share">
                    <svg class="icon">
                        <use xlink:href="#share">
                    </svg>
                </a>
                <a href="/" class="btn-main-panel btn btn-tuning edit-pen">
                    <svg class="icon">
                        <use xlink:href="#pen">
                    </svg>
                </a>
            </div>
        </div>
        <div class="main-grid user-grid">
            <div class="center-column">
                <?php 
                    $contacts = get_field('contacts', 'user_'.$author->ID);
                    
                    ?>
                <div class="info-source shadow-block">
                    <h3 class="info-title">Контакти</h3>
                    <ul class="info-list">
                        <?php if($contacts['country'] && $contacts['city']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#map-mark-stroke">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Адреса</span>
                                <a class="info-link" href="/"><?= $contacts['country'].', '.$contacts['city'] ?></a>
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($contacts['hide_email'] != 1 && $email){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#mail">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">E-mail:</span>
                                <a class="info-link" href="mailto:<?= $email; ?>"><?= $email; ?></a>
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($contacts['hide_phone'] != 1 && $contacts['phone']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#telephone">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Телефон:</span>
                                <a class="info-link" href="tel:<?= cleanPhone($contacts['phone']); ?>"><?= $contacts['phone']; ?></a>
                            </div>
                        </li>
                        <?php } ?>
                        <?php if($contacts['hide_website'] != 1 && $contacts['website']){ ?>
                        <li class="info-list-item">
                            <span class="info-type">
                                <svg class="icon">
                                    <use xlink:href="#www">
                                </svg>
                            </span>
                            <div class="info-content">
                                <span class="info-text">Веб-сайт:</span>
                                <a class="info-link"  href="<?= $contacts['website']; ?>" target="_blank"><?= cleanWebSiteUrl($contacts['website']); ?></a>
                            </div>
                        </li>
                        <?php } ?>
                    </ul>
                    <?php 
                        $education = get_field('education', 'user_'.$author->ID);
                        $job = get_field('career', 'user_'.$author->ID);
                        $about_myself = get_field('about_myself', 'user_'.$author->ID);
                        $social_network = get_field('social_network', 'user_'.$author->ID);
                        if($education || $job || $about_myself || $social_network){
                        
                        ?>
                    <div class="more">
                        <button class="more-text btn-transparent">
                        <span class="text active">Детальна інформація</span>
                        <span class="text">Приховати детальну інформацію</span>
                        </button>
                    </div>
                    <?php } ?>
                    <div class="more-panel">
                        <?php 
                            //job || education
                            if($job || $education){
                            
                            ?>
                        <h3 class="info-title">Детальна інформація</h3>
                        <ul class="info-list">
                            <?php 
                                foreach($social_network as $key_soc => $url){
                                    if($url){
                                	?>
                                    <li class="info-list-item">
                                        <span class="info-type <?= $key_soc; ?>-color bg-color">
                                            <svg class="icon svg-fill">
                                                <use xlink:href="#<?= $key_soc; ?>">
                                            </svg>
                                        </span>
                                        <div class="info-content">
                                            <span class="info-text"><?= $key_soc; ?></span>
                                            <a href="<?= $url; ?>" class="info-link" target="_blank">
                                                Посилання
                                            </a>
                                        </div>
                                    </li>
                                    <?php
                                    }
                                }
                                
                                ?>	
                            <?php 
                                //education
                                if($education){
                                
                                ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#education-stroke">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Освіта</span>
                                    <span class="text"><?= $education['university'] .' '. $education['faculty']?></span>
                                </div>
                            </li>
                            <?php } ?>
                            <?php 
                                //job
                                if($job){
                                
                                ?>
                            <li class="info-list-item">
                                <span class="info-type">
                                    <svg class="icon">
                                        <use xlink:href="#bag-stroke">
                                    </svg>
                                </span>
                                <div class="info-content">
                                    <span class="info-text">Місце роботи</span>
                                    <span class="text">
                                    <?= $job['work_place'] .' '. $job['position']?>
                                    </span>
                                </div>
                            </li>
                            <?php } ?>
                        </ul>
                        <?php } ?>
                        <?php 
                            //about my self
                            if($about_myself){
                            
                            ?>
                        <div class="info-article">
                            <span class="info-title">
                            Про себе
                            </span>
                            <div class="info-article-text">
                                <p>
                                    <?= $about_myself; ?>
                                </p>
                            </div>
                        </div>
                        <?php 
                            }
                            
                            ?>
                    </div>
                </div>
                <?php 
                    //get post this user
                    $args = array(
                        'post_type'     => array('event', 'opportunities', 'news', 'knowledge_base', ),
                        'post_per_page' => 10,
                        'order'         => 'DESC',
                        'meta_query' => array(
                            'relation' => 'AND',
                            'public'        => array(
                                'key'       => 'public',
                                'value'     => $post->post_type.'_'.$post->ID,
                                'compare'   => 'LIKE',
                            )
                        )
                    );

                    $wp_query = new WP_Query($args);
                ?>
                <div class="wall shadow-block">
                    <div class="wall-header">
                        <?php 
                            $args = array(
                                'post_type'         => array('news', 'event', 'opportunities', 'knowledge_base'),
                                'posts_per_page'    => 10,
                                'order'             => 'DESC',
                                'meta_query'        => array(
                                    'public'        => array(
                                        'key'       => 'public',
                                        'value'     => $post->post_type.'_'.$post->ID,
                                        'compare'   => 'LIKE',
                                    )
                                )
                            );
                            $query_all              = new WP_Query($args);
                        ?>
                        <button class="wall-all">
                            <span class="text">
                                Всі публікації
                            </span>
                            <span class="number">
                                <?= $query_all->found_posts; ?>
                            </span>
                        </button>
                        <ul class="wall-categories-list">
                            <?php
                                $args['post_type'] = 'event';
                                $query_event       = new WP_Query($args);
                            ?>
                            <li class="wall-category">
                                <a href="?post_type=event" class="wall-category-block">
                                    <span class="category-icon">
                                        <svg class="icon">
                                            <use xlink:href="#calendar-events">
                                        </svg>
                                    </span>
                                    <span class="category-name">Події</span>
                                    <span class="category-number">
                                        <?= $query_event->found_posts; ?>
                                    </span>
                                </a>
                            </li>
                            <?php
                                $args['post_type'] = 'knowledge_base';
                                $query_knowledge_base       = new WP_Query($args);
                            ?>
                            <li class="wall-category">
                                <a href="?post_type=knowledge_base" class="wall-category-block">
                                    <span class="category-icon">
                                        <svg class="icon">
                                            <use xlink:href="#education">
                                        </svg>
                                    </span>
                                    <span class="category-name">База знань</span>
                                    <span class="category-number">
                                        <?= $query_knowledge_base->found_posts; ?>
                                    </span>
                                </a>
                            </li>
                            <?php
                                $args['post_type'] = 'opportunities';
                                $query_opportunities       = new WP_Query($args);
                            ?>
                            <li class="wall-category">
                                <a href="?post_type=opportunities" class="wall-category-block">
                                    <span class="category-icon">
                                        <svg class="icon">
                                            <use xlink:href="#bag">
                                        </svg>
                                    </span>
                                    <span class="category-name">Вакансії</span>
                                    <span class="category-number">
                                        <?= $query_opportunities->found_posts; ?>
                                    </span>
                                </a>
                            </li>
                            <?php
                                $args['post_type'] = 'news';
                                $query_news       = new WP_Query($args);
                            ?>
                            <li class="wall-category">
                                <a href="?post_type=news" class="wall-category-block">
                                    <span class="category-icon">
                                        <svg class="icon">
                                            <use xlink:href="#list-block">
                                        </svg>
                                    </span>
                                    <span class="category-name">Новини</span>
                                    <span class="category-number">
                                        <?= $query_news->found_posts; ?>
                                    </span>
                                </a>
                            </li>
                        </ul>
                    </div>
                    <div class="wall-inner">
                        <?php 
                            if(isset($_GET['post_type'])){
                                if($_GET['post_type'] == 'news'){
                                    $query = $query_news;
                                }else if($_GET['post_type'] == 'event'){
                                    $query = $query_event;
                                }else if($_GET['post_type'] == 'knowledge_base'){
                                    $query = $query_knowledge_base;
                                }else if($_GET['post_type'] == 'opportunities'){
                                    $query = $query_opportunities;
                                }
                            }else{
                                $query = $query_all;
                            }

                            if($query->have_posts()){
                                while($query->have_posts()){
                                    $query->the_post();
                                    get_template_part('partials/post/post-short');
                                }
                                wp_reset_postdata();
                            }else{
                                echo '<h1>Записів не знайдено</h1>';
                            }
                        ?>
                    </div>
                </div>
            </div>
            <div class="right-column">
                <?php
                    //get enterprises user
                    $args = array(
                        'post_type'         => 'enterprises',
                        'post_per_page'     => -1,
                        'order'             => 'DESC',
                        'meta_query'        => array(
                            'relation'      => 'AND',
                            array(
                                'key'       => 'page_administrators',
                                'value'     => $author->ID,
                                'compare'   => 'LIKE'
                            )
                        )
                    );

                    $wp_query_enterprises = new WP_Query($args);
                    if($wp_query_enterprises->have_posts()){
                ?> 
                <div class="block siblings-block">
                    <h2 class="block-title">
                        Підприємства
                    </h2>
                    <?php 
                        while($wp_query_enterprises->have_posts()){
                            $wp_query_enterprises->the_post();
                            get_template_part('partials/post/small-enterprice');
                        }
                    ?>
                    
                </div>
                <?php } ?>
                <button class="btn-block">
                    <svg class="icon">
                        <use xlink:href="#share">
                    </svg>
                    <span class="text">
                        Поширити сторінку
                    </span>
                </button>
                <?php if(in_array('user_'.$user_id, $admins)){ ?>
                <a href="<?= get_the_permalink(53) ?>?<?= $type; ?>&post_id=<?= $post->ID; ?>" class="btn-block edit-pen">
                    <svg class="icon">
                        <use xlink:href="#pen">
                    </svg>
                    <span class="text">
                        Редагувати
                    </span>
                </a>
                <?php } ?>
            </div>
        </div>
    </div>
</main>