<?php 
    global $post, $author_id, $avatar;
    $author_id  = get_field('id_author');
    //get avatar author
    $avatar = get_field('avatar', 'user_'.$author_id);
?>
<div class="block shadow-block panel-main-lg question-panel">
    <div class="question-panel-main">
        <a <?php if($post->post_status != 'draft'){ ?> href="<?php the_permalink(); ?>" <?php } ?> class="question-title">
            <?php the_title(); ?>
            <?php if($post->post_status == 'draft'){ echo '<div class="tag single-tag yellow">На затвердженні</div>'; } ?>
        </a>
        <div class="panel-footer">
            <div class="tags">
                <?php 
                    show_all_terms($post->ID);
                ?>
            </div>
        </div>
    </div>
    <?php 
        get_template_part('partials/author/short');
    ?>
</div>