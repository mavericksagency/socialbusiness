<?php 
    global $post;
    $logo = get_the_post_thumbnail_url();
    $typeIcon   = getIconPostType($post->ID, $post->post_type);
?>
<div class="sibling-item">
    <div class="sibling-image <?php if(!$logo){echo 'inner-svg '; echo $post->post_type;}; ?>" <?php if($logo){ echo 'style="background-image: url('.$logo.')"'; } ?>>
        <?php if(!$logo){ ?>
        <svg class="icon svg-fill">
            <use xlink:href="#<?= $typeIcon; ?>">
        </svg>
        <?php } ?>
    </div>
    <a href="<?php the_permalink(); ?>" class="sibling-info">
        <h3 class="sibling-title">
            <?php the_title(); ?>
        </h3>
        <span class="sibling-desc">
            <?= get_type_resourse($post); ?>
        </span>
    </a>
</div>