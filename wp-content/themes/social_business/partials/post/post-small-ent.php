<?php 
    global $post, $author_id, $bookmarks;
    $img        = get_the_post_thumbnail_url($post, 'news-image');
    $url        = get_the_permalink($post);
    $typeIcon   = getIconPostType($post->ID, $post->post_type);
?>
<div class="page-panel class-<?= $post->post_type; ?>">
    <a href="<?= $url ; ?>" class="page-image <?php if(!$img){ echo 'no-pictures'; } ?>">
    <?php if(!$img){ ?>
        <svg class="image svg-fill">
            <use xlink:href="#<?= $typeIcon; ?>"></use>
        </svg>
        <?php }else{ ?>
            <img src="<?= $img; ?>" alt="">
        <?php } ?>
    </a>
    <div class="page-info">
        <a href="<?= $url ; ?>" class="page-title">
            <?php the_title(); ?>
        </a>
        <!-- <span class="page-type">
            організація
        </span> -->
        <button class="mark-with-icon add_bookmark <?php if(in_array($post->ID, $bookmarks, FALSE)){ echo 'added'; } ?>" data-id="<?= $post->ID; ?>">
            <svg class="icon">
                <use xlink:href="#star">
            </svg>
            <span class="text">До закладок</span>
        </button>
    </div>
</div>