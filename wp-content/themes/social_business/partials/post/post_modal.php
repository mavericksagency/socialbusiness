<?php 
	global $post, $author_id, $bookmarks;
	$iconURL   = getIconPostType($post->ID, $post->post_type);
?>
<div class="post-modal">
	<button class="btn-close close-modal">
		<svg class="icon">
			<use xlink:href="#close">
		</svg>
	</button>
	<div class="modal-inner">
		<div class="center-column post-center">
			<?php 
                $public = get_field('public', $post->ID);
                $array  = explode('_',$public);
                $id     = $array[1];
                
                if($array[0] == 'user'){
                    $url    = get_author_posts_url($id);
                    $image  = get_field('avatar', 'user_'.$id)['sizes']['logo'];
					$title  = get_the_author_meta('first_name', $id). ' '. get_the_author_meta('last_name', $id);
					$iconPublish = 'user';
                }else{
                    $url    = get_the_permalink($id);
                    $image  = get_the_post_thumbnail_url($id, 'logo');
					$title  = get_the_title($id);
					$postPublish	= get_post($id);
					$iconPublish   	= getIconPostType($postPublish->ID, $postPublish->post_type);
                }
            ?>
			<div class="post-header <?= 'class-'.$array[0]; ?>"> 
				<div class="post-type-icon <?php if(!$image){echo 'inner-svg';} ?>">
					<?php if($image){
						echo '<img src="'.$image.'" alt=""/>';
					}else{ ?>
					<svg class="icon svg-fill">
						<use xlink:href="#<?= $iconPublish; ?>"></use>
					</svg>
					<?php } ?>
				</div>
				<div class="post-type-info">
					<a href="<?= $url; ?>" class="post-title">
						<?= $title; ?> 
					</a>
					<span class="post-date">
						<?= get_the_date('j F Y') ?>
					</span>
				</div>
			</div>
			<div class="post-main">
				<div class="post-portrait">
					<img class="post-portrait-inner" src="<?= the_post_thumbnail_url('full'); ?>">
				</div>
				<div class="post-article">
					<h1 class="post-title">
						<?php the_title(); ?>
					</h1>
					<div class="panel-tag">
						<div class="tags">
							<?php 
								show_all_terms($post->ID);
							?>
						</div>
					</div>


					<!-- event contacts -->
					<?php 
						$main_info_event = get_field('main_info');
						$custom_date		= get_field('custom_date');
						if($main_info_event){
					?>
					<div class="post-contacts">
						<?php if($custom_date && $main_info_event['time_start']){ ?>
						<div class="contact-info">
							<div class="contact-icon">
								<svg class="icon">
									<use xlink:href="#calendar">
								</svg>
							</div>
							<div class="contact-content">
								<span class="contact-main"><?= $custom_date; ?>, <?= $main_info_event['time_start']; ?></span>
							</div>
						</div>
						<?php } ?>
						<?php if($main_info_event['time_end']){ ?>
							<div class="contact-info">
								<div class="contact-icon">
									<svg class="icon">
										<use xlink:href="#time">
									</svg>
								</div>
								<div class="contact-content">
									<span class="contact-main"><?= $main_info_event['time_end']; ?></span>
									<span class="contact-desc">час закінчення </span>
								</div>
							</div>
						<?php } ?>
						<?php if($main_info_event['end_register']){ ?>
							<div class="contact-info">
								<div class="contact-icon">
									<svg class="icon">
										<use xlink:href="#register-time">
									</svg>
								</div>
								<div class="contact-content">
									<span class="contact-main"><?= $main_info_event['end_register']; ?></span>
									<span class="contact-desc">реєстрація дійсна до</span>
								</div>
							</div>
						<?php } ?>
						<?php if($main_info_event['url_register']){ ?>
							<div class="contact-info">
								<div class="contact-content">
									<span class="contact-main">
										<a href="<?= $main_info_event['url_register']; ?>" target="_blank" class="btn-with-icon">
											<span class="text">Посилання на реєстрацію </span>
										</a>
									</span>
								</div>
							</div>
						<?php } ?>
						<?php if($main_info_event['price']){ ?>
							<div class="contact-info">
								<div class="contact-icon">
									<svg class="icon">
										<use xlink:href="#price-tag">
									</svg>
								</div>
								<div class="contact-content">
									<span class="contact-main"><?= $main_info_event['price']; ?></span>
									<span class="contact-desc">вартість</span>
								</div>
							</div>
						<?php } ?>
						<?php 
						if($post->post_type == 'event'){
							$types = get_the_terms($post->ID, 'type_event');
							if($types[0]){ ?>
							<div class="contact-info">
								<div class="contact-icon">
									<svg class="icon">
										<use xlink:href="#book">
									</svg>
								</div>
								<div class="contact-content">
									<span class="contact-main"><?= $types[0]->name; ?></span>
									<span class="contact-desc">категорія</span>
								</div>
							</div>
						<?php 
							}
						} ?>
						<?php 
							$place = get_field('place');
							if($place['coordinates'] && $place['address']){
						?>
						<div class="contact-info">
							<div class="contact-icon">
								<svg class="icon">
									<use xlink:href="#map-mark">
								</svg>
							</div>
							<div class="contact-content">
								<a href="https://www.google.com/maps/search/?api=1&query=<?= $place['coordinates']; ?>" class="contact-main"><?= $place['address'] ?></a>
							</div>
						</div>
						<?php } ?>
					</div>
					<?php	
							if($main_info_event['contact']){
								?>
								<div class="post-contacts">
									<div class="contact-info">
										<div class="contact-icon">
											<svg class="icon">
												<use xlink:href="#phone">
											</svg>
										</div>
										<div class="contact-content">
											<a href="tel:<?= $main_info_event['contact']; ?>" class="contact-main"><?= $main_info_event['contact']; ?></a>
											<span class="contact-desc">контактний телефон</span>
										</div>
									</div>
								</div>
								<?php
							}
						}  
					?>
					<?php 
					//book
					$salary = get_field('salary');
					if($salary){ ?>
					<div class="post-contacts">
						<div class="contact-info">
							<div class="contact-icon">
								<svg class="icon">
									<use xlink:href="#price-tag">
								</svg>
							</div>
							<div class="contact-content">
								<span class="contact-main"><?= $salary; ?></span>
								<span class="contact-desc">Заробітна плата</span>
							</div>
						</div>
					</div>
					<?php 
					}
					//book
					$book = get_field('url_book');
					if($book){ ?>
					<div class="post-contacts">
						<div class="contact-info">
							<div class="contact-content">
								<a href="<?= $book; ?>" target="_blank" class="btn-with-icon">
									<svg class="icon">
										<use xlink:href="#<?= $iconURL; ?>">
									</use></svg>
									<span class="text">
										Посилання на книгу
									</span>
								</a>
							</div>
						</div>
					</div>
					<?php 
					}
					//url_course
					$url_course = get_field('url_course');
					if($url_course){ ?>
					<div class="post-contacts">
						<div class="contact-info">
							<div class="contact-content">
								<a href="<?= $url_course; ?>" target="_blank" class="btn-with-icon">
									<svg class="icon">
										<use xlink:href="#<?= $iconURL; ?>">
									</use></svg>
									<span class="text">
										Посилання на курс
									</span>
								</a>
							</div>
						</div>
					</div>
					<?php } 
					//manual
					$manual = get_field('url_manual');
					if($manual){ ?>
					<div class="post-contacts">
						<div class="contact-info">
							<div class="contact-content">
								<a href="<?= $manual; ?>" target="_blank" class="btn-with-icon">
									<svg class="icon">
										<use xlink:href="#<?= $iconURL; ?>">
									</use></svg>
									<span class="text">
										Посилання на посібник
									</span>
								</a>
							</div>
						</div>
					</div>
					<?php } 
					//video
					$video = get_field('url_video');
					if($video){ ?>
					<div class="post-contacts">
						<div class="contact-info">
							<div class="contact-content">
								<a href="<?= $video; ?>" target="_blank" class="btn-with-icon">
									<svg class="icon">
										<use xlink:href="#<?= $iconURL; ?>">
									</use></svg>
									<span class="text">
										Посилання на відео
									</span>
								</a>
							</div>
						</div>
					</div>
					<?php } 
					//material
					$material = get_field('url_material');
					if($material){ ?>
						<div class="post-contacts">
							<div class="contact-info">
								<div class="contact-content">
									<a href="<?= $material; ?>" target="_blank" class="btn-with-icon">
										<svg class="icon">
											<use xlink:href="#<?= $iconURL; ?>">
										</use></svg>
										<span class="text">
											Посилання на матеріал
										</span>
									</a>
								</div>
							</div>
						</div>
					<?php } ?>
					<div class="post-article-text" id="post-content">
						<?php 
							echo convertYoutube(wpautop($post->post_content));
							//echo wpautop($post->post_content);
						?>
					</div>
				</div>
				<div class="post-footer">
					<?php 
						//get authors
						$authors = get_field('authors');
						$authors = json_decode($authors, true);
						$content = '';

						if($authors && $authors != 'null'){
							foreach($authors as $author){
								if(strlen($author[0]) != 0){
									$authorEl = '';
									if($author[1]){
										$authorEl = '<a target="_blank" href="'.$author[1].'">'.$author[0].'</a>';
									}else{
										$authorEl = $author[0];
									}
									$content .= '<li>'.$authorEl.'</li>';
								}
							}
						}
									
								
					?>
					<div class="post-authors <?php if(!$content){ echo 'no-border'; } ?>">
						<?php if($content){ ?>
						<h4 class="post-authors-title">
							Автори
						</h4>
						<div>
							<ul>
								<?= $content; ?>
							</ul>
						</div>
						<?php } ?>
					</div>
					<div class="post-storage">
						<button class="btn-transparent share-button" data-url="<?php the_permalink(); ?>">
							<svg class="icon">
								<use xlink:href="#share">
							</svg>
							<span class="text">
								Поширити
							</span>
						</button>
						<button class="btn-transparent btn-mark add_bookmark <?php if(in_array($post->ID, $bookmarks, FALSE)){ echo 'added'; } ?>" data-id="<?= $post->ID; ?>">
							<svg class="icon">
								<use xlink:href="#star">
							</svg>
							<span class="text">
								До закладок
							</span>
						</button>
					</div>
				</div>
			</div>
		</div> 
		<?php get_sidebar('post'); ?> 
	</div>
</div>