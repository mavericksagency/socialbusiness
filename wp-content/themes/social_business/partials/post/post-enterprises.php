<?php 
    global $post, $author_id, $bookmarks, $my_page;
    $image_url = get_the_post_thumbnail_url($post, 'logo');
    $sbInUAPost = get_field('sb_in_ua', $post->ID);
    $typeIcon   = getIconPostType($post->ID, $post->post_type);
?>
<div class="panel-main-lg  <?= 'class-'.$post->post_type; ?>">
    <div class="item-main">
        <?php if($image_url){ ?>
            <div class="item-image">
                <img src="<?= $image_url ?>" alt="Company" class="img">
            </div>
        <?php 
        }else{ 
            echo '<div class="item-image inner-svg">
						<svg class="icon svg-fill">
							<use xlink:href="#'.$typeIcon.'">
						</use></svg>
					</div>';
        } ?>
        
        <div class="item-info">
            <a href="<?= editUrl($post->post_type, $post->post_status) ?>" class="item-title">
                <?php the_title(); ?>
                <?php if($post->post_status == 'draft'){ echo '<div class="tag single-tag yellow">На затвердженні</div>'; } ?>
                <?php 
                    if($sbInUAPost){
                        echo '<span class="sb-mark">SBinUA</span>';
                    }
                ?>
            </a>
            <span class="item-desc"><?= get_type_resourse($post); ?></span>
            <div class="panel-footer">
                <div class="tags">
                    <?php 
                        show_all_terms($post->ID);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-main-buttons">
        <?php 
            if($my_page){
        ?>
        <a class="btn-mark mark edit-pen" href="<?= editUrl($post->post_type, 'draft') ?>">
            <svg class="icon">
                <use xlink:href="#pen">
            </use></svg>
        </a>
        <?php
            }else{
        ?>
        <button class="btn btn-mark add_bookmark <?php if(in_array($post->ID, $bookmarks, FALSE)){ echo 'added'; } ?>" data-id="<?= $post->ID; ?>">
            <svg class="icon">
                <use xlink:href="#star">
            </svg>
            <span class="text">До закладок</span>
        </button>
        <?php } ?>
    </div>
</div>
