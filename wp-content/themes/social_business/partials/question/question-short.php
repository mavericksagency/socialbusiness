<?php 
    global $post, $author_id, $avatar;
    $author_id  = get_field('id_author');
    //get avatar author
    $avatar = get_field('avatar', 'user_'.$author_id);
?>
<div class="block shadow-block panel-main-lg question-panel">
    <div class="question-panel-main">
        <a href="<?php the_permalink(); ?>" class="question-title">
            <?php the_title(); ?>
        </a>
        <div class="panel-footer">
            <div class="tags">
                <?php 
                    show_all_terms($post->ID);
                ?>
            </div>
        </div>
    </div>
    <?php 
        get_template_part('partials/author/short');
    ?>
</div>