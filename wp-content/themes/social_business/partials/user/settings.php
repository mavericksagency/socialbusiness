<?php 
    global $user_id;
?>
<div class="user-menu-inner-settings">
    <ul class="user-main-settings user-menu-section">
        <li>
            <a href="<?= get_author_posts_url($user_id); ?>">
                <svg class="icon">
                    <use xlink:href="#person-circle">
                </svg>
                <span class="text">
                    Профіль
                </span>
            </a>
        </li>
        <li>
            <a href="<?= get_the_permalink(486); ?>">
                <svg class="icon">
                    <use xlink:href="#settings">
                </svg>
                <span class="text">
                    Мої закладки
                </span>
            </a>	
        </li>
        <li>
            <a href="<?= get_the_permalink(658); ?>">
                <svg class="icon">
                    <use xlink:href="#settings">
                </svg>
                <span class="text">
                    Змінити пароль
                </span>
            </a>	
        </li>
        <li>
            <a href="<?= get_the_permalink(34); ?>">
                <svg class="icon">
                    <use xlink:href="#settings">
                </svg>
                <span class="text">
                    Редагувати профіль
                </span>
            </a>	
        </li>
    </ul>
    <ul class="user-menu-list user-menu-section">				
        <li>
            <a href="<?= get_the_permalink(546); ?>?post=enterprises">
                Мої підприємства
            </a>	
        <li>
        <li>
            <a href="<?= get_the_permalink(546); ?>?post=event">
                Мої події
            </a>	
        </li>
        <li>
            <a href="<?= get_the_permalink(546); ?>?post=knowledge_base">
                Моя база знань
            </a>	
        </li>
        <li>
            <a href="<?= get_the_permalink(546); ?>?post=resources">
                Мої ресурси
            </a>	
        </li>
        <li>
            <a href="<?= get_the_permalink(546); ?>?post=opportunities">
                Мої вакансії
            </a>	
        </li>
        <li>
            <a href="<?= get_the_permalink(546); ?>?post=news">
                Мої новини
            </a>	
        </li>
        <li>
            <a href="/online_cons/?my_question">
                Мої питання
            </a>	
        </li>
    </ul>
    <a href="#" id="logout" class="user-exit user-menu-section">
        <svg class="icon">
            <use xlink:href="#exit">
        </svg>
        <span class="text">
            Вихід з профілю
        </span>
    </a>
</div>