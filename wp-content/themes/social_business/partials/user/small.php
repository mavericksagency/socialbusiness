<?php
    global $userPost, $wp_roles, $user_id;

    if($userPost){
        //get role user
        $author_roles = $userPost->roles;
        $author_role = array_shift($author_roles);
        $role = translate_user_role( $wp_roles->roles[$author_role]['name']);
        //avatar
        $avatar = get_field('avatar', 'user_'.$userPost->ID);
    }  
?>
<div class="field-person">
    <div class="person">                         
        <div class="person-logo" <?php if($avatar){ echo 'style="background-image: url('.$avatar['url'].')"'; } ?>>
            <?php if(!$avatar){ ?>
            <svg class="icon">
                <use xlink:href="#person">
            </svg>
            <?php } ?>
        </div>
        <div class="person-name">
            <span class="name"><?= get_the_author_meta('first_name', $userPost->ID). ' '. get_the_author_meta('last_name', $userPost->ID) ?></span>
            <span class="desc"><?= $role; ?></span>
        </div>
    </div>
    <button type="button" class="btn-delete" data-remove="user_<?= $userPost->ID; ?>">
        <svg>
            <use xlink:href="#close">
        </svg>
    </button>                        
</div>
