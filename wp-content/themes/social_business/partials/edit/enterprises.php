<?php 
    global $user_id, $current_user, $admins;
    if($_GET['post_id']){
        $image = array(
            'url'   => get_the_post_thumbnail_url($_GET['post_id'], 'full'),
            'id'    => get_post_thumbnail_id($_GET['post_id'])
        );
        $customPost         = array(
            'title'         => get_the_title($_GET['post_id']),
            'main'          => get_field('main', $_GET['post_id']),
            'contacts'      => get_field('contacts', $_GET['post_id']),
            'workers'       => get_field('workers', $_GET['post_id']),
            'add_info'      => get_field('add_info', $_GET['post_id']),
            'geo_position'  => get_field('geo_position', $_GET['post_id']),
            'admins'        => get_field('page_administrators', $_GET['post_id']),
            'terms'         => get_terms_ids($_GET['post_id'])

        );

        $admins     = get_field('page_administrators', (int)$_GET['post_id']);
        $admins     = explode(',', $admins);
        if($customPost['geo_position']){ 
            ?>
            <script>
                coords = '<?= $customPost['geo_position']; ?>';
            </script>
            <?php
        }
    }

    if($_GET['post_id'] && !in_array('user_'.$user_id, $admins)){
        get_template_part('partials/no-access-edit');
    }else{
    
?>
<form action="#" method="POST" class="center-column" id="ajax_enterprise">
    <div class="account-block shadow-block settings-block" id="main_info">
        <div class="account-header red">
            <span class="setting-icon bg-color">
                <svg class="icon svg-fill">
                    <use xlink:href="#buildings">
                </svg>
            </span>
            <h2 class="setting-title">
                Основна інформація
            </h2>
        </div>
        <?php if($_GET['post_id']){ ?>
            <input type="hidden" name="post_id" value="<?= $_GET['post_id']; ?>">
        <?php } ?>
        <div class="settings-fields">
            <div class="field">
                <span class="field-title">
                    Логотип підприємства
                </span>
                <input type="hidden" name="id_photo" id="id_photo">
                <label class="field-content field-upload-photo">
                    <input type="file" name="photo" id="photo_load" size="500000" accept=".png,.jpg,.jpeg,.gif" class="input-photo">
                    <span class="add-photo" id="photo">
                            <span class="photo-icon" <?php if($_GET['post_id'] && $image['url']){ echo 'style="background-image: url('. $image['url'] .')"'; } ?>>
                                <?php if(!$_GET['post_id'] && !$image['url']){  ?>
                                <svg class="icon">
                                    <use xlink:href="#photo">
                                </svg>
                                <?php } ?>
                            </span>                           
                        <span class="add-photo-text">
                            Завантажити логотип
                        </span>
                    </span>
                </label>
            </div>
            <div class="field" id="name">
                <label class="input-label">
                    <span class="input-desc">Назва соціального підприємництва</span>
                    <input type="text" name="title" class="input" <?php if($_GET['post_id']){ echo 'value="'.$customPost['title'].'"'; } ?> required placeholder="Введіть назву">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field" id="activity-div">
                <span class="field-title" id="activity-field">
                    Організаційна форма діяльності
                </span>
                <?php 
                    $field_commercial_activity = get_field_object('field_5bbb54a39e6d0');
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($field_commercial_activity['sub_fields'][0]){
                            echo '<select name="form_of_activity">
                                <option hidden="hidden">Оберіть форма діяльності</option>';
                                foreach($field_commercial_activity['sub_fields'][0]['choices'] as $key_field => $field_val){
                                    ?>
                                    <option <?php if($customPost && $customPost['main']['form_of_activity'] == $key_field){ echo 'selected="selected"'; } ?> value="<?= $key_field ?>"><?= $field_val ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <div class="filter-block required-input" id="service">
                <h3 class="filter-type">
                    Сфера комерційної діяльності
                </h3>
                <button type="button" class="btn-tag-add">
                    <span class="tag-add__name">
                        Додати сферу
                    </span>
                    <span class="tag-add__control">
                        <svg class="icon">
                            <use xlink:href="#plus">
                        </svg>
                    </span>
                </button>
                <div class="modal-filter">
                    <?php
                        $args = array(
                            'taxonomy' => 'service',
                            'hide_empty' => false
                        );
                        $terms = get_terms( $args );
                    ?>
                    <div class="modal-inner">
                        <h2 class="modal-title">
                            Оберіть сферу
                        </h2>
                        <button type="button" class="modal-close close-modal-filter">
                            <svg class="icon">
                                <use xlink:href="#close">
                            </svg>
                        </button>
                        <div class="tag-block tag-filter">
                            <?php
                                foreach($terms as $term){
                                    ?>
                                    <label class="tag">
                                        <input type="checkbox" name="service" value="<?= $term->term_id; ?>" <?php if($customPost && in_array($term->term_id, $customPost['terms'])){ echo 'checked'; } ?>>
                                        <span class="tag-style">
                                            <svg class="icon tag-type">
                                                <use xlink:href="#sphere">
                                            </svg>
                                            <svg class="icon tag-close">
                                                <use xlink:href="#close">
                                            </svg>
                                            <span class="text"><?= $term->name; ?></span>
                                        </span>
                                    </label>
                                    <?php
                                }
                            ?>						
                        </div>
                        <div class="modal-footer-buttons">
                            <button type="button" class="btn btn-remove">Очистити</button>
                            <button type="button" class="btn-raised close-modal-filter">Застосувати</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field required-input">
                <span class="field-title" id="activity-field">
                    Область
                </span>
                <?php 
                    $args = array(
                        'taxonomy' => 'geo',
                        'hide_empty' => false
                    );
                    $geo = get_terms($args);
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($geo){
                            echo '<select name="geo">
                                <option value="">Оберіть область</option>';
                                foreach($geo as $term){
                                    ?>
                                    <option <?php if($customPost && in_array($term->term_id, $customPost['terms'])){ echo 'selected="selected"'; } ?> value="<?= $term->term_id ?>"><?= $term->name ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <div class="filter-block required-input" id="type_enterprises">
                <h3 class="filter-type">
                    Тип соціального підприємства
                </h3>
                <button type="button" class="btn-tag-add">
                    <span class="tag-add__name">
                        Додати тип
                    </span>
                    <span class="tag-add__control">
                        <svg class="icon">
                            <use xlink:href="#plus">
                        </svg>
                    </span>
                </button>
                <div class="modal-filter">
                    <?php
                        $args = array(
                            'taxonomy' => 'type_enterprises',
                            'hide_empty' => false
                        );
                        $terms = get_terms( $args );
                    ?>
                    <div class="modal-inner">
                        <h2 class="modal-title">
                            Додати тип
                        </h2>
                        <button type="button" class="modal-close close-modal-filter">
                            <svg class="icon">
                                <use xlink:href="#close">
                            </svg>
                        </button>
                        <div class="tag-block tag-filter">
                            <?php
                                foreach($terms as $term){
                                    ?>
                                    <label class="tag">
                                        <input type="checkbox" name="type_enterprises" value="<?= $term->term_id; ?>" <?php if($customPost && in_array($term->term_id, $customPost['terms'])){ echo 'checked'; } ?>>
                                        <span class="tag-style">
                                            <svg class="icon tag-type">
                                                <use xlink:href="#filter">
                                            </svg>
                                            <svg class="icon tag-close">
                                                <use xlink:href="#close">
                                            </svg>
                                            <span class="text"><?= $term->name; ?></span>
                                        </span>
                                    </label>
                                    <?php
                                }
                            ?>						
                        </div>
                        <div class="modal-footer-buttons">
                            <button type="button" class="btn btn-remove">Очистити</button>
                            <button type="button" class="btn-raised close-modal-filter">Застосувати</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="filter-block required-input" id="beneficiaries">
                <h3 class="filter-type">
                    Заради кого працюєте
                </h3>
                <button type="button" class="btn-tag-add">
                    <span class="tag-add__name">
                        Додати пункт
                    </span>
                    <span class="tag-add__control">
                        <svg class="icon">
                            <use xlink:href="#plus">
                        </svg>
                    </span>
                </button>
                <div class="modal-filter">
                    <?php
                        $args = array(
                            'taxonomy' => 'beneficiaries',
                            'hide_empty' => false
                        );
                        $terms = get_terms( $args );
                    ?>
                    <div class="modal-inner">
                        <h2 class="modal-title">
                            Додати пункт
                        </h2>
                        <button type="button" class="modal-close close-modal-filter">
                            <svg class="icon">
                                <use xlink:href="#close">
                            </svg>
                        </button>
                        <div class="tag-block tag-filter">
                            <?php
                                foreach($terms as $term){
                                    ?>
                                    <label class="tag">
                                        <input type="checkbox" name="beneficiaries" value="<?= $term->term_id; ?>" <?php if($customPost && in_array($term->term_id, $customPost['terms'])){ echo 'checked'; } ?>>
                                        <span class="tag-style">
                                            <svg class="icon tag-type">
                                                <use xlink:href="#filter">
                                            </svg>
                                            <svg class="icon tag-close">
                                                <use xlink:href="#close">
                                            </svg>
                                            <span class="text"><?= $term->name; ?></span>
                                        </span>
                                    </label>
                                    <?php
                                }
                            ?>						
                        </div>
                        <div class="modal-footer-buttons">
                            <button type="button" class="btn btn-remove">Очистити</button>
                            <button type="button" class="btn-raised close-modal-filter">Застосувати</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field" id="birth-date">
                <div class="field-group">
                    <label class="input-label">
                        <span class="input-desc">Дата заснування</span>
                        <input type="text" name="date_of_foundation" <?php if($customPost && $customPost['main']['date_of_foundation']){ echo 'value="'.$customPost['main']['date_of_foundation'].'"'; } ?> class="input datepickerResourse" autocomplete="off" placeholder="Введіть дату заснування">
                        <span class="valid-notification"></span>
                    </label>
                </div>
            </div>
            <div class="field">
                <span class="field-title">
                    Про підприємство
                </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea 
                        class="textarea" 
                        name="about_enterprise" 
                        placeholder="Про підприємство" 
                        rows="10" 
                        required
                        maxlength="2000"><?php if($customPost && $customPost['add_info']['about_enterprise']){ echo $customPost['add_info']['about_enterprise']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="account-block shadow-block settings-block" id="contacts">
        <div class="account-header blue">
            <span class="setting-icon bg-color">
                <svg class="icon svg-fill">
                    <use xlink:href="#map-mark">
                </svg>
            </span>
            <h2 class="setting-title">
                Контакти
            </h2>
        </div>
        <div class="settings-fields">
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Адреса</span>
                    <input type="text" id="pac-input" required name="address" <?php if($customPost && $customPost['contacts']['address']){ echo 'value="'.$customPost['contacts']['address'].'"'; } ?> class="input" placeholder="Введіть адресу">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <input type="hidden" name="geo_position" id="geo_position" value="<?php if($customPost && $customPost['geo_position']){ echo $customPost['geo_position']; } ?>">
            <div id="map_canvas" class="map-select"></div>

            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Країна</span>
                    <input type="text" name="country" id="country" <?php if($customPost && $customPost['contacts']['country']){ echo 'value="'.$customPost['contacts']['country'].'"'; } ?> class="input" placeholder="Введіть назву країни" maxlength="30">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Місто</span>
                    <input type="text" name="city" id="city" <?php if($customPost && $customPost['contacts']['city']){ echo 'value="'.$customPost['contacts']['city'].'"'; } ?> class="input" placeholder="Введіть місто" maxlength="60">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field has-show">
                <label class="input-label">
                    <span class="input-desc">Телефон</span>
                    <input type="tel" name="phone" <?php if($customPost && $customPost['contacts']['phone']){ echo 'value="'.$customPost['contacts']['phone'].'"'; } ?> class="input" placeholder="Введіть телефон" maxlength="15">
                    <span class="valid-notification"></span>
                </label>
                <label class="show-in-profile">
                    <input type="checkbox" class="input-show" <?php if($customPost && $customPost['contacts']['hide_phone']){ echo 'checked'; } ?> name="hide_phone" value="1">
                    <span class="show-label-inner">
                        <span class="show-image">
                            <svg class="icon">
                                <use xlink:href="#lock">
                            </svg>
                        </span>
                        <span class="not-show-text text">
                            Приховано в профілі
                        </span>
                        <span class="show-text text">
                            Загальнодоступно
                        </span>
                    </span>
                </label>
            </div>
            <div class="field has-show">
                <label class="input-label">
                    <span class="input-desc">Електронна пошта</span>
                    <input type="email" name="email" <?php if($customPost && $customPost['contacts']['email']){ echo 'value="'.$customPost['contacts']['email'].'"'; } ?> class="input" placeholder="Введіть e-mail" maxlength="60">
                    <span class="valid-notification"></span>
                </label>
                <label class="show-in-profile">
                    <input type="checkbox" class="input-show" name="hide_email" <?php if($customPost && $customPost['contacts']['hide_email']){ echo 'checked'; } ?> value="1">
                    <span class="show-label-inner">
                        <span class="show-image">
                            <svg class="icon">
                                <use xlink:href="#lock">
                            </svg>
                        </span>
                        <span class="not-show-text text">
                            Приховано в профілі
                        </span>
                        <span class="show-text text">
                            Загальнодоступно
                        </span>
                    </span>
                </label>
            </div>
            <div class="field has-show">
                <label class="input-label">
                <span class="input-desc">Веб-сайт</span>
                <input type="url" name="website" <?php if($customPost && $customPost['contacts']['website']){ echo 'value="'.$customPost['contacts']['website'].'"'; } ?> class="input" placeholder="Введіть адресу сайту"  maxlength="100">
                <span class="valid-notification"></span>
                </label>
                <label class="show-in-profile">
                    <input type="checkbox" class="input-show"  name="hide_website" <?php if($customPost && $customPost['contacts']['hide_website']){ echo 'checked'; } ?> value="1">
                    <span class="show-label-inner">
                        <span class="show-image">
                            <svg class="icon">
                                <use xlink:href="#lock">
                            </svg>
                        </span>
                        <span class="not-show-text text">
                            Приховано в профілі
                        </span>
                        <span class="show-text text">
                            Загальнодоступно
                        </span>
                    </span>
                </label>
            </div>
        </div>
    </div>
    <div class="account-block shadow-block settings-block" id="workers">
        <div class="account-header violet">
            <span class="setting-icon bg-color">
                <svg class="icon svg-fill">
                    <use xlink:href="#people">
                </svg>
            </span>
            <h2 class="setting-title">
                Працівники
            </h2>
        </div>
        <div class="settings-fields">
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Керівник підприємства</span>
                    <input type="text" class="input load_persone" name="manager_enterprise" <?php if($customPost && $customPost['workers']['manager_enterprise']){ echo 'value="'.$customPost['workers']['manager_enterprise'].'"'; } ?> placeholder="Додати керівника" maxlength="60">
                    <span class="valid-notification"></span>
                </label>                                               
            </div>
            <div class="field has-button">
                <input type="hidden" name="page_administrators" id="page_administrators" value="<?php if($customPost && $customPost['admins']){ echo $customPost['admins']; }else{ echo 'user_'.$user_id.','; } ?>">
                <label class="input-label">
                    <span class="input-desc">Адміністратори сторінки</span>
                    <input type="text" 
                        class="input load_persone button-right-select" 
                        id="page_administrators_load" 
                        data-corrent="0" 
                        data-max="20" 
                        data-block="#page_administrators_people" 
                        data-input="#page_administrators" 
                        placeholder="Введіть e-mail користувача"  
                        maxlength="30"
                        data-self="<?= $current_user->data->user_email; ?>"
                        >
                    <span class="valid-notification"></span>
                </label>   
                <button type="button" class="search-user input-step-button btn" data-input="#page_administrators_load">Пошук користувача</button>  
                <span class="input-notification">
                    <span class="text">
                        Вказані користувачі матимуть повний доступ до редагування сторінки.
                    </span>
                </span>
            </div>
            <div class="width has-notification people-block" id="page_administrators_people" data-input="#page_administrators" data-input-load="#page_administrators_load">
                <?php if($customPost && $admins){ 
                    get_template_part('partials/admins');
                } ?>
            </div>
            <div class="field">
                <label class="input-label">
                <span class="input-desc">Кількість працівників</span>
                <input type="text" name="number_of_employees" <?php if($customPost && $customPost['workers']['number_of_employees']){ echo 'value="'.$customPost['workers']['number_of_employees'].'"'; } ?> class="input" placeholder="Введіть кількість працівників"  maxlength="10">
                <span class="valid-notification"></span>
                </label>
            </div>
        </div>
    </div>
    <div class="account-block shadow-block settings-block" id="other_info">
        <div class="account-header brown">
            <span class="setting-icon bg-color">
                <svg class="icon svg-fill">
                    <use xlink:href="#bag">
                </svg>
            </span>
            <h2 class="setting-title">
                Додаткова інформація
            </h2>
        </div>
        <div class="settings-fields">
            <div class="field">
                <span class="field-title">
                    Послуги
                </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="services" placeholder="Продукти або послуги соціального підприємництва" rows="10"  maxlength="2000"><?php if($customPost && $customPost['add_info']['services']){ echo $customPost['add_info']['services']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="field-title">
                    Місія підприємства
                </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="mission_enterprise" placeholder="Яка місія Вашого соціального підприємства та в якому документі вона прописана?" rows="10" maxlength="500"><?php if($customPost && $customPost['add_info']['mission_enterprise']){ echo $customPost['add_info']['mission_enterprise']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="field-title">
                Історія створення
                </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="story_creation" placeholder="Яка історія створення Вашого соціального підприємства?" rows="10" maxlength="2000"><?php if($customPost && $customPost['add_info']['story_creation']){ echo $customPost['add_info']['story_creation']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="field-title">
                Історія успіху
                </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="history_of_success" placeholder="Історія успіху Вашого соціального підприємства" rows="10" maxlength="2000"><?php if($customPost && $customPost['add_info']['history_of_success']){ echo $customPost['add_info']['history_of_success']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="field-title">
                    Реалізація місії підприємства
                </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="real_companys_bus" placeholder="Як Ваше соціальне підприємство досягає реалізації своєї місії?" rows="10" maxlength="1000"><?php if($customPost && $customPost['add_info']['real_companys_bus']){ echo $customPost['add_info']['real_companys_bus']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="field-title">
                    Прибуток та фінансові показники
                </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="profit_fin_ind" placeholder="Яким чином розподіляється прибуток Вашого соціального підприємства?" rows="10" maxlength="1000"><?php if($customPost && $customPost['add_info']['profit_fin_ind']){ echo $customPost['add_info']['profit_fin_ind']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="field-title">
                    Вплив діяльності
                </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="impact_activity" placeholder="Яким чином Ваше соціальне підприємство вимірює вплив від своєї діяльності?" rows="10" maxlength="1000"><?php if($customPost && $customPost['add_info']['impact_activity']){ echo $customPost['add_info']['impact_activity']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
        </div>
        <div class="account-footer">
            <button type="submit" class="btn-raised submit">
                <span class="save-text">
                    Зберегти
                </span>
            </button>
            <span class="saved-text">
                Збережено
            </span>
        </div>
    </div>
    <?php if($_GET['post_id']){ 
        get_template_part('partials/edit/common/delete-post');
     } ?>
</form>
<?php } ?>