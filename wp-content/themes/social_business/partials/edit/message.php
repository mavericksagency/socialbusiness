<?php 
	global $user_id;
?>
<main class="main">
	<div class="main-grid grid-mob-column">
		<div class="left-column">
			<?php get_sidebar(); ?>
		</div>
		<div class="center-column">
			<?php 
				if(!$user_id){
					get_template_part('partials/no-access');
				}else{
			?>
			<form class="account-block shadow-block settings-block" id="add_advice_question">
				<div class="account-header">
					<h2 class="setting-title">
						Консультація
					</h2>
				</div>
				<div class="settings-fields message-fields">
					<!-- if message theme exists -->						
					<div class="field">
						<label class="input-label full-width">
							<span class="input-desc">Тема запитання</span>
							<input type="text" name="title" required class="input" placeholder="Тема запитання" maxlength="500">
							<span class="valid-notification"></span>
						</label>
					</div>
					<div class="field field-two-column">
						<div class="field-inner">
							<span class="field-title">
								Сфера
							</span>
							<div class="field-group">
								<?php 
									$args = array(
										'taxonomy' => 'service',
										'hide_empty' => false
									);
									$service = get_terms($args);
								?>
								<select name="service">
									<option value="hidden" hidden="hidden">
										Оберіть сферу
									</option>
									<?php 
										foreach($service as $serviceTerm){
											echo '<option value="'.$serviceTerm->term_id.'">'.$serviceTerm->name.'</option>';
										}
									?>
								</select>
							</div>
						</div>
						<div class="field-inner">
							<span class="field-title" id="activity-field">
								Тематика
							</span>
							<div class="field-group">
								<?php 
									$args = array(
										'taxonomy' => 'topics',
										'hide_empty' => false
									);
									$topics = get_terms($args);
								?>
								<select name="topics">
									<option value="hidden" hidden="hidden">
										Оберіть тематику
									</option>
									<?php 
										foreach($topics as $topicsTerm){
											echo '<option value="'.$topicsTerm->term_id.'">'.$topicsTerm->name.'</option>';
										}
									?>
								</select>
							</div>
						</div>
					</div>
					<div class="field wp_editor">
						<label class="input-label full-width">
							<span class="input-desc"></span>
							<?php 
								wp_editor('', 'content', array(
									'wpautop'       => 1,
									'media_buttons' => 0,
									'textarea_name' => 'content',
									'textarea_rows' => 10,
									'tabindex'      => null,
									'editor_css'    => '',
									'editor_class'  => '',
									'teeny'         => false,
									'dfw'           => 0,
									'tinymce'       => 1,
									'quicktags'     => 1,
									'drag_drop_upload' => false
								) );
							?>
							<span class="valid-notification"></span>
						</label>
					</div>
					<div class="message-footer">
						<div class="add-files-block">
							<input type="hidden" name="file" id="file_attached">
							<label class="add-file" >
								<input type="file" multiple id="load_file">
								<span class="btn-add-file">
									<svg class="icon">
										<use xlink:href="#paperclip">
									</svg>
								</span>
							</label>
							<div class="files-list" id="files-list">
								
							</div>
						</div>
						<div class="message-button message-button-send">
							<button type="submit" class="btn-raised submit">
								<span class="save-text">
									Надіслати
								</span>
							</button>
							<span class="saved-text">
								Надіслано
							</span>
						</div>
					</div>
				</div>
			</form>
			<?php 
				}
			?>
		</div>
		<?php //get_sidebar('post'); ?>
	</div>
</main>