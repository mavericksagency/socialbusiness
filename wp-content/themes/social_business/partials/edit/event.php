<?php 
    if($_GET['post_id']){
        $image = array(
            'url'   => get_the_post_thumbnail_url($_GET['post_id'], 'full'),
            'id'    => get_post_thumbnail_id($_GET['post_id'])
        );
        $custom_post                    =   get_post($_GET['post_id']);
        $title                          =   get_the_title($_GET['post_id']);
        $main                           =   get_field('main_info', $_GET['post_id']);
        $place                          =   get_field('place', $_GET['post_id']);
        $date                           =   get_field('custom_date', $_GET['post_id']);
        $content                        =   $custom_post->post_content;
        
        $terms                          =   get_terms_ids($_GET['post_id']);

        if($place['coordinates']){
            ?>
            <script>
                coords = '<?= $place['coordinates']; ?>';
            </script>
            <?php
        }
    }else{
        $content = '';
    }

    if($_GET['post_id'] && !adminCheck($_GET['post_id'])){
        get_template_part('partials/no-access-edit');
    }else{
?>
<form action="#" method="POST" class="center-column" id="ajax_event">
    <div class="account-block shadow-block settings-block" id="main_info">
        <div class="account-header red">
            <span class="setting-icon bg-color">
                <svg class="icon svg-fill">
                    <use xlink:href="#buildings">
                </svg>
            </span>
            <h2 class="setting-title">
                Створюємо подію
            </h2>
        </div>
        <?php if($_GET['post_id']){ ?>
            <input type="hidden" name="post_id" value="<?= $_GET['post_id']; ?>">
        <?php } ?>
        <div class="settings-fields">
            <div class="field">
                <span class="field-title">
                    Зображення
                </span>
                <input type="hidden" name="id_photo" id="id_photo">
                <label class="field-content field-upload-photo">
                    <input type="file" name="photo" id="photo_load" value="<?= $image['url']; ?>" size="500000" accept=".png,.jpg,.jpeg,.gif" class="input-photo">
                    <span class="add-photo" id="photo">
                            <span class="photo-icon square-image" <?php if($_GET['post_id'] && $image['url']){ echo 'style="background-image: url('. $image['url'] .')"'; } ?>>
                                <?php if(!$_GET['post_id'] && !$image['url']){  ?>
                                <svg class="icon">
                                    <use xlink:href="#photo">
                                </svg>
                                <?php } ?>
                            </span>                           
                        <span class="add-photo-text">
                            Завантажити зображення
                        </span>
                    </span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Назва</span>
                    <input type="text" name="title" required <?php if($_GET['post_id'] && $title){ echo 'value="'.$title.'"'; } ?> class="input" placeholder="Введіть назву" maxlength="300">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Дата</span>
                    <input type="text" name="date" required <?php if($_GET['post_id'] && $date){ echo 'value="'.$date.'"'; } ?> class="input datepickerEvent" autocomplete="off" placeholder="Введіть дату">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Час початку</span>
                    <input type="text" name="time_start" <?php if($_GET['post_id'] && $main['time_start']){ echo 'value="'.$main['time_start'].'"'; } ?> class="input time_picker" placeholder="Введіть час початку">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Час завершення</span>
                    <input type="text" name="time_end" <?php if($_GET['post_id'] && $main['time_end']){ echo 'value="'.$main['time_end'].'"'; } ?> class="input time_picker" placeholder="Введіть час закінчення">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Місце</span>
                    <input type="text" name="place" id="pac-input" <?php if($_GET['post_id'] && $place['address']){ echo 'value="'.$place['address'].'"'; } ?> class="input" placeholder="Введіть місце проведення" maxlength="300">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <input type="hidden" name="geo_position" id="geo_position" <?php if($_GET['post_id'] && $place['coordinates']){ echo 'value="'.$place['coordinates'].'"'; } ?>>
            <div id="map_canvas" class="map-select"></div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Дедлайн реєстрації</span>
                    <input type="text" name="end_register" <?php if($_GET['post_id'] && $main['end_register']){ echo 'value="'.$main['end_register'].'"'; } ?> class="input" placeholder="Введіть дедлайн реєстрації">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Реєстрація (лінк)</span>
                    <input type="text" name="url_register" <?php if($_GET['post_id'] && $main['url_register']){ echo 'value="'.$main['url_register'].'"'; } ?> class="input" placeholder="Введіть посилання на реєстрацію" maxlength="100">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Вартість</span>
                    <input type="text" name="price" <?php if($_GET['post_id'] && $main['price']){ echo 'value="'.$main['price'].'"'; } ?> class="input" placeholder="Введіть вартість" maxlength="100">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <!-- <div class="field">
                <span class="input-desc">Короткий опис </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="description" placeholder="Введіть короткий опис" rows="10" maxlength="1000"><?php if($_GET['post_id'] && $main['description']){ echo $main['description']; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div> -->
            <div class="field wp_editor">
                <span class="input-desc">Опис</span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <?php 
                        wp_editor($content, 'content', array(
                            'wpautop'       => 1,
                            'media_buttons' => 0,
                            'textarea_name' => 'content',
                            'textarea_rows' => 10,
                            'tabindex'      => null,
                            'editor_css'    => '',
                            'editor_class'  => '',
                            'teeny'         => false,
                            'dfw'           => 0,
                            'tinymce'       => 1,
                            'quicktags'     => 1,
                            'drag_drop_upload' => false
                        ) );
                    ?>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Контакт</span>
                    <input type="text" name="contact" <?php if($_GET['post_id'] && $main['contact']){ echo 'value="'.$main['contact'].'"'; } ?> class="input" placeholder="Введіть назву"  maxlength="300">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="field-title">
                    Область
                </span>
                <?php 
                    $args = array(
                        'taxonomy' => 'geo',
                        'hide_empty' => false
                    );
                    $geo = get_terms($args);
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($geo){
                            echo '<select name="geo">
                                <option>Оберіть область</option>';
                                foreach($geo as $term){
                                    ?>
                                    <option <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'selected="selected"'; } ?> value="<?= $term->term_id ?>"><?= $term->name ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <div class="field required-input">
                <span class="field-title">
                    Тип події
                </span>
                <?php 
                    $args = array(
                        'taxonomy' => 'type_event',
                        'hide_empty' => false
                    );
                    $geo = get_terms($args);
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($geo){
                            echo '<select name="type_event">
                                <option hidden="hidden" value="">Оберіть тип</option>';
                                foreach($geo as $term){
                                    ?>
                                    <option <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'selected="selected"'; } ?> value="<?= $term->term_id ?>"><?= $term->name ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <div class="filter-block" id="service">
                <h3 class="filter-type">
                    Сфера комерційної діяльності
                </h3>
                <button type="button" class="btn-tag-add">
                    <span class="tag-add__name">
                        Додати сферу
                    </span>
                    <span class="tag-add__control">
                        <svg class="icon">
                            <use xlink:href="#plus">
                        </svg>
                    </span>
                </button>
                <div class="modal-filter">
                    <?php
                        $args = array(
                            'taxonomy' => 'service',
                            'hide_empty' => false
                        );
                        $terms = get_terms( $args );
                    ?>
                    <div class="modal-inner">
                        <h2 class="modal-title">
                            Оберіть сферу
                        </h2>
                        <button type="button" class="modal-close close-modal-filter">
                            <svg class="icon">
                                <use xlink:href="#close">
                            </svg>
                        </button>
                        <div class="tag-block tag-filter">
                            <?php
                                foreach($terms as $term){
                                    ?>
                                    <label class="tag">
                                        <input type="checkbox" name="service" value="<?= $term->term_id; ?>" <?php if($customPost && in_array($term->term_id, $customPost['terms'])){ echo 'checked'; } ?>>
                                        <span class="tag-style">
                                            <svg class="icon tag-type">
                                                <use xlink:href="#sphere">
                                            </svg>
                                            <svg class="icon tag-close">
                                                <use xlink:href="#close">
                                            </svg>
                                            <span class="text"><?= $term->name; ?></span>
                                        </span>
                                    </label>
                                    <?php
                                }
                            ?>						
                        </div>
                        <div class="modal-footer-buttons">
                            <button type="button" class="btn btn-remove">Очистити</button>
                            <button type="button" class="btn-raised close-modal-filter">Застосувати</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <span class="field-title">
                    Тематика
                </span>
                <?php 
                    $args = array(
                        'taxonomy' => 'topics',
                        'hide_empty' => false
                    );
                    $topics = get_terms($args);
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($topics){
                            echo '<select name="topics">
                                <option>Оберіть тематику</option>';
                                foreach($topics as $term){
                                    ?>
                                    <option <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'selected="selected"'; } ?> value="<?= $term->term_id ?>"><?= $term->name ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <?php 
                //public choise
                get_template_part('partials/public');
            ?>
            <div class="account-footer">
                <button type="submit" class="btn-raised submit">
                    <span class="save-text">
                        Зберегти
                    </span>
                </button>
                <span class="saved-text">
                    Збережено
                </span>
            </div>
        </div>
    </div>
    <?php if($_GET['post_id']){ 
        get_template_part('partials/edit/common/delete-post');
    } ?>
</form>
<?php } ?>