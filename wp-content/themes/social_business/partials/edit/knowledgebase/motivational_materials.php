<?php 
    global $url_material;
?>
<div class="field">
    <label class="input-label">
        <span class="input-desc">Посилання на матеріал</span>
        <input type="url" name="url_material" <?php if($_GET['post_id'] && $url_material){ echo 'value="'.$url_material.'"'; } ?> class="input" placeholder="" maxlength="100">
        <span class="valid-notification"></span>
    </label>
</div>