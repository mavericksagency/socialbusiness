<?php 
    global $url_video;
?>
<div class="field">
    <label class="input-label">
        <span class="input-desc">Посилання</span>
        <input type="url" name="url_video" required <?php if($_GET['post_id'] && $url_video){ echo 'value="'.$url_video.'"'; } ?> class="input" placeholder="Введіть посилання на відео" maxlength="100">
        <span class="valid-notification"></span>
    </label>
</div>