<?php 
    global $url_manual;
?>
<div class="field">
    <label class="input-label">
        <span class="input-desc">Посилання</span>
        <input type="url" name="url_manual" <?php if($_GET['post_id'] && $url_manual){ echo 'value="'.$url_manual.'"'; } ?> class="input" placeholder="Введіть посилання" maxlength="100">
        <span class="valid-notification"></span>
    </label>
</div>