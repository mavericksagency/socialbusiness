<?php 
    global $terms, $url_book;
?>
<div class="field">
    <label class="input-label">
        <span class="input-desc">Посилання</span>
        <input type="url" name="url_book" <?php if($_GET['post_id'] && $url_book){ echo 'value="'.$url_book.'"'; } ?> class="input" placeholder="Введіть силку на книгу">
        <span class="valid-notification"></span>
    </label>
</div>
<div class="field">
    <span class="field-title">
        Мова
    </span>
    <?php 
        $args = array(
            'taxonomy' => 'language',
            'hide_empty' => false
        );
        $languages = get_terms($args);
    ?>
    <div class="field-group half-width">
        <?php 
            if($languages){
                echo '<select name="language" class="custom-fields">
                    <option hidden="hidden">Оберіть мову</option>';
                    foreach($languages as $term){
                        ?>
                            <option <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'selected="selected"'; } ?> <?php if($type && $type == $key_field){ echo 'selected="selected"'; } ?> value="<?= $term->term_id; ?>"><?= $term->name; ?></option>
                        <?php
                    }
                echo '</select>';
            }
        ?>             
    </div>
</div>