<?php 
    global $user_id, $current_user;

    if($_GET['post_id']){
        $image = array(
            'url'   => get_the_post_thumbnail_url($_GET['post_id'], 'full'),
            'id'    => get_post_thumbnail_id($_GET['post_id'])
        );

        $custom_post                    =   get_post($_GET['post_id']);
        $content                        =   $custom_post->post_content;
        $title                          =   $custom_post->post_title;
        $description                    =   get_field('description', $_GET['post_id']);
        $authors                        =   get_field('authors', $_GET['post_id']);
        
        $terms                          =   get_terms_ids($_GET['post_id']);
        $admins                         =   get_field('page_administrators',$_GET['post_id']);
    }else{
        $content                        =   '';
    }

    if($_GET['post_id'] && !adminCheck($_GET['post_id'])){
        get_template_part('partials/no-access-edit'); 
    }else{
?>
<form action="#" method="POST" class="center-column" id="ajax_news">
    <div class="account-block shadow-block settings-block" id="main_info">
        <div class="account-header red">
            <span class="setting-icon bg-color">
                <svg class="icon svg-fill">
                    <use xlink:href="#buildings">
                </svg>
            </span>
            <h2 class="setting-title">
                Створюємо новину
            </h2>
        </div>
        <?php if($_GET['post_id']){ ?>
            <input type="hidden" name="post_id" value="<?= $_GET['post_id']; ?>">
        <?php } ?>
        <div class="settings-fields">
            <div class="field">
                <span class="field-title">
                    Зображення
                </span>
                <input type="hidden" name="id_photo" id="id_photo">
                <label class="field-content field-upload-photo">
                    <input type="file" name="photo" id="photo_load" value="<?= $image['url']; ?>" size="500000" accept=".png,.jpg,.jpeg,.gif" class="input-photo">
                    <span class="add-photo" id="photo">
                            <span class="photo-icon square-image" <?php if($_GET['post_id'] && $image['url']){ echo 'style="background-image: url('. $image['url'] .')"'; } ?>>
                                <?php if(!$_GET['post_id'] && !$image['url']){  ?>
                                <svg class="icon">
                                    <use xlink:href="#photo">
                                </svg>
                                <?php } ?>
                            </span>                           
                        <span class="add-photo-text">
                            Завантажити картинку
                        </span>
                    </span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Назва</span>
                    <input type="text" name="title" required <?php if($_GET['post_id'] && $title){ echo 'value="'.$title.'"'; } ?> class="input" placeholder="Введіть назву" maxlenght="300">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <!-- <div class="field">
                <span class="input-desc">Короткий опис </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="description" placeholder="Введіть короткий опис" rows="10" maxlength="2000"><?php if($_GET['post_id'] && $description){ echo $description; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div> -->
            <div class="field wp_editor">
                <span class="input-desc">Повний текст</span>
                <?php 
                    wp_editor($content, 'content', array(
                        'wpautop'       => 1,
                        'media_buttons' => 0,
                        'textarea_name' => 'content',
                        'textarea_rows' => 10,
                        'tabindex'      => null,
                        'editor_css'    => '',
                        'editor_class'  => '',
                        'teeny'         => false,
                        'dfw'           => 0,
                        'tinymce'       => 1,
                        'quicktags'     => 1,
                        'drag_drop_upload' => false
                    ) );
                ?>
                <!-- <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="content"  placeholder="Введіть повний текст" rows="10" maxlength="5000"><?php if($_GET['post_id'] && $custom_post){ echo $custom_post->post_content; } ?></textarea>
                    <span class="valid-notification"></span>
                </label> -->
            </div>
            <div class="field">
                <span class="field-title">
                    Область
                </span>
                <?php 
                    $args = array(
                        'taxonomy' => 'geo',
                        'hide_empty' => false
                    );
                    $geo = get_terms($args);
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($geo){
                            echo '<select name="geo">
                                <option>Оберіть область</option>';
                                foreach($geo as $term){
                                    ?>
                                    <option <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'selected="selected"'; } ?> value="<?= $term->term_id ?>"><?= $term->name ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <div class="filter-block" id="service">
                <h3 class="filter-type">
                    Сфера комерційної діяльності
                </h3>
                <button type="button" class="btn-tag-add">
                    <span class="tag-add__name">
                        Додати сферу
                    </span>
                    <span class="tag-add__control">
                        <svg class="icon">
                            <use xlink:href="#plus">
                        </svg>
                    </span>
                </button>
                <div class="modal-filter">
                    <?php
                        $args = array(
                            'taxonomy' => 'service',
                            'hide_empty' => false
                        );
                        $termsService = get_terms( $args );
                    ?>
                    <div class="modal-inner">
                        <h2 class="modal-title">
                            Оберіть сферу
                        </h2>
                        <button type="button" class="modal-close close-modal-filter">
                            <svg class="icon">
                                <use xlink:href="#close">
                            </svg>
                        </button>
                        <div class="tag-block tag-filter">
                            <?php
                                foreach($termsService as $term){
                                    ?>
                                    <label class="tag">
                                        <input type="checkbox" name="service" value="<?= $term->term_id; ?>" <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'checked'; } ?>>
                                        <span class="tag-style">
                                            <svg class="icon tag-type">
                                                <use xlink:href="#sphere">
                                            </svg>
                                            <svg class="icon tag-close">
                                                <use xlink:href="#close">
                                            </svg>
                                            <span class="text"><?= $term->name; ?></span>
                                        </span>
                                    </label>
                                    <?php
                                }
                            ?>						
                        </div>
                        <div class="modal-footer-buttons">
                            <button type="button" class="btn btn-remove">Очистити</button>
                            <button type="button" class="btn-raised close-modal-filter">Застосувати</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <span class="field-title">
                    Тематика
                </span>
                <?php 
                    $args = array(
                        'taxonomy' => 'topics',
                        'hide_empty' => false
                    );
                    $topics = get_terms($args);
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($topics){
                            echo '<select name="topics">
                                <option>Оберіть тематику</option>';
                                foreach($topics as $term){
                                    ?>
                                    <option <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'selected="selected"'; } ?> value="<?= $term->term_id ?>"><?= $term->name ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <?php 
                //public choise
                get_template_part('partials/public');
            ?>
            <?php 
            if($authors){
                $authors = json_decode($authors);
            }
            ?>
            <div class="field" id="fields_get">
                <div class="content">
                    <span class="input-desc">Автори </span>
                    <?php if($authors){ ?>
                    <?php foreach($authors as $author_line){ 
                        if(strlen($author_line[0]) != 0){
                        ?>
                        <fieldset class="fieldset-columns">
                        <label class="input-label full-width">
                            <span class="input-desc"></span> 
                            <input type="text" class="input" value="<?= $author_line[0]; ?>" placeholder="ПІБ" maxlength="100"/>
                            <input type="url" class="input" value="<?= $author_line[1]; ?>" placeholder="Поcилання на автора" maxlength="100"/>
                            <span class="valid-notification"></span>
                        </label>
                    </fieldset>
                    <?php }
                    }
                    }
                    if(!$authors){ ?>
                    <fieldset class="fieldset-columns">
                        <label class="input-label full-width">
                            <span class="input-desc"></span>
                            <input type="text" class="input" placeholder="ПІБ" maxlength="100"/>
                            <input type="url" class="input" placeholder="Поcилання на автора" maxlength="100"/>
                            <span class="valid-notification"></span>
                        </label>
                    </fieldset>
                    <?php } ?>
                </div>
                <button type="button" class="btn-raised" id="add_author">Додати ще автора</button>
            </div>
            <div class="account-footer">
                <button type="submit" class="btn-raised submit">
                    <span class="save-text">
                        Зберегти
                    </span>
                </button>
                <span class="saved-text">
                    Збережено
                </span>
            </div>
        </div>
    </div>
    <?php if($_GET['post_id']){ 
        get_template_part('partials/edit/common/delete-post');
    } ?>
</form>
<?php } ?>