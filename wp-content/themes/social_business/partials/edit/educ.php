<?php 
    global $user_id, $post, $current_user, $admins;
    if($_GET['post_id']){
        $image = array(
            'url'   => get_the_post_thumbnail_url($_GET['post_id'], 'full'),
            'id'    => get_post_thumbnail_id($_GET['post_id'])
        );
        $postEdit                       =   get_post($_GET['post_id']);

        $title                          =   get_the_title($_GET['post_id']);
        $contact_person                 =   get_field('contact_person', $_GET['post_id']);
        $type                           =   get_field('type', $_GET['post_id']);
        $phone                          =   get_field('phone', $_GET['post_id']);
        $email                          =   get_field('email', $_GET['post_id']);
        $url_soc                        =   get_field('url_soc', $_GET['post_id']);
        $education                      =   get_field('education', $_GET['post_id']);
        $keys                           =   get_field('keys', $_GET['post_id']);
        $org_before                     =   get_field('org_before', $_GET['post_id']);
        $course_information             =   get_field('url_soc', $_GET['post_id']);
        $faculty                        =   get_field('faculty', $_GET['post_id']);
        $address                        =   get_field('address', $_GET['post_id']);
        $contact_person                 =   get_field('contact_person', $_GET['post_id']);
        $support                        =   get_field('support', $_GET['post_id']);
        $about                          =   get_field('about', $_GET['post_id']);
        $terms                          =   get_terms_ids($_GET['post_id']);

        $admins_str                     =   get_field('page_administrators',$_GET['post_id']);
        $admins                         =   explode(',', $admins_str);
    }

    if($_GET['post_id'] && !in_array('user_'.$user_id, $admins)){
        get_template_part('partials/no-access-edit');
    }else{
?>
<form action="#" method="POST" class="center-column" id="ajax_resurse">
    <div class="account-block shadow-block settings-block" id="main_info">
        <div class="account-header red">
            <span class="setting-icon bg-color">
                <svg class="icon svg-fill">
                    <use xlink:href="#buildings">
                </svg>
            </span>
            <h2 class="setting-title">
                Освітні майданчики
            </h2>
        </div>
        <?php if($_GET['post_id']){ ?>
            <input type="hidden" name="post_id" value="<?= $_GET['post_id']; ?>">
        <?php } ?>
        <div class="settings-fields">
            <div class="field">
                <span class="field-title">
                    Логотип
                </span>
                <input type="hidden" name="id_photo" id="id_photo">
                <label class="field-content field-upload-photo">
                    <input type="file" name="photo" id="photo_load" value="<?= $image['url']; ?>" size="500000" accept=".png,.jpg,.jpeg,.gif" class="input-photo">
                    <span class="add-photo" id="photo">
                            <span class="photo-icon" <?php if($_GET['post_id'] && $image['url']){ echo 'style="background-image: url('. $image['url'] .')"'; } ?>>
                                <?php if(!$_GET['post_id'] && !$image['url']){  ?>
                                <svg class="icon">
                                    <use xlink:href="#photo">
                                </svg>
                                <?php } ?>
                            </span>                           
                        <span class="add-photo-text">
                            Завантажити логотип
                        </span>
                    </span>
                </label>
            </div>
            <input type="hidden" name="type" value="educ">
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Назва</span>
                    <input type="text" name="title" required <?php if($_GET['post_id'] && $title){ echo 'value="'.$title.'"'; } ?> class="input" placeholder="Введіть назву">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Адреса</span>
                    <input type="text" id="address_field" required name="address" <?php if($_GET['post_id'] && $title){ echo 'value="'.$address.'"'; } ?> class="input" placeholder="Введіть адресу">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Контактний телефон</span>
                    <input type="text" name="phone" required <?php if($_GET['post_id'] && $phone){ echo 'value="'.$phone.'"'; } ?> class="input" placeholder="Введіть телефон" maxlength="15">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Електронна адреса</span>
                    <input type="email" name="email" required <?php if($_GET['post_id'] && $email){ echo 'value="'.$email.'"'; } ?> class="input" placeholder="Введіть email" maxlength="60">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Веб-сторінка або офіційна сторінка у соціальних мережах</span>
                    <input type="url" name="url_soc" <?php if($_GET['post_id'] && $url_soc){ echo 'value="'.$url_soc.'"'; } ?> class="input" placeholder="Введіть посилання" maxlength="100">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <label class="input-label">
                    <span class="input-desc">Контактна особа</span>
                    <input type="text" name="person" required <?php if($_GET['post_id'] && $contact_person['person']){ echo 'value="'.$contact_person['person'].'"'; } ?> class="input" placeholder="Введіть ПІБ" maxlength="100">
                    <input type="text" name="position" required <?php if($_GET['post_id'] && $contact_person['position']){ echo 'value="'.$contact_person['position'].'"'; } ?> class="input" placeholder="Введіть Посаду" maxlength="100">
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="field-title">
                    Область
                </span>
                <?php 
                    $args = array(
                        'taxonomy' => 'geo',
                        'hide_empty' => false
                    );
                    $geo = get_terms($args);
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($geo){
                            echo '<select name="geo">
                                <option>Оберіть область</option>';
                                foreach($geo as $term){
                                    ?>
                                    <option <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'selected="selected"'; } ?> value="<?= $term->term_id ?>"><?= $term->name ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <div class="filter-block" id="service">
                <h3 class="filter-type">
                    Сфера комерційної діяльності
                </h3>
                <button type="button" class="btn-tag-add">
                    <span class="tag-add__name">
                        Додати сферу
                    </span>
                    <span class="tag-add__control">
                        <svg class="icon">
                            <use xlink:href="#plus">
                        </svg>
                    </span>
                </button>
                <div class="modal-filter">
                    <?php
                        $args = array(
                            'taxonomy' => 'service',
                            'hide_empty' => false
                        );
                        $termsService = get_terms( $args );
                    ?>
                    <div class="modal-inner">
                        <h2 class="modal-title">
                            Оберіть сферу
                        </h2>
                        <button type="button" class="modal-close close-modal-filter">
                            <svg class="icon">
                                <use xlink:href="#close">
                            </svg>
                        </button>
                        <div class="tag-block tag-filter">
                            <?php
                                foreach($termsService as $term){
                                    ?>
                                    <label class="tag">
                                        <input type="checkbox" name="service" value="<?= $term->term_id; ?>" <?php if($postEdit && in_array($term->term_id, $terms)){ echo 'checked'; } ?>>
                                        <span class="tag-style">
                                            <svg class="icon tag-type">
                                                <use xlink:href="#sphere">
                                            </svg>
                                            <svg class="icon tag-close">
                                                <use xlink:href="#close">
                                            </svg>
                                            <span class="text"><?= $term->name; ?></span>
                                        </span>
                                    </label>
                                    <?php
                                }
                            ?>						
                        </div>
                        <div class="modal-footer-buttons">
                            <button type="button" class="btn btn-remove">Очистити</button>
                            <button type="button" class="btn-raised close-modal-filter">Застосувати</button>
                        </div>
                    </div>
                </div>
            </div>
            <div class="field">
                <span class="field-title">
                    Тематика
                </span>
                <?php 
                    $args = array(
                        'taxonomy' => 'topics',
                        'hide_empty' => false
                    );
                    $topics = get_terms($args);
                ?>
                <div class="field-group half-width">
                    <?php 
                        if($topics){
                            echo '<select name="topics">
                                <option>Оберіть тематику</option>';
                                foreach($topics as $term){
                                    ?>
                                    <option <?php if($_GET['post_id'] && in_array($term->term_id, $terms)){ echo 'selected="selected"'; } ?> value="<?= $term->term_id ?>"><?= $term->name ?></option>
                                    <?php
                                }
                            echo '</select>';
                        }
                    ?>             
                </div>
            </div>
            <div class="field has-notification has-button">
                <input type="hidden" name="page_administrators" id="page_administrators" value="<?php if($_GET['post_id'] && $admins){ echo $admins_str; }else{ echo 'user_'.$user_id.','; } ?>">
                <label class="input-label">
                    <span class="input-desc">Адміністратори сторінки</span>
                    <input type="text" 
                        class="input load_persone button-right-select" 
                        id="page_administrators_load" 
                        data-corrent="0" 
                        data-max="20" 
                        data-block="#page_administrators_people" 
                        data-input="#page_administrators" 
                        placeholder="Введіть e-mail користувача"  
                        maxlength="30"
                        data-self="<?= $current_user->data->user_email; ?>"
                        ><span class="valid-notification"></span>
                </label>
                <button type="button" class="search-user btn input-step-button" data-input="#page_administrators_load">Пошук користувача</button>   
                <span class="input-notification">
                    <span class="text">
                        Вказані користувачі матимуть повний доступ до редагування сторінки.
                    </span>
                </span> 
            </div>
            <div class="width has-notification people-block" id="page_administrators_people" data-input="#page_administrators" data-input-load="#page_administrators_load">
                <?php if($_GET['post_id'] && $admins){ 
                    get_template_part('partials/admins');
                } ?>
            </div>
            <div class="field">
                <span class="input-desc">Про освітній майданчик </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" required name="about" placeholder="Введіть інформацію" rows="10" maxlength="1000"><?php if($_GET['post_id'] && $about){ echo $about; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="field">
                <span class="input-desc">Додаткова інформація </span>
                <label class="input-label full-width">
                    <span class="input-desc"></span>
                    <textarea class="textarea" name="content" placeholder="Введіть додаткову інформацію" rows="10" maxlength="1000"><?php if($_GET['post_id'] && $postEdit){ echo $postEdit->post_content; } ?></textarea>
                    <span class="valid-notification"></span>
                </label>
            </div>
            <div class="account-footer">
                <button type="submit" class="btn-raised submit">
                    <span class="save-text">
                        Зберегти
                    </span>
                </button>
                <span class="saved-text">
                    Збережено
                </span>
            </div>
        </div>
    </div>
    <?php if($_GET['post_id']){ 
        get_template_part('partials/edit/common/delete-post');
    } ?>
</form>
<?php } ?>