<?php 
    $post_del = get_post($_GET['post_id']);
?>
<div class="account-block shadow-block settings-block">
    <button type="button" class="btn remove" id="remove-post" data-url="<?= get_the_permalink(546).'?post='.$post_del->post_type; ?>" data-id="<?= $_GET['post_id']; ?>">
        <svg class="icon">
            <use xlink:href="#close">
        </svg>
        <span class="text">
            Видалити
        </span>
    </button>
</div>