<?php
    global $args;
    $query['all']              = new WP_Query($args);
    //event
    $args['post_type'] = 'event';
    $query['event']       = new WP_Query($args);
    //knowledge_base
    $args['post_type'] = 'knowledge_base';
    $query['knowledge_base']       = new WP_Query($args);
    //opportunities
    $args['post_type'] = 'opportunities';
    $query['opportunities']       = new WP_Query($args);
    //news
    $args['post_type'] = 'news';
    $query['news']       = new WP_Query($args);
?>
    <button class="wall-all">
        <span class="text">
            <?= getTypeNamePosts($_GET['post_type_author']);  ?> 
        </span>
        <span class="number">
            <?php
                if(isset($_GET['post_type_author'])){
                    echo $query[$_GET['post_type_author']]->found_posts;
                }else{
                    echo $query['all']->found_posts;
                }
            ?>
        </span>
    </button>
    <ul class="wall-categories-list">
        <?php 
            if(isset($_GET['post_type_author'])){
        ?>
        <li class="wall-category">
            <a href="?" class="wall-category-block">
                <span class="category-icon">
                    <svg class="icon">
                        <use xlink:href="#list-block">
                    </svg>
                </span>
                <span class="category-name">Всі події</span>
                <span class="category-number">
                    <?= $query['all']->found_posts; ?>
                </span>
            </a>
        </li>
        <?php 
            }
        ?>
        <li class="wall-category">
            <a href="?post_type_author=event" class="wall-category-block">
                <span class="category-icon">
                    <svg class="icon">
                        <use xlink:href="#calendar-events">
                    </svg>
                </span>
                <span class="category-name">Події</span>
                <span class="category-number">
                    <?= $query['event']->found_posts; ?>
                </span>
            </a>
        </li>
        <li class="wall-category">
            <a href="?post_type_author=knowledge_base" class="wall-category-block">
                <span class="category-icon">
                    <svg class="icon">
                        <use xlink:href="#education">
                    </svg>
                </span>
                <span class="category-name">База знань</span>
                <span class="category-number">
                    <?= $query['knowledge_base']->found_posts; ?>
                </span>
            </a>
        </li>
        <li class="wall-category">
            <a href="?post_type_author=opportunities" class="wall-category-block">
                <span class="category-icon">
                    <svg class="icon">
                        <use xlink:href="#bag">
                    </svg>
                </span>
                <span class="category-name">Вакансії</span>
                <span class="category-number">
                    <?= $query['opportunities']->found_posts; ?>
                </span>
            </a>
        </li>
        <li class="wall-category">
            <a href="?post_type_author=news" class="wall-category-block">
                <span class="category-icon">
                    <svg class="icon">
                        <use xlink:href="#list-block">
                    </svg>
                </span>
                <span class="category-name">Новини</span>
                <span class="category-number">
                    <?= $query['news']->found_posts; ?>
                </span>
            </a>
        </li>
    </ul>
</div>
<div class="wall-inner">
    <?php 
        if(isset($_GET['post_type_author'])){
            if($_GET['post_type_author'] == 'news'){
                $queryResult = $query['news'];
            }else if($_GET['post_type_author'] == 'event'){
                $queryResult = $query['event'];
            }else if($_GET['post_type_author'] == 'knowledge_base'){
                $queryResult = $query['knowledge_base'];
            }else if($_GET['post_type_author'] == 'opportunities'){
                $queryResult = $query['opportunities'];
            }
        }else{
            $queryResult = $query['all'];
        }

        if($queryResult->have_posts()){
            while($queryResult->have_posts()){
                $queryResult->the_post();
                get_template_part('partials/post/post-short');
            }
            wp_reset_postdata();
        }else{
            echo '<h1>Записів не знайдено</h1>';
        }
    ?>
</div>