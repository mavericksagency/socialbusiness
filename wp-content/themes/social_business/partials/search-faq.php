<form action="<?= get_the_permalink(544); ?>" method="POST" class="search-form">
    <button class="btn-transparent">
        <svg class="icon">
            <use xlink:href="#search">
        </svg>
    </button>
    <label class="input-with-icon support-search">
        <input name="search" type="text" required placeholder="Введіть ключове слово">
    </label>
</form>
