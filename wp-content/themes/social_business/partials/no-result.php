<?php 
    global $url;
?>
<div class="clear-block">
    <img class="clear-image" src="<?php bloginfo('template_directory'); ?>/img/no-result.svg">
    <h2 class="clear-title">Немає записів</h2>
    <?php if($url){ ?>
    <p>
        <a href="<?= $url; ?>" class="btn btn-create-clear">
            <svg class="icon">
                <use xlink:href="#plus">
            </svg>
            <span>Створити <?= correntTypeName($_GET['post']); ?></span>
        </a>
    </p>
    <?php } ?>
</div>