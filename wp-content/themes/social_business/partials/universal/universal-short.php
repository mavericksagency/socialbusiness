<?php 
    global $post, $author_id, $bookmarks;
?>
<div class="panel-main-lg">
    <div class="item-main">
        <div class="item-image">
            <img src="<?php the_post_thumbnail_url(); ?>" alt="Company" class="img">
        </div>
        <div class="item-info">
            <a href="<?php the_permalink(); ?>" class="item-title"><?php the_title(); ?></a>
            <span class="item-desc">test test test</span>
            <div class="panel-footer">
                <div class="tags">
                    <?php 
                        show_all_terms($post->ID);
                    ?>
                </div>
            </div>
        </div>
    </div>
    <div class="panel-main-buttons">
        <button class="btn btn-mark">
            <svg class="icon">
                <use xlink:href="#star">
            </svg>
            <span class="text">До закладок</span>
        </button>
    </div>
</div>
